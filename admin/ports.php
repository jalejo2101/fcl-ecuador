<?php include_once("Consultas.php");?>
<?php include_once("funciones.php");?>
<?php
if($_POST["modo"]=="new"){
    agrega_puerto();
}else if($_POST["modo"]=="update"){
    modifica_puerto();
}if($_POST["modo"]=="delete"){
    elimina_puerto();
}

$con=new Consultas();
$paises=$con->get_country_list();



if(isset($_GET["pais"]) || isset($_POST["pais"])){
    $lst=$con->get_port_list($_GET["pais"]);
    $p=$_GET["pais"];
    if(isset($_POST["pais"])){
        $lst=$con->get_port_list($_POST["pais"]);
        $p=$_POST["pais"];
    }


}else{
    $lst=$con->get_port_list();
    $p="ECUADOR";
}?>

<!DOCTYPE html>
<html>
<head>
    <?php include("head.php");?>
    <title></title>
    <script>
        function borrar(id)
        {
            document.fr.id.value=id;
            if(confirm("Esta seguro que desea eliminar este Puerto?")){
                document.fr.submit();
            }
        }
    </script>
</head>
<body>
<div class="row">
    <div class="col-xs-6 col-xs-offset-3">
        <h3>Listado de Puertos</h3>
    </div>
    <div class="col-xs-2" style="padding-top:15px">
        <button type="button" class="btn btn-primary" style="width: 100%" onclick="window.open('port.php','_self','')">Agregar</button>
    </div>
</div>





<div class="row">
    <div class="col-xs-2 col-xs-offset-1">
        <?php $op=12 ?>
        <?php include_once("menu.php")?>
    </div>
    <div class="col-xs-8">


        <div class="panel panel-success" style="width: 50%">
            <div class="panel-heading">
                <h3 class="panel-title">Pais</h3>
            </div>
            <div class="panel-body">
                <?php //var_dump($lst);?>
                <form name="ps" action="" method="get">
                    <select class="form-control" name="pais" id="paises">
                        <?php
                        foreach($paises as $pais){ ?>
                        <option value="<?php echo $pais['pais']?>"><?php echo $pais['pais']?></option>
                        <?php } ?>
                    </select>
                </form>
            </div>
        </div>

        <script type="text/javascript">
            var p='<?php echo $p ?>';
            $("#paises").change(function(){
                document.ps.submit();
            });

            $(document).ready(function(){

                $("#paises option[value='"+ p + "']").attr('selected', 'selected');
            });
        </script>

        <table class="table table-hover">
            <thead>
            <tr style="background: #9acfea">
                <td style="width: 5%">Codigo</td>
                <td style="text-align: center">Nombre</td>
                <td style="width: 20%; text-align: center">Ciudad</td>
                <td style="width: 15%; text-align: center">Tipo</td>
                <td style="width: 7%; text-align: center">Estatus</td>
                <td style="width: 5%; text-align: center">M</td>
                <td style="width: 5%; text-align: center">E</td>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach($lst as $item){ ?>
            <tr>
                <td><?php echo $item['codigo']?></td>
                <td><?php echo $item['nombre']?></td>
                <td style="; text-align: center"><?php echo $item['ciudad'] ?></td>
                <td style="; text-align: center"><?php echo $item['tipo'] ?></td>
                <td style="; text-align: center"><input type="checkbox" disabled <?php echo ($item["estatus"]== 1) ? "checked ":"";?>></td>
                <td style="; text-align: center"><a href="port.php?id=<?php echo $item["id"]?>"><img src="img/edit_icon.png"></a></td>
                <td style="; text-align: center"><a href="javascript:borrar('<?php echo $item["id"]?>')"><img src="img/delete_icon.png"></a></td>
            </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>


<form name="fr" method="post" action="">
    <input type="hidden" name="id">
    <input type="hidden" name="modo" value="delete">
</form>

</body>
</html>





