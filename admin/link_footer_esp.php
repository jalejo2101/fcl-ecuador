<?php
/**
 * Created by PhpStorm.
 * User: JuanJosé
 * Date: 07/08/14
 * Time: 09:17 AM
 */
include_once("Consultas.php");
$con=new Consultas();

$id=0;
$$link=null;
$tipo="Link";
$grupo="";
if($_GET["id"]!=null){
    $id=$_GET["id"];
    $link=$con->get_link($id,"_esp");
    $grupo=(isset($link["grupo"]))?$link["grupo"]:"";
    $url=$link["url"];
    $tipo=$link["tipo"];
    $desc=$link["descripcion"];
    $ventana=($link["ventana"]!=null)?1:0;
    $activo=($link["activo"]!=null)?1:0;
}


?>
<!DOCTYPE html>
<html>
<head>
    <?php include("head.php");?>
    <title></title>
</head>
<body>
<div class="row">
    <div class="col-md-7 col-md-offset-3">
        <?php if($id==0){ ?>
            <h3>Insercion de Links</h3>
        <?php }else{?>
            <h3>Modificacion de Links</h3>
        <?php } ?>
    </div>
</div>

<div class="row">
    <div class="col-xs-2 col-xs-offset-1">
        <?php $op=4 ?>
        <?php include_once("menu.php")?>
    </div>
    <div class="col-xs-8">

        <form role="form" action="links_footer_esp.php" method="post" enctype="multipart/form-data">
            <?php if($id>0){ ?>
            <div class="form-group">
                <label for="id">Id</label>
                <input type="text" class="form-control" id="id" name="id" value="<?php echo $id ?>" readonly>
            </div>
            <?php } ?>

            <div class="form-group">
                <label for="grupo">Grupo</label>
                <select class="form-control" name="grupo" id="grupo">
                    <option <?php echo ($grupo=="POPULAR LIKNS")? "selected":"" ?>value="POPULAR LIKNS">POPULAR LIKNS</option>
                    <option <?php echo ($grupo=="TOOLS")? "selected":"" ?> value="TOOLS">TOOLS</option>
                    <option <?php echo ($grupo=="ABOUT US")? "selected":"" ?> value="ABOUT US">ABOUT US</option>
                    <option <?php echo ($grupo=="SUPPORT")? "selected":"" ?> value="SUPPORT">SUPPORT</option>
                </select>
            </div>

            <div class="radio">
                <label>
                    <input type="radio" name="tipo" id="archivo" value="archivo" <?php echo ($tipo=="Archivo")? "checked":"" ?> onclick="abrirArchivo()">
                    Utilizar un archivo para el Hipervinculo
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="tipo" id="link" value="link" <?php echo ($tipo=="Link")? "checked":"" ?> onclick="abrirUrl()">
                    Utilizar una URL para el Hipervinculo
                </label>
            </div>
            <hr>


            <div class="form-group" id="url" style="display: none">
                <label for="url">URL</label>
                <input type="text" class="form-control" id="url" name="url" placeholder="http://..." value="<?php echo ($id>0)? $url:"" ?>" >
            </div>
            <div class="form-group" id="documento">
                <label for="file">Adjuntar Archivo</label>
                <input type="file" id="file" name="file">
                <?php echo ($id>0)? "<help>".$link["archivo"]."</help>":""?>
            </div>
            <div class="form-group">
                <label for="desc">Descripcion</label>
                <input type="text" class="form-control" id="desc" name="desc" placeholder="Descripcion" value="<?php echo ($id>0)? $desc:"" ?>" >
            </div>
            <hr>
            <div class="checkbox">
                <label>
                    <input name="ventana" type="checkbox" <?php echo ($id>0 && $link["ventana"]==1)? "checked":""?> > Abrir en Nueva Ventana
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input name="activo" type="checkbox" <?php echo ($id>0 && $link["activo"]==1)? "checked":""?> > Activo
                </label>
            </div>
            <button type="submit" class="btn btn-default">Enviar</button>
            <input type="hidden" name="modo" value="<?php echo ($id==0)? "new":"update"?>">
        </form>
    </div>
</div>
</body>
<script>
    <?php echo ($tipo=="Archivo")? "abrirArchivo()":"abrirUrl()" ?>

    function abrirArchivo(){
        document.getElementById("url").style.display='none';
        document.getElementById("documento").style.display='block';
    }
    function abrirUrl(){
        document.getElementById("url").style.display='block';
        document.getElementById("documento").style.display='none';
    }


</script>

</html>