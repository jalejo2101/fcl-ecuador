<?php
/**
 * Created by PhpStorm.
 * User: JuanJosé
 * Date: 07/08/14
 * Time: 09:17 AM
 */
include_once("Consultas.php");
$con=new Consultas();

$id=0;
$tipo="admin";
if($_GET["id"]!=null){
    $id=$_GET["id"];
    $cuenta=$con->get_admin($id);
    $user=$cuenta[0]["cuenta"];
    $nombre=$cuenta[0]["nombre"];
    $mail=$cuenta[0]["mail"];
    $tipo=$cuenta[0]["tipo_usuario"];
}


?>
<!DOCTYPE html>
<html>
<head>
    <?php include("head.php");?>
    <title></title>
    <script>
    <?php if($id>0){ ?>
        var isnew=false;
    <?php }else{ ?>
        var isnew=true;
    <?php } ?>

    function chg_pass(){
        document.getElementById('psw').style.display='block';
        document.getElementById('chg').style.display='none';
        document.getElementById('can').style.display='block';
        document.fr.pass1.value="";
        document.fr.pass2.value="";
    }
    function cancel(){
        document.getElementById('psw').style.display='none';
        document.getElementById('chg').style.display='block';
        document.getElementById('can').style.display='none';
        document.fr.pass1.value="passwordinvalido##";
        document.fr.pass2.value="passwordinvalido##";
    }


    function validar(){
        err=false;
        if(document.fr.cuenta.value==""){
            err=true;
            msg="Please enter the User Name";
        }else if(document.fr.name.value==""){
            err=true;
            msg="Please enter the Name";
        }else if(document.fr.pass1.value!=document.fr.pass2.value && isnew){
            err=true;
            msg="Repeat the password";
        }else if(document.fr.pass1.value.length<8 && isnew){
            err=true;
            msg="Password is case sensitive, has a minimum length of 8 characters.";
        }
        if(err==true){
            alert(msg);
        }else{
            document.fr.submit()
        }
    }
    </script>


</head>
<body>
<div class="row">
    <div class="col-md-7 col-md-offset-3">
        <?php if($id==0){ ?>
            <h3>Insercion de Administrador</h3>
        <?php }else{?>
            <h3>Modificacion de Administrador</h3>
        <?php } ?>
    </div>
</div>

<div class="row">
    <div class="col-xs-2 col-xs-offset-1">
        <?php $op=19 ?>
        <?php include_once("menu.php")?>
    </div>
    <div class="col-xs-8">
        <form role="form" action="admins.php" method="post" enctype="multipart/form-data" name="fr">
            <?php if($id>0){ ?>
            <div class="form-group">
                <label for="id">Id</label>
                <input type="text" class="form-control" id="id" name="id" value="<?php echo $id ?>" readonly>
            </div>
            <?php } ?>
            <div class="form-group">
                <label for="cuenta">User</label>
                <input type="text" class="form-control" id="cuenta" name="cuenta" placeholder="Cuenta" value="<?php echo ($id>0)? $user:"" ?>" required="" >
            </div>
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?php echo ($id>0)? $nombre:"" ?>" required="" >
            </div>
            <div class="form-group">
                <label for="mail">Email</label>
                <input type="email" class="form-control" id="mail" name="mail" placeholder="Email" value="<?php echo ($id>0)? $mail:"" ?>" required="" >
            </div>
            <button id="chg" type="button" class="btn btn-default" onclick="chg_pass()">
                Cambiar Password
            </button>
            <button id="can" type="button" class="btn btn-default" onclick="cancel()" style="display: none">
                Cancelar
            </button>
            <div class="row" id="psw" style="display: none; margin-top: 15px">
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="pass1">Password</label>
                        <input type="password" class="form-control" id="pass1" name="pass1" placeholder="Password" value="<?php echo ($id>0)? "passwordinvalido##":"" ?>">
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="pass2">Repeat Password</label>
                        <input type="password" class="form-control" id="pass2" name="pass2" placeholder="Repeat Password" value="<?php echo ($id>0)? "passwordinvalido##":"" ?>" >
                    </div>
                </div>
            </div>
            <hr>
            <label for="tipo">User Type</label>
            <div class="radio">
                <label>
                    <input type="radio" name="tipo" id="tipo" value="admin" <?php echo ($tipo=="admin")? "checked":"" ?>>
                    Admin
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="tipo" id="tipo" value="superadmin" <?php echo ($tipo=="superadmin")? "checked":"" ?>>
                    SuperAdmin
                </label>
            </div>
            <hr>
            <button type="button" onclick="validar()" class="btn btn-default">Enviar</button>
            <input type="hidden" name="modo" value="<?php echo ($id==0)? "new":"update"?>">
            <?php if($id<=0){?>
                <script>
                    chg_pass();
                    document.getElementById('chg').style.display='none';
                    document.getElementById('can').style.display='none';
                </script>
            <?php } ?>
        </form>
    </div>
</div>

</body>
</html>