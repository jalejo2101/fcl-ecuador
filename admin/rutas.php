<?php include_once("Consultas.php");?>
<?php include_once("funciones.php");?>
<?php
if($_POST["modo"]=="new"){
    agrega_ruta();
}else if($_POST["modo"]=="update"){
    //modifica_ruta();
}if($_POST["modo"]=="delete"){
    elimina_ruta();
}

$con=new Consultas();
$paises=$con->get_country_list();



if(isset($_GET["pais"])){
    $lst=$con->get_rutas($_GET["pais"]);
    $p=$_GET["pais"];
}else{
    $lst=$con->get_rutas();
    $p="ECUADOR";
}?>

<!DOCTYPE html>
<html>
<head>
    <?php include("head.php");?>
    <title></title>
    <script>
        function borrar(id)
        {
            document.fr.id.value=id;
            if(confirm("Esta seguro que desea eliminar esta ruta ?")){
                document.fr.submit();
            }
        }
    </script>
</head>
<body>
<div class="row">
    <div class="col-xs-6 col-xs-offset-3">
        <h3>Listado de Rutas</h3>
    </div>
    <div class="col-xs-2" style="padding-top:15px">
        <button type="button" class="btn btn-primary" style="width: 100%" onclick="window.open('ruta.php','_self','')">Agregar</button>
    </div>
</div>





<div class="row">
    <div class="col-xs-2 col-xs-offset-1">
        <?php $op=15 ?>
        <?php include_once("menu.php")?>
    </div>
    <div class="col-xs-8">

        <?php /* ?>
        <div class="panel panel-success" style="width: 50%">
            <div class="panel-heading">
                <h3 class="panel-title">Pais</h3>
            </div>

            <div class="panel-body">
                <form name="ps" action="" method="get">
                    <select class="form-control" name="pais" id="paises">
                        <?php
                        foreach($paises as $pais){ ?>
                        <option value="<?php echo $pais['pais']?>"><?php echo $pais['pais']?></option>
                        <?php } ?>
                    </select>
                </form>
            </div>

        </div>
        <?php */ ?>
        <script type="text/javascript">
            var p='<?php echo $p ?>';
            $("#paises").change(function(){
                document.ps.submit();
            });

            $(document).ready(function(){

                $("#paises option[value='"+ p + "']").attr('selected', 'selected');
            });
        </script>

        <table class="table table-hover">
            <thead>
            <tr style="background: #9acfea">
                <td style="width: 35%">ORIGEN: <b>Pais</b> / Ciudad / [Puerto]</td>
                <td style="width: 35%">DESTINO: <b>Pais</b> / Ciudad / [Puerto]</td>
                <td style="width: 8%; text-align: center">Tipo</td>
                <td style="width: 15%; text-align: center" colspan="2">Tarifas</td>
                <td style="width: 5%; text-align: center">E</td>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach($lst as $item){ ?>
            <tr>
                <td>
                    <div class="ps"><?php echo $item['pais_o']?></div>
                    <div class="ct">
                        <b><?php echo $item['ciudad_o']?></b><br>
                        <b>[ </b><?php echo $item['puerto_o']?><b> ]</b>
                    </div>
                </td>
                <td>
                    <div class="ps"><?php echo $item['pais_d']?></div>
                    <div class="ct">
                        <b><?php echo $item['ciudad_d']?></b><br>
                        <b>[ </b><?php echo $item['puerto_d']?><b> ]</b>
                    </div>
                </td>


                <td style="text-align: center"><?php echo $item['tipo_ruta'] ?></td>
                <!--<td style="text-align: center"><input type="checkbox" disabled <?php /*echo ($item["estatus"]== 1) ? "checked ":"";*/?>></td>-->
                <!--td style="text-align: center"><a href="salidas_ruta.php?id=<?php echo $item["id"]?>"><img src="img/pricelist_contenedor_icon.jpg" alt="SALIDAS POR RUTA"></a></td-->
                <td style="text-align: center"><a href="tarifas_generales.php?id=<?php echo $item["id"]?>"><img src="img/pricelist_contenedor_icon.jpg" alt="TARIFAS POR CONTENEDOR"></a></td>
                <!--td style="text-align: center"><a href="tarifas_ruta.php?id=<?php echo $item["id"]?>"><img src="img/pricelist_proveedor_icon.jpg" alt="TARIFAS POR RUTA"></a></td-->
                <td style="text-align: center"><a href="tarifas_lcl.php?id=<?php echo $item["id"]?>"><img src="img/pricelist_lcl_icon.jpg" alt="TARIFAS LCL"></a></td>
                <td style="text-align: center"><a href="javascript:borrar('<?php echo $item["id"]?>')"><img src="img/delete_icon.png"></a></td>
            </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>


<form name="fr" method="post" action="">
    <input type="hidden" name="id">
    <input type="hidden" name="modo" value="delete">
</form>



</body>
</html>





