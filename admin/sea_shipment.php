<?php
session_start();
/**
 * Created by PhpStorm.
 * User: JuanJosé
 * Date: 07/08/14
 * Time: 09:17 AM
 */
include_once("Consultas.php");
include_once("funciones.php");

$con=new Consultas();
$id=$_GET["id_client"];
$new=false;

/*if(isset($_GET["modo"])){
    if($_GET["modo"]=="new") $new=true;
}*/

//echo ">>>>>".$_POST["modo"];

if(isset($_POST["modo"])){
    if($_POST["modo"]=="new"){
        agrega_sea_shipment();
        $new=false;
    }else if($_POST["modo"]=="update"){
        update_sea_shipment();
        $new=true;
    }
}

if($_GET["bl_number"]!=null || $_POST["bl_number"]!=null ){
    if(isset($_GET["bl_number"])) $bl=$_GET["bl_number"];
    if(isset($_POST["bl_number"])) $bl=$_POST["bl_number"];
    $sea=$con->get_sea_tracking($bl);
    $sea_detail=$con->get_sea_tracking_detail($bl);

    $type_c=$sea["type"];
    $origen=$sea["origen"];
    $to=$sea["shipped_to"];
    $destino=$sea["destino"];
    $final_pod=$sea["final_pod"];
    $eta=$sea["eta"];
    //$pick_up_date=$sea["pick_up_date"];
    $new=false;
}
if($_GET["type"]=="new"){
    $new=true;
}


?>
<!DOCTYPE html>
<html>
<head>
    <?php include("head.php");?>
    <title></title>
    <script>
        function show_edit(){
            if($("#edit_data").css("display")=="block"){
                $("#edit_data").css("display","none");
                $("#show_data").css("display","block");
                $("#sh_e").text("Edit");
            }else{
                $("#edit_data").css("display","block");
                $("#show_data").css("display","none");
                $("#sh_e").text("Cancel Edit");
            }
        }
    </script>
    <script>
        $(function() {
            $( "#pick_up_date" ).datepicker();
        });
    </script>

</head>
<body>
<div class="row">
    <div class="col-xs-6 col-xs-offset-3">
            <h3>sea Shipment</h3>
    </div>
    <div class="col-xs-2" style="margin-top: 15px">
        <?php if($_GET["type"]!="new"){?>
        <button id="sh_e" type="button" class="btn btn-info" style="width: 100%" onclick="show_edit()">Edit</button>
        <?php } ?>
    </div>
</div>

<div class="row">
    <div class="col-xs-2 col-xs-offset-1">
        <?php $op=11 ?>
        <?php include_once("menu.php")?>
    </div>
    <div class="col-xs-8" >
        <form role="form" action="sea_shipment.php" method="post" enctype="multipart/form-data" id="edit_data" style="display: <?php echo ($new)?"block":"none"?>">
            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="bl_number">BL Number</label>
                        <input type="text" class="form-control" id="bl_number" name="bl_number" value="<?php echo ($bl!="")?$bl:"" ?>" >
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="from">Type</label>
                        <input type="text" class="form-control" id="type_c" name="type_c" value="<?php echo ($bl!="")?$type_c:"" ?>" >
                    </div>
                </div>
            </div>
            <!----------------------------------------------------------->
            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="origen">Origin</label>
                        <input type="text" class="form-control" id="origen" name="origen" value="<?php echo ($bl!="")?$origen:"" ?>" >
                    </div>
                </div>
                <div class="col-xs-6" style="display: none;">
                    <div class="form-group">
                        <label for="from">Shippet To</label>
                        <input type="text" class="form-control" id="shipped_to" name="shipped_to" value="<?php echo ($bl!="")?$to:"" ?>" >
                    </div>
                </div>
            </div>
            <!----------------------------------------------------------->
            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="destino">Destination</label>
                        <input type="text" class="form-control" id="destino" name="destino" value="<?php echo ($bl!="")?$destino:"" ?>" >
                    </div>
                </div>
                <div class="col-xs-6" style="display: none;">
                    <div class="form-group">
                        <label for="numero_pck">Final POD</label>
                        <input type="text" class="form-control" id="final_pod" name="final_pod" value="<?php echo ($bl!="")?$final_pod:"" ?>" >
                    </div>
                </div>
            </div>
            <!----------------------------------------------------------->
            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="pick_up_date">ETA</label>
                        <input type="date" class="form-control" id="" name="eta" value="<?php echo ($bl!="")?$eta:"" ?>" >
                    </div>
                </div>
            </div>


            <div class="form-group">
                <label for="file">Attach File</label>
                <input type="file" id="file" name="file">
                <?php echo ($id>0)? "<help>".$news["imagen"]."</help>":""?>
            </div>
            <button type="submit" class="btn btn-success"  style="width: 100%">Enviar</button>
            <input type="hidden" name="id" value="<?php echo $id ?>">
            <input type="hidden" name="modo" value="<?php echo ($bl=="")? "new":"update"?>">


        </form>

        <div class="panel panel-default" id="show_data" style="margin-top: 30px; display:<?php echo ($new)?"none":"block"; ?>">
            <!--div class="panel-heading">Título del panel</div-->
            <table border="1" class="table" >
                <tr>
                    <th style="width: 20%">Bl NUMBER</th>
                    <td><?php echo $bl?></td>
                </tr>
                <tr>
                    <th>Container Type</th>
                    <td><?php echo $type_c ?></td>
                </tr>
                <tr>
                    <th>Origin</th>
                    <td><?php echo $origen ?></td>
                </tr>
                
                <tr>
                    <th>Destination</th>
                    <td><?php echo $destino ?></td>
                </tr>
                
                <tr>
                    <th>ETA</th>
                    <td><?php echo $eta ?></td>
                </tr>
                <!--tr>
                    <th>Pick Up Date</th>
                    <td><?php echo $pick_up_date ?></td>
                </tr-->
            </table>
        </div>

        <div id="detalles" style="width: 100%"></div>

        <div style="width: 100%; text-align: center">
            <hr>
            <?php
            $mail=$con->get_user_mail_by_sea($bl);
            $param="bl_number=".$bl."&mail=".$mail[0]["mail"]; ?>
            <img src="../img/send_mail.jpg" style="width: 100px; cursor: pointer" onclick="javascript:enviarCorreo('<?php echo $param ?>')">
        </div>
        <div id="enviado" style="width: 100; text-align: center"></div>

    </div>
</div>
<script>
    function detalles(param){
        $.ajax({
            url: "sea_detail.php?"+param,
            cache: false
        }).done(function( html ) {
                $( "#detalles").empty().append( html );
        });
    }
    function enviarCorreo(param){
        $.ajax({
            url: "sea_shipment_mail.php?"+param,
            cache: false
        }).done(function( ) {
            $( "#enviado").empty().append( "Correo Enviado!" );
        });
    }
</script>
<script>
    <?php $param="bl_number=".$bl; ?>
    detalles('<?php echo $param ?>');
</script>


</body>
</html>