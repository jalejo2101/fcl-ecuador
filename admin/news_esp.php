<?php
/**
 * Created by PhpStorm.
 * User: JuanJosé
 * Date: 07/08/14
 * Time: 09:17 AM
 */
include_once("Consultas.php");
$con=new Consultas();

$id=0;
$$news=null;
if($_GET["id"]!=null){
    $id=$_GET["id"];
    $news=$con->get_noticias($id,"_esp");
    $imagen=$news["imagen"];
    $titulo=$news["titulo"];
    $texto=$news["texto"];
    $fecha=$news["fecha"];
    $activo=($news["activo"]!=null)?1:0;
}


?>
<!DOCTYPE html>
<html>
<head>
    <?php include("head.php");?>
    <title></title>
</head>
<body>
<div class="row">
    <div class="col-md-7 col-md-offset-3">
        <?php if($id==0){ ?>
            <h3>Insercion de Noticias</h3>
        <?php }else{?>
            <h3>Modificacion de Noticias</h3>
        <?php } ?>
    </div>
</div>

<div class="row">
    <div class="col-xs-2 col-xs-offset-1">
        <?php $op=6 ?>
        <?php include_once("menu.php")?>
    </div>
    <div class="col-xs-8">

        <form role="form" action="news_list_esp.php" method="post" enctype="multipart/form-data">
            <?php if($id>0){ ?>
            <div class="form-group">
                <label for="id">Id</label>
                <input type="text" class="form-control" id="id" name="id" value="<?php echo $id ?>" readonly>
            </div>
            <?php } ?>

            <div class="form-group">
                <label for="tituloN">Titulo</label>
                <input type="text" class="form-control" id="tituloN" name="tituloN" placeholder="Titulo" value="<?php echo ($id>0)? $titulo:"" ?>" >
            </div>
            <div class="form-group">
                <label for="texto">Texto</label>
                <textarea class="form-control" rows="3" id="texto" name="texto" ><?php echo ($id>0)? $texto:"" ?></textarea>
            </div>

            <div class="form-group">
                <label for="fecha">Fecha</label>
                <input type="date" class="form-control" id="fecha" name="fecha" placeholder="dd / mm / aaaa" value="<?php echo ($id>0)? $fecha:"" ?>" >
            </div>
            <div class="form-group">
                <label for="file">Adjuntar imagen</label>
                <input type="file" id="file" name="file">
                <?php echo ($id>0)? "<help>".$news["imagen"]."</help>":""?>
            </div>
            <div class="checkbox">
                <label>
                    <input name="activo" type="checkbox" <?php echo ($id>0 && $news["activo"]==1)? "checked":""?> > Activo
                </label>
            </div>
            <button type="submit" class="btn btn-default">Enviar</button>
            <input type="hidden" name="modo" value="<?php echo ($id==0)? "new":"update"?>">
        </form>
    </div>
</div>

</body>
</html>