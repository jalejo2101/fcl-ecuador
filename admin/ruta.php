<?php
/**
 * Created by PhpStorm.
 * User: JuanJosé
 * Date: 07/08/14
 * Time: 09:17 AM
 */
include_once("Consultas.php");
$con=new Consultas();
$paises=$con->get_country_list();

$id=0;
$port=null;
/*
if($_GET["id"]!=null){
    $id=$_GET["id"];
    $port=$con->get_port($id);
    $port=$port[0];
    $codigo=$port["codigo"];
    $tipo=$port["tipo"];
    $nombre=$port["nombre"];
    $ciudad=$port["ciudad"];
    $pais=$port["pais"];
    $activo=($port["estatus"]==1)?1:0;

    echo ">>>".$port["estatus"];
}
*/

?>
<!DOCTYPE html>
<html>
<head>
    <?php include("head.php");?>
    <title></title>
</head>
<body>
<div class="row">
    <div class="col-md-7 col-md-offset-3">
        <?php if($id==0){ ?>
            <h3>Insercion de Ruta</h3>
        <?php }else{?>
            <h3>Modificacion de Ruta</h3>
        <?php } ?>
    </div>
</div>

<div class="row">
    <div class="col-xs-2 col-xs-offset-1">
        <?php $op=15 ?>
        <?php include_once("menu.php")?>
    </div>
    <div class="col-xs-8">
        <form role="form" action="rutas.php" method="post" enctype="multipart/form-data">
            <?php if($id>0){ ?>
            <div class="form-group">
                <label for="id">Id</label>
                <input type="text" class="form-control" id="id" name="id" value="<?php echo $id ?>" readonly>
            </div>
            <?php } ?>

            <div class="row">
                <!------------------------------------- ORIGEN ------------------------------------->
                <div class="col-xs-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading" style="font-size: 16px; font-weight: bold">Origen de la ruta</div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="texto">Pais de Origen</label>
                                <select class="form-control" name="pais_o" id="pais_o">
                                    <option value=""></option>
                                    <?php
                                    foreach($paises as $p){ ?>
                                        <option value="<?php echo $p['pais']?>"><?php echo $p['pais']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group" id="ct_origen">
                                <label for="texto">Ciudad</label>
                                <select class="form-control" name="ciudad_o" id="ciudad_o" disabled>
                                </select>
                            </div>

                            <div class="form-group" id="pt_origen">
                                <label for="texto">Puerto</label>
                                <select  class="form-control" name="puerto_o" id="puerto_o" disabled>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <!------------------------------------- DESTINO ------------------------------------->
                <div class="col-xs-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading" style="font-size: 16px; font-weight: bold">Destino de la ruta</div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="texto">Pais de Destino</label>
                                <select class="form-control" name="pais_d" id="pais_d">
                                    <option value=""></option>
                                    <?php
                                    foreach($paises as $p){ ?>
                                        <option value="<?php echo $p['pais']?>"><?php echo $p['pais']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group" id="ct_destino">
                                <label for="texto">Ciudad</label>
                                <select class="form-control" name="ciudad_d" id="ciudad_d" disabled>
                                </select>
                            </div>

                            <div class="form-group" id="pt_destino">
                                <label for="texto">Puerto</label>
                                <select  class="form-control" name="puerto_d" id="puerto_d" disabled>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="form-group">
                <label for="texto">Tipo de Envio</label>
                <select class="form-control" name="tipo_ruta" id="tipo_puerto">
                    <option value="Maritimo" selected>Maritimo</option>
                    <option value="Aereo">Aereo</option>
                </select>
            </div>

            <?php  /*
            <div class="checkbox">
                <label>
                    <input name="activo" type="checkbox" <?php echo ($id>0 && $activo==1)? "checked":""?> > Activo
                </label>
            </div> */ ?>
            <button type="submit" class="btn btn-default">Enviar</button>
            <input type="hidden" name="modo" value="<?php echo ($id==0)? "new":"update"?>">
        </form>
    </div>
</div>
<script type="text/javascript">
    var p='<?php echo $pais ?>';
    var pt='<?php echo $tipo ?>';
    $(document).ready(function(){
        $("#paises option[value='"+ p + "']").attr('selected', 'selected');
        $("#tipo_puerto option[value='"+ pt + "']").attr('selected', 'selected');
    });

    $("#pais_o").change(function(){
        var p=$("#pais_o option:selected").val();
        $.ajax({
            type: "GET",
            url: "listas.php",
            data: { tp: "1", pais: p}
        }).done(function( html ) {
                $( "#ct_origen").empty().append( html );
        });
        $( "#puerto_o").empty();
    });
    $("#pais_d").change(function(){
        var p=$("#pais_d option:selected").val();

        $.ajax({
            type: "GET",
            url: "listas.php",
            data: { tp: "3", pais: p}
        }).done(function( html ) {
            $( "#ct_destino").empty().append( html );
        });
        $( "#puerto_d").empty();
    });

</script>
</body>
</html>