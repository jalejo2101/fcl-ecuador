
<script>
    var ids=[];
</script>


        <?php include_once("Consultas.php");?>
        <?php include_once("funciones.php");?>
        <?php

        $con=new Consultas();
        $pr_list=$con->get_providers_list();
        $id_ruta=$_GET['id_ruta'];

        if($_GET["modo"]=="new"){
            //echo $_GET["modo"];
            agrega_tarifa_general();
        }else if($_GET["modo"]=="update"){
            modifica_tarifa_general();
        }if($_GET["modo"]=="delete"){
            elimina_tarifa_general();
        }
        ?>
        <?php
            $lista_tarifas=$con->get_tarifas_generales($id_ruta);

            ?>
           <!------------------------------------------------------------>
            <script>
            <?php
            $c=0;
            foreach($lista_tarifas as $item){
            echo "ids[".$c++."]=".$item['id_proveedor'],";";
            } ?>
            </script>
            <!------------------------------------------------------------>
    <div>


            <?php
            if(count($lista_tarifas)>0)
            foreach($lista_tarifas as $item){ ?>
        <table class="table table-condensed table-striped tarifas">
            <thead>
            <tr>
                <td colspan="3" style="text-align: center"><?php lang("Provider","Proveedor") ?></td>
                <td colspan="2" style="text-align: center"><?php lang("Shipping date/Frecuencia","Fecha de salida/Frecuencia") ?></td>
                <!--td style="text-align: center"><?php lang("Frecuencia","Frecuencia")?></td-->
                <td style="text-align: center"><?php lang("Dias limite","Dias limite")?></td>
                <td style="width: 14%; text-align: center"><?php lang("Dias limite doc.","Dias limite doc.")?></td>
            </tr>
            </thead>

            <tbody>
            <tr>
                <td colspan="3"><?php echo $item['compania']?></td>
                <td colspan="2"><?php echo $item['fecha_base']?></td>
                <!--td><?php echo $item['frecueny']?></td-->
                <td><?php echo $item['limit_days']?></td>
                <td><?php echo $item['doc_limit_day']?></td>
            </tr>
            </tbody>

            <thead>
            <tr>
                <td style="width: 16%; text-align: center"><?php lang("Validity","Validez") ?></td>
                <td style="width: 14%; text-align: center"><?php lang("Transit","Transito") ?></td>
                <td style="width: 14%; text-align: center"><?php lang("Import Services","Servicio de importacion")?></td>
                <td style="width: 14%; text-align: center"><?php lang("BL Fee","Procesamiento de BL")?></td>
                <td style="width: 14%; text-align: center"><?php lang("CNTR handling","Manejo de CNTRS")?></td>
                <td style="width: 14%; text-align: center" colspan="2">THCD</td>
            </tr>
            </thead>

            <tbody>
            <tr>
                <td><?php echo $item['validity']?></td>
                <td><?php echo $item['transit']?></td>
                <td><?php echo $item['import_services']?></td>
                <td><?php echo $item['bl_fee']?></td>
                <td><?php echo $item['handling']?></td>
                <td colspan="2"><?php echo $item['thcd']?></td>
            </tr>
            </tbody>

            <thead>
            <tr>
                <td style="text-align: center"><?php lang("D/O","Carta de salida")?></td>
                <td style="text-align: center"><?php lang("Gate In","INSPECCION Contenedor")?></td>
                <td style="text-align: center"><?php lang("Export Services","Servicio de Exportacion")?></td>
                <td style="text-align: center"><?php lang("Gate Out","Entrega de Contendor")?></td>
                <td style="text-align: center"><?php lang("CNTR insurance","Seguro de contenedor")?></td>
                <td style="text-align: center"><?php lang("Other charges","Otros cargos")?></td>
                <td style="text-align: center"><?php lang("Currecny","Currecny")?></td>
            </tr>
            </thead>

            <tbody>
            <tr>
                <td><?php echo $item['d_o']?></td>
                <td><?php echo $item['gate_in']?></td>
                <td><?php echo $item['export_services']?></td>
                <td><?php echo $item['gate_out']?></td>
                <td><?php echo $item['insurance']?></td>
                <td><?php echo $item['other_charges']?></td>
                <td><?php echo ($item['other_charges']=='DOL')?"Dolar":"Euro"; ?></td>
                <!--td>
                    <select class="form-control" name="currecny" id="currecny" style="height: 25px;padding: 0px 5px;">
                        <option value="DOL" <?php echo ($p['currecny']=='DOL')?"Selected":""; ?>>Dolar</option>
                        <option value="EUR" <?php echo ($p['currecny']=='EUR')?"Selected":""; ?>>Euro</option>
                    </select>
                </td-->
            </tr>
            </tbody>

            <thead>
            <tr>
                <td style="text-align: center">Description</td>
                <td style="text-align: center">Basic O.F.</td>
                <td style="text-align: center">BSC</td>
                <td style="text-align: center">ISP</td>
                <td style="text-align: center">THO</td>
                <td style="text-align: center">Other Sub C.</td>
                <td style="text-align: center">Total</td>
            </tr>
            </thead>

            <tbody>
            <tr>
                <td style="text-align: left">Container 20</td>
                <td style="text-align: right"><?php echo $item['precio_20']?></td>
                <td style="text-align: right"><?php echo $item['bsc_20']?></td>
                <td style="text-align: right"><?php echo $item['isp_20']?></td>
                <td style="text-align: right"><?php echo $item['tho_20']?></td>
                <td style="text-align: right"><?php echo $item['other_20']?></td>
                <td style="text-align: right"><?php echo $item['total_20']?></td>
            </tr>
            <tr>
                <td style="text-align: left">Container 40</td>
                <td style="text-align: right"><?php echo $item['precio_40']?></td>
                <td style="text-align: right"><?php echo $item['bsc_40']?></td>
                <td style="text-align: right"><?php echo $item['isp_40']?></td>
                <td style="text-align: right"><?php echo $item['tho_40']?></td>
                <td style="text-align: right"><?php echo $item['other_40']?></td>
                <td style="text-align: right"><?php echo $item['total_40']?></td>
            </tr>
            <tr>
                <td style="text-align: left">Container HR</td>
                <td style="text-align: right"><?php echo $item['precio_hr']?></td>
                <td style="text-align: right"><?php echo $item['bsc_hr']?></td>
                <td style="text-align: right"><?php echo $item['isp_hr']?></td>
                <td style="text-align: right"><?php echo $item['tho_hr']?></td>
                <td style="text-align: right"><?php echo $item['other_hr']?></td>
                <td style="text-align: right"><?php echo $item['total_hr']?></td>
            </tr>

            <tr>
                <td style="text-align: left">Container NOR</td>
                <td style="text-align: right"><?php echo $item['precio_nor']?></td>
                <td style="text-align: right"><?php echo $item['bsc_nor']?></td>
                <td style="text-align: right"><?php echo $item['isp_nor']?></td>
                <td style="text-align: right"><?php echo $item['tho_nor']?></td>
                <td style="text-align: right"><?php echo $item['other_nor']?></td>
                <td style="text-align: right"><?php echo $item['total_nor']?></td>
            </tr>
            </tbody>
            <tbody>
                <tr>
                    <td colspan="6" style="text-align: right"></td>
                    <td style="text-align: right">
                        <img src="img/delete_icon.png"
                             onclick="if(confirm('Desea Eliminar este Proveedor?'))eliminar('<?php echo $item['id']?>')" style="margin-right: 10px">
                        <img src="img/edit_icon.png"
                             onclick="modificar(
                                 '<?php echo $item['id']?>',
                                 '<?php echo $item['id_proveedor']?>',

                                 '<?php echo $item['validity']?>',
                                 '<?php echo $item['transit']?>',
                                 '<?php echo $item['fecha_base']?>',
                                 '<?php echo $item['frecueny']?>',
                                 '<?php echo $item['currecny']?>',
                                 '<?php echo $item['import_services']?>',
                                 '<?php echo $item['bl_fee']?>',
                                 '<?php echo $item['handling']?>',
                                 '<?php echo $item['thcd']?>',
                                 '<?php echo $item['d_o']?>',
                                 '<?php echo $item['gate_in']?>',
                                 '<?php echo $item['export_services']?>',
                                 '<?php echo $item['gate_out']?>',
                                 '<?php echo $item['insurance']?>',
                                 '<?php echo $item['other_charges']?>',
                                 '<?php echo $item['limit_days']?>',
                                 '<?php echo $item['doc_limit_day']?>',

                                 '<?php echo $item['precio_20']?>',
                                 '<?php echo $item['bsc_20']?>',
                                 '<?php echo $item['isp_20']?>',
                                 '<?php echo $item['tho_20']?>',
                                 '<?php echo $item['other_20']?>',

                                 '<?php echo $item['precio_40']?>',
                                 '<?php echo $item['bsc_40']?>',
                                 '<?php echo $item['isp_40']?>',
                                 '<?php echo $item['tho_40']?>',
                                 '<?php echo $item['other_40']?>',

                                 '<?php echo $item['precio_hr']?>',
                                 '<?php echo $item['bsc_hr']?>',
                                 '<?php echo $item['isp_hr']?>',
                                 '<?php echo $item['tho_hr']?>',
                                 '<?php echo $item['other_hr']?>',

                                 '<?php echo $item['precio_nor']?>',
                                 '<?php echo $item['bsc_nor']?>',
                                 '<?php echo $item['isp_nor']?>',
                                 '<?php echo $item['tho_nor']?>',
                                 '<?php echo $item['other_nor']?>',
                                 '<?php echo $item['currecny']?>')"
                            />
                    </td>
                </tr>
            </tbody>
        </table>
            <?php  } ?>

            <script>
                function eliminar(id){
                    $.ajax({
                        url: "tabla_tarifas_generales.php",
                        type: "GET",
                        data: {id:id,
                                id_ruta:<?php echo $id_ruta ?>,
                                modo:'delete'
                        }
                    }).done(function( html ) {
                        $( "#contenedor").empty().append( html );
                    });
                }

                function modificar(id,idp,validity,transit,fecha_base,frecueny,currecny,import_services,bl_fee,handling,thcd,d_o,
                                   gate_in,export_services,gate_out,insurance,other_charges,limit_days,doc_limit_day,
                                   p20,bsc20,isp20,tho20,oth20,p40,bsc40,isp40,tho40,oth40,phr,bschr,isphr,thohr,othhr,pnor,bscnor,ispnor,thonor,othnor,curr){

                    document.fr.id_tarifa.value=id;
                    document.fr.validity.value=validity;
                    document.fr.transit.value=transit;
                    document.fr.fecha_base.value=fecha_base;
                    //document.fr.frecueny.value=frecueny;
                    document.fr.currecny.value=currecny;
                    document.fr.import_services.value=import_services;
                    document.fr.bl_fee.value=bl_fee;
                    document.fr.handling.value=handling;
                    document.fr.thcd.value=thcd;
                    document.fr.d_o.value=d_o;
                    document.fr.gate_in.value=gate_in;
                    document.fr.export_services.value=export_services;
                    document.fr.gate_out.value=gate_out;
                    document.fr.insurance.value=insurance;
                    document.fr.other_charges.value=other_charges;
                    document.fr.limit_days.value=limit_days;
                    document.fr.doc_limit_day.value=doc_limit_day;

                    document.fr.precio_20.value=p20;
                    document.fr.bsc_20.value=bsc20;
                    document.fr.isp_20.value=isp20;
                    document.fr.tho_20.value=tho20;
                    document.fr.other_20.value=oth20;

                    document.fr.precio_40.value=p40;
                    document.fr.bsc_40.value=bsc40;
                    document.fr.isp_40.value=isp40;
                    document.fr.tho_40.value=tho40;
                    document.fr.other_40.value=oth40;

                    document.fr.precio_hr.value=phr;
                    document.fr.bsc_hr.value=bschr;
                    document.fr.isp_hr.value=isphr;
                    document.fr.tho_hr.value=thohr;
                    document.fr.other_hr.value=othhr;

                    document.fr.precio_nor.value=pnor;
                    document.fr.bsc_nor.value=bscnor;
                    document.fr.isp_nor.value=ispnor;
                    document.fr.tho_nor.value=thonor;
                    document.fr.other_nor.value=othnor;

                    $("#compania").val(idp);
                    $("#currecny").val(curr);
                    $("#cancelar").attr("disabled",false);
                    $("#agregar").attr("disabled",false);

                    $("#compania").attr("disabled",true);

                    edit=1;
                    $(document).scrollTop(400);
                }
            </script>
        </div>
