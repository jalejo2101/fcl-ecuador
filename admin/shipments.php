<?php include_once("Consultas.php");?>
<?php include_once("funciones.php");?>
<?php
/*
if($_POST["modo"]=="new"){
    agrega_usuario();
}else if($_POST["modo"]=="update"){
    modifica_usuario($id);
}if($_POST["modo"]=="delete"){
    elimina_usuario();
}
*/

if(isset($_POST["track"])){
    if($_POST["tipo"]=="air"){
        elimina_air_shipment($_POST["track"]);
    }else{
        elimina_sea_shipment($_POST["track"]);
    }

}

$con=new Consultas();
$u=false;
$id_user="";
if(isset($_GET["id_client"])){
    $lst=$con->get_tracking($_GET["id_client"]);
    $id_user=$_GET["id_client"];
    $u=true;
}else{
    $lst=$con->get_tracking();
}

?>
<!DOCTYPE html>
<html>
<head>
    <?php include("head.php");?>
    <title></title>
    <script>
        function borrar(track,tipo)
        {
            document.fr.track.value=track;
            document.fr.tipo.value=tipo;
            if(confirm("Esta seguro que desea eliminar este Envio? \nTambien se eliminaran los detalles del mismo!")){
                document.fr.submit();
            }
        }
    </script>
</head>
<body>
<div class="row">
    <div class="col-xs-4 col-xs-offset-3">
        <h3>Shippemnt Lists</h3>
    </div>
    <?php if($id_user!=""){ ?>
    <div class="col-xs-2" style="padding-top:15px">
        <button type="button" class="btn btn-primary" style="width: 100%" onclick="window.open('air_shipment.php?modo=new&id_client=<?php echo $_GET["id_client"]?>','_self','')">Add Air Shipment</button>
    </div>
    <div class="col-xs-2" style="padding-top:15px">
        <button type="button" class="btn btn-primary" style="width: 100%" onclick="window.open('sea_shipment.php?modo=new&id_client=<?php echo $_GET["id_client"]?>','_self','')">Add Sea Shipment</button>
    </div>
    <?php } ?>
</div>

<div class="row">
    <div class="col-xs-2 col-xs-offset-1">
        <?php $op=11 ?>
        <?php include_once("menu.php")?>
    </div>
    <div class="col-xs-8">
        <table style="100%">
            <tr>
                <td style="width: 20%">

                </td>
                <td style="width: 25%"></td>
                <td></td>
            </tr>
        </table>
        <table class="table table-hover">
            <thead>
            <tr style="background: #9acfea">
                <td style="width: 15%; text-align: center">Tracking Number</td>
                <td style="width: 20%; text-align: center">Company Name</td>
                <td style="width: 20%; text-align: center">Origin</td>
                <td style="width: 20%; text-align: center">Destination</td>
                <td style="width: 15%; text-align: center">Pick Up Date</td>
                <td style="width: 5%; text-align: center">M</td>
                <td style="width: 5%; text-align: center">E</td>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach($lst as $item){ ?>
            <tr>
                <!--td><?php echo $item['id']?></td-->
                <?php if($item['type']=="air"){?>
                    <td style="text-align: center"><a href="javascript:air_view('<?php echo $item["tracking"]?>')"><?php echo $item['tracking']?></a></td>
                <?php }else{?>
                    <td style="text-align: center"><a href="javascript:sea_view('<?php echo $item["tracking"]?>')"><?php echo $item['tracking']?></a></td>
                <?php } ?>
                <td style="text-align: left"><?php echo $item['company_name']?></td>
                <td style="text-align: left"><?php echo $item['origen']?></td>
                <td style="text-align: center"><?php echo $item['destino']?></td>
                <td style="text-align: center"><?php echo $item['pick_up']?></td>
                <?php if($item['type']=="air"){?>
                    <td style="text-align: center"><a href="air_shipment.php?id_user=<?php echo $id_user ?>&awb_number=<?php echo $item["tracking"]?>"><img src="img/edit_find.png"></a></td>
                    <td style="text-align: center"><a href="javascript:borrar('<?php echo $item["tracking"]?>','air')"><img src="img/delete_icon.png"></a></td>
                <?php }else{ ?>
                    <td style="text-align: center"><a href="sea_shipment.php?id_user=<?php echo $id_user ?>&bl_number=<?php echo $item["tracking"]?>"><img src="img/edit_find.png"></a></td>
                    <td style="text-align: center"><a href="javascript:borrar('<?php echo $item["tracking"]?>','sea')"><img src="img/delete_icon.png"></a></td>
                <?php } ?>
            </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>


<form name="fr" method="post" action="">
    <input type="hidden" name="track">
    <input type="hidden" name="tipo" >
</form>
<script>
    $(document).ready(function(){
        $("input,textarea").css("background-color","white");
    });
</script>
</body>
</html>





