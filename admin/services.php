<?php include_once("Consultas.php");?>
<?php include_once("funciones.php");?>
<?php
if($_POST["modo"]=="new"){
    agrega_servicio();
}else if($_POST["modo"]=="update"){
    modifica_servicio();
}if($_POST["modo"]=="delete"){
    elimina_servicio();
}

$con=new Consultas();
$lst=$con->get_sevices_list();
//$paises=$con->get_country_list();
/*if(isset($_GET["pais"])){
    $lst=$con->get_port_list($_GET["pais"]);
    $p=$_GET["pais"];
}else{

    $p="ECUADOR";
}*/?>

<!DOCTYPE html>
<html>
<head>
    <?php include("head.php");?>
    <title></title>
    <script>
        function borrar(id)
        {
            document.fr.id.value=id;
            if(confirm("Esta seguro que desea eliminar este Servicio?")){
                document.fr.submit();
            }
        }
    </script>
</head>
<body>
<div class="row">
    <div class="col-xs-6 col-xs-offset-3">
        <h3>Listado de Servicios</h3>
    </div>
    <div class="col-xs-2" style="padding-top:15px">
        <button type="button" class="btn btn-primary" style="width: 100%" onclick="window.open('service.php','_self','')">Agregar</button>
    </div>
</div>

<div class="row">
    <div class="col-xs-2 col-xs-offset-1">
        <?php $op=14 ?>
        <?php include_once("menu.php")?>
    </div>
    <div class="col-xs-8">
        <table class="table table-hover">
            <thead>
            <tr style="background: #9acfea">
                <td style="width: 35%">Servicio</td>
                <td style="text-align: left">Descripcion</td>
                <?php /*<td style="width: 20%; text-align: center">Tipo de Servicio</td> */?>
                <td style="width: 7%; text-align: center">Estatus</td>
                <td style="width: 5%; text-align: center">M</td>
                <td style="width: 5%; text-align: center">E</td>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach($lst as $item){ ?>
            <tr>
                <td><?php echo $item['servicio']?></td>
                <td><?php echo $item['descripcion']?></td>
                <?php /* <td style="; text-align: center"><?php echo $item['tipo_servicio'] ?></td> */?>
                <td style="; text-align: center"><input type="checkbox" disabled <?php echo ($item["estatus"]== 1) ? "checked ":"";?>></td>
                <td style="; text-align: center"><a href="service.php?id=<?php echo $item["id"]?>"><img src="img/edit_icon.png"></a></td>
                <td style="; text-align: center"><a href="javascript:borrar('<?php echo $item["id"]?>')"><img src="img/delete_icon.png"></a></td>
            </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<form name="fr" method="post" action="">
    <input type="hidden" name="id">
    <input type="hidden" name="modo" value="delete">
</form>
</body>
</html>





