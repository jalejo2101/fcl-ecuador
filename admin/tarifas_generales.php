<?php
/**
 * Created by PhpStorm.
 * User: JuanJosé
 * Date: 07/08/14
 * Time: 09:17 AM
 */
include_once("funciones.php");
include_once("Consultas.php");
$con=new Consultas();


$id=0;
$port=null;

if($_GET["id"]!=null){
    $id=$_GET["id"];
    $ruta=$con->get_ruta($id)[0];
}

?>
<!DOCTYPE html>
<html>
<head>
    <?php include("head.php");?>
    <title></title>
</head>
<body>
<script>
    var edit=0;
</script>
<div class="row">
    <div class="col-md-7 col-md-offset-3">
        <?php if($id==0){ ?>
            <h3>Tarifas de ruta por Contenedor</h3>
        <?php }else{?>
            <h3>Tarifas de ruta por Contenedor</h3>
        <?php } ?>
    </div>
</div>

<div class="row">
    <div class="col-xs-2 col-xs-offset-1">
        <?php $op=15 ?>
        <?php include_once("menu.php")?>
    </div>
    <div class="col-xs-8">

            <?php if($id>0){ ?>
            <div class="form-group">
                <!--label for="id"></label-->
                <input type="hidden" class="form-control" id="id" name="id" value="<?php echo $id ?>">
            </div>
            <?php  } ?>

            <div class="row">
                <!------------------------------------- ORIGEN ------------------------------------->
                <div class="col-xs-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading" style="font-size: 16px; font-weight: bold">Origen de la ruta!</div>
                        <div class="panel-body ruta">
                            <div class="form-group">
                                <label for="pais_o" class="control-label col-xs-5">Pais de Origen</label>
                                <div class="col-xs-7">
                                    <input type="text" class="form-control" id="pais_o" name="pais_o"  value="<?php echo $ruta['pais_o'] ?>" readonly >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="ciudad_o" class="control-label col-xs-5">Ciudad de Origen</label>
                                <div class="col-xs-7">
                                    <input type="text" class="form-control" id="ciudad_o" name="ciudad_o"  value="<?php echo $ruta['ciudad_o'] ?>" readonly >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="puerto_o" class="control-label col-xs-5">Puerto de Origen</label>
                                <div class="col-xs-7">
                                    <input type="text" class="form-control" id="puerto_o" name="puerto_o"  value="<?php echo $ruta['puerto_o'] ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!------------------------------------- DESTINO ------------------------------------->
                <div class="col-xs-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading" style="font-size: 16px; font-weight: bold">Origen de la ruta!</div>
                        <div class="panel-body ruta">
                            <div class="form-group">
                                <label for="pais_o" class="control-label col-xs-5">Pais de Origen</label>
                                <div class="col-xs-7">
                                    <input type="text" class="form-control" id="pais_o" name="pais_o"  value="<?php echo $ruta['pais_d'] ?>" readonly >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="ciudad_o" class="control-label col-xs-5">Ciudad de Origen</label>
                                <div class="col-xs-7">
                                    <input type="text" class="form-control" id="ciudad_o" name="ciudad_o"  value="<?php echo $ruta['ciudad_d'] ?>" readonly >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="puerto_o" class="control-label col-xs-5">Puerto de Origen</label>
                                <div class="col-xs-7">
                                    <input type="text" class="form-control" id="puerto_o" name="puerto_o"  value="<?php echo $ruta['puerto_d'] ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <div class="form-group">
                            <label for="tipo_ruta">Tipo de Envio</label>
                            <input type="text" class="form-control" id="tipo_ruta" name="tipo_ruta"  value="<?php echo $ruta['tipo_ruta'] ?>" readonly>
                        </div>
                    </div>
                </div>
            </div>
            <!-------------------------------------------------------------------------------->
            <?php
            $pr_list=$con->get_providers_list();
            /*if($_GET["modo"]=="new"){
                agrega_tarifa();
            }else if($_GET["modo"]=="update"){
                //modifica_tarifa();
            }if($_GET["modo"]=="delete"){
                elimina_tarifa();
            }*/
            ?>
            <form class="form-horizontal fr" role="form" name="fr" method="get">


                <table class="table table-condensed" id="t_precios">
                    <thead>
                    <tr>
                        <td colspan="3" style="text-align: center"><?php lang("Provider","Proveedor") ?></td>
                        <td colspan="2" style="text-align: center"><?php lang("Shipping date/Frecuencia","Fecha de salida/Frecuencia") ?></td>
                        <!--td style="text-align: center"><?php lang("Frecuencia","Frecuencia")?></td-->
                        <td style="text-align: center"><?php lang("Dias limite","Dias limite")?></td>
                        <td style="width: 14%; text-align: center"><?php lang("Dias limite doc.","Dias limite documento")?></td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td colspan="3">
                               <select class="form-control" name="compania" id="compania" style="padding: 0px 10px; height: 25px">
                                    <option value="0"></option>
                                    <?php
                                    foreach($pr_list as $p){ ?>
                                        <option value="<?php echo $p['id']?>"><?php echo $p['compania']?></option>
                                    <?php } ?>
                                </select>
                        </td>
                        <td colspan="2"><input type="text" class="form-control input-sm text-right" name="fecha_base" id="fecha_base" placeholder="dd/mm/aaaa:f; dd/mm/aaaa:f; ..." style="line-height: 1.5 !important;">
                        <!--td><input type="text" class="form-control input-sm text-right" name="frecueny" id="frecueny" placeholder="n; n; n..."></td-->
                        <td><input type="text" class="form-control input-sm text-right val" name="limit_days"></td>
                        <td><input type="text" class="form-control input-sm text-right val" name="doc_limit_day"></td>
                    </tr>
                    </tbody>

                    <thead>
                    <tr>
                        <td style="width: 16%; text-align: center"><?php lang("Validity","Validez") ?></td>
                        <td style="width: 14%; text-align: center"><?php lang("Transit","Transito") ?></td>
                        <td style="width: 14%; text-align: center"><?php lang("Import Services","Servicio de importacion")?></td>
                        <td style="width: 14%; text-align: center"><?php lang("BL Fee","Procesamiento de BL")?></td>
                        <td style="width: 14%; text-align: center"><?php lang("CNTR handling","Manejo de CNTRS")?></td>
                        <td style="width: 14%; text-align: center" colspan="2">THCD</td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><input type="text" class="form-control input-sm text-right " name="validity"></td>
                        <td><input type="text" class="form-control input-sm text-right " name="transit"></td>
                        <td><input type="text" class="form-control input-sm text-right val" name="import_services"></td>
                        <td><input type="text" class="form-control input-sm text-right " name="bl_fee"></td>
                        <td><input type="text" class="form-control input-sm text-right " name="handling"></td>
                        <td colspan="2"><input type="text" class="form-control input-sm text-right val" name="thcd"></td>
                    </tr>
                    </tbody>
                    <thead>
                    <tr>
                        <td style="text-align: center"><?php lang("D/O","Carta de salida")?></td>
                        <td style="text-align: center"><?php lang("Gate In","INSPECCION Contenedor")?></td>
                        <td style="text-align: center"><?php lang("Export Services","Servicio de Exportacion")?></td>
                        <td style="text-align: center"><?php lang("Gate Out","Entrega de Contendor")?></td>
                        <td style="text-align: center"><?php lang("CNTR insurance","Seguro de contenedor")?></td>
                        <td style="text-align: center"><?php lang("Other charges","Otros cargos")?></td>
                        <td style="text-align: center"><?php lang("Currecny","Currecny")?></td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><input type="text" class="form-control input-sm text-right " name="d_o"></td>
                        <td><input type="text" class="form-control input-sm text-right " name="gate_in"></td>
                        <td><input type="text" class="form-control input-sm text-right val" name="export_services"></td>
                        <td><input type="text" class="form-control input-sm text-right " name="gate_out"></td>
                        <td><input type="text" class="form-control input-sm text-right " name="insurance"></td>
                        <td><input type="text" class="form-control input-sm text-right val" name="other_charges"></td>
                        <td>
                            <select class="form-control" name="currecny" id="currecny" style="height: 25px;padding: 0px 5px;">
                                <option value="DOL" <?php echo ($p['currecny']=='DOL')?"Selected":""; ?>>Dolar</option>
                                <option value="EUR" <?php echo ($p['currecny']=='EUR')?"Selected":""; ?>>Euro</option>
                            </select>
                        </td>
                    </tr>
                    </tbody>
                    <thead>
                    <tr>
                        <td style="text-align: center">Description</td>
                        <td style="text-align: center">Basic O.F.</td>
                        <td style="text-align: center">BSC</td>
                        <td style="text-align: center">ISP</td>
                        <td style="text-align: center">THO</td>
                        <td style="text-align: center">Other Sub C.</td>
                        <td style="text-align: center">Total</td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <input type="hidden" name="id_tarifa" value="0">
                            Container 20
                        </td>
                        <td><input type="text" class="form-control input-sm text-right val" name="precio_20"></td>
                        <td><input type="text" class="form-control input-sm text-right val" name="bsc_20"></td>
                        <td><input type="text" class="form-control input-sm text-right val" name="isp_20"></td>
                        <td><input type="text" class="form-control input-sm text-right val" name="tho_20"></td>
                        <td><input type="text" class="form-control input-sm text-right val" name="other_20"></td>
                        <td><input type="text" class="form-control input-sm text-right val" name="total_20"></td>
                    </tr>
                    <tr>
                        <td>Container 40</td>
                        <td><input type="text" class="form-control input-sm text-right val" name="precio_40"></td>
                        <td><input type="text" class="form-control input-sm text-right val" name="bsc_40"></td>
                        <td><input type="text" class="form-control input-sm text-right val" name="isp_40"></td>
                        <td><input type="text" class="form-control input-sm text-right val" name="tho_40"></td>
                        <td><input type="text" class="form-control input-sm text-right val" name="other_40"></td>
                        <td><input type="text" class="form-control input-sm text-right val" name="total_40"></td>
                    </tr>
                    <tr>
                        <td>Container HR</td>
                        <td><input type="text" class="form-control input-sm text-right val" name="precio_hr"></td>
                        <td><input type="text" class="form-control input-sm text-right val" name="bsc_hr"></td>
                        <td><input type="text" class="form-control input-sm text-right val" name="isp_hr"></td>
                        <td><input type="text" class="form-control input-sm text-right val" name="tho_hr"></td>
                        <td><input type="text" class="form-control input-sm text-right val" name="other_hr"></td>
                        <td><input type="text" class="form-control input-sm text-right val" name="total_hr"></td>
                    </tr>
                    <tr>
                        <td>Container NOR</td>
                        <td><input type="text" class="form-control input-sm text-right val" name="precio_nor"></td>
                        <td><input type="text" class="form-control input-sm text-right val" name="bsc_nor"></td>
                        <td><input type="text" class="form-control input-sm text-right val" name="isp_nor"></td>
                        <td><input type="text" class="form-control input-sm text-right val" name="tho_nor"></td>
                        <td><input type="text" class="form-control input-sm text-right val" name="other_nor"></td>
                        <td><input type="text" class="form-control input-sm text-right val" name="total_nor"></td>
                    </tr>
                    <tr>
                        <td colspan="7">
                            <button type="button" class="btn btn-success btn-sm" id="agregar">Guardar</button>

                                <button type="button" class="btn btn-success" id="cancelar" disabled>Cancelar Edición</button>
                        </td>
                    </tr>

                    </tbody>
                </table>
                <input type="hidden" id="modo"  name="modo" >
            </form>

            <div class="row">
                <div class="col-xs-12" id="contenedor">

                </div>
            </div>



                <!-------------------------------------------------------------------------------->
                <?php  /*
                <div class="checkbox">
                    <label>
                        <input name="activo" type="checkbox" <?php echo ($id>0 && $activo==1)? "checked":""?> > Activo
                    </label>
                </div>
                <button type="submit" class="btn btn-default">Enviar</button>
                <input type="hidden" name="modo" value="<?php echo ($id==0)? "new":"update"?>">*/ ?>

        </div>
    </div>
    <script type="text/javascript">



    $(document).ready(function(){
        $(".val ").each(function(){
            $(this).val('0,00');
        });
        $(".val ").click(function(){
            if($(this).val()=='0,00'){
                $(this).val('');
            }
        });
        $(".val ").focusout(function(){
            if($(this).val()=='' || $(this).val()=='0'){
                $(this).val('0,00')
            }
            var v=parseFloat($(this).val());
            if(isNaN(v)){
                //alert('r');
                $(this).val('0,00');
            }
        });


        $.ajax({
            url: "tabla_tarifas_generales.php",
            type: "GET",
            data: {id_ruta:'<?php echo $id ?>'}
        }).done(function( html ) {
            $( "#contenedor").empty().append( html );
        });

    });


    $("#agregar").click(function(){
        if(document.fr.id_tarifa.value=="0"){
            //alert(document.fr.id_tarifa.value);
            agrega();
        }else{
            //alert("M"+document.fr.id_tarifa.value);
            modifica();
        }
    });

    function agrega(){
        var idpr=$("#compania option:selected").val();
        var exist=0;
        for(i=0; i<ids.length;i++){
            if(ids[i]==idpr){
                //alert(ids.length+"-"+ids[i]+ "-" +idpr);
                exist=1;
            }
        }
        if($("#compania option:selected").val()!='0' && exist==0){

        $.ajax({
            url: "tabla_tarifas_generales.php",
            type: "GET",
            data: {
                id_ruta:<?php echo $id ?> ,
                id_proveedor:idpr,
                validity:document.fr.validity.value,
                transit:document.fr.transit.value,
                fecha_base:document.fr.fecha_base.value,
                //frecueny:document.fr.frecueny.value,
                currecny:$("#currecny").val(),
                import_services:document.fr.import_services.value,
                bl_fee:document.fr.bl_fee.value,
                handling:document.fr.handling.value,
                thcd:document.fr.thcd.value,
                d_o:document.fr.d_o.value,
                gate_in:document.fr.gate_in.value,
                export_services:document.fr.export_services.value,
                gate_out:document.fr.gate_out.value,
                insurance:document.fr.insurance.value,
                other_charges:document.fr.other_charges.value,
                limit_days:document.fr.limit_days.value,
                doc_limit_day:document.fr.doc_limit_day.value,

                precio_20:document.fr.precio_20.value,
                bsc_20:document.fr.bsc_20.value,
                isp_20:document.fr.isp_20.value,
                tho_20:document.fr.tho_20.value,
                other_20:document.fr.other_20.value,
                precio_40:document.fr.precio_40.value,
                bsc_40:document.fr.bsc_40.value,
                isp_40:document.fr.isp_40.value,
                tho_40:document.fr.tho_40.value,
                other_40:document.fr.other_40.value,
                precio_hr:document.fr.precio_hr.value,
                bsc_hr:document.fr.bsc_hr.value,
                isp_hr:document.fr.isp_hr.value,
                tho_hr:document.fr.tho_hr.value,
                other_hr:document.fr.other_hr.value,
                precio_nor:document.fr.precio_nor.value,
                bsc_nor:document.fr.bsc_nor.value,
                isp_nor:document.fr.isp_nor.value,
                tho_nor:document.fr.tho_nor.value,
                other_nor:document.fr.other_nor.value,
                modo:'new'}
        }).done(function( html ) {
            document.fr.validity.value="";
            document.fr.transit.value="";
            document.fr.fecha_base.value="";
          //  document.fr.frecueny.value="0";
            document.fr.import_services.value="0,00";
            document.fr.bl_fee.value="";
            document.fr.handling.value="";
            document.fr.thcd.value="";
            document.fr.d_o.value="";
            document.fr.gate_in.value="";
            document.fr.export_services.value="0,00";
            document.fr.gate_out.value="";
            document.fr.insurance.value="";
            document.fr.other_charges.value="0,00";
            document.fr.limit_days.value="0";
            document.fr.doc_limit_day.value="0";

            document.fr.precio_20.value="0,00";
            document.fr.bsc_20.value="0,00";
            document.fr.isp_20.value="0,00";
            document.fr.tho_20.value="0,00";
            document.fr.other_20.value="0,00";
            document.fr.precio_40.value="0,00";
            document.fr.bsc_40.value="0,00";
            document.fr.isp_40.value="0,00";
            document.fr.tho_40.value="0,00";
            document.fr.other_40.value="0,00";
            document.fr.precio_hr.value="0,00";
            document.fr.bsc_hr.value="0,00";
            document.fr.isp_hr.value="0,00";
            document.fr.tho_hr.value="0,00";
            document.fr.other_hr.value="0,00";
            document.fr.precio_nor.value="0,00";
            document.fr.bsc_nor.value="0,00";
            document.fr.isp_nor.value="0,00";
            document.fr.tho_nor.value="0,00";
            document.fr.other_20.value="0,00";
            $("#compania").val(0);
            $( "#contenedor").empty().append( html );
        });
        }else{
            if(exist==1){
                alert("Este proveedor ya tiene una tarifa asignada para esta ruta!");
            }else{
                alert("Debe indicar un proveedor");
            }
        }

    }

    function modifica(){
        var idpr=$("#compania option:selected").val();
        var exist=0;

        if($("#compania option:selected").val()!='0' && exist==0){
            $.ajax({
                url: "tabla_tarifas_generales.php",
                type: "GET",
                data: {
                    id_tarifa:document.fr.id_tarifa.value,
                    id_ruta:<?php echo $id?>,

                    validity:document.fr.validity.value,
                    transit:document.fr.transit.value,
                    fecha_base:document.fr.fecha_base.value,
                 //   frecueny:document.fr.frecueny.value,
                    import_services:document.fr.import_services.value,
                    bl_fee:document.fr.bl_fee.value,
                    handling:document.fr.handling.value,
                    thcd:document.fr.thcd.value,
                    d_o:document.fr.d_o.value,
                    gate_in:document.fr.gate_in.value,
                    export_services:document.fr.export_services.value,
                    gate_out:document.fr.gate_out.value,
                    insurance:document.fr.insurance.value,
                    other_charges:document.fr.other_charges.value,
                    limit_days:document.fr.limit_days.value,
                    doc_limit_day:document.fr.doc_limit_day.value,

                    precio_20:document.fr.precio_20.value,
                    bsc_20:document.fr.bsc_20.value,
                    isp_20:document.fr.isp_20.value,
                    tho_20:document.fr.tho_20.value,
                    other_20:document.fr.other_20.value,
                    precio_40:document.fr.precio_40.value,
                    bsc_40:document.fr.bsc_40.value,
                    isp_40:document.fr.isp_40.value,
                    tho_40:document.fr.tho_40.value,
                    other_40:document.fr.other_40.value,
                    precio_hr:document.fr.precio_hr.value,
                    bsc_hr:document.fr.bsc_hr.value,
                    isp_hr:document.fr.isp_hr.value,
                    tho_hr:document.fr.tho_hr.value,
                    other_hr:document.fr.other_hr.value,
                    precio_nor:document.fr.precio_nor.value,
                    bsc_nor:document.fr.bsc_nor.value,
                    isp_nor:document.fr.isp_nor.value,
                    tho_nor:document.fr.tho_nor.value,
                    other_nor:document.fr.other_nor.value,
                    currecny:$("#currecny").val(),
                    modo:'update'}
            }).done(function( html ) {
                document.fr.validity.value="";
                document.fr.transit.value="";
                document.fr.fecha_base.value="";
             //   document.fr.frecueny.value="0";
                document.fr.import_services.value="0,00";
                document.fr.bl_fee.value="";
                document.fr.handling.value="";
                document.fr.thcd.value="";
                document.fr.d_o.value="";
                document.fr.gate_in.value="";
                document.fr.export_services.value="0,00";
                document.fr.gate_out.value="";
                document.fr.insurance.value="";
                document.fr.other_charges.value="0,00";
                document.fr.limit_days.value="0";
                document.fr.doc_limit_day.value="0";

                document.fr.precio_20.value="0,00";
                document.fr.bsc_20.value="0,00";
                document.fr.isp_20.value="0,00";
                document.fr.tho_20.value="0,00";
                document.fr.other_20.value="0,00";
                document.fr.precio_40.value="0,00";
                document.fr.bsc_40.value="0,00";
                document.fr.isp_40.value="0,00";
                document.fr.tho_40.value="0,00";
                document.fr.other_40.value="0,00";
                document.fr.precio_hr.value="0,00";
                document.fr.bsc_hr.value="0,00";
                document.fr.isp_hr.value="0,00";
                document.fr.tho_hr.value="0,00";
                document.fr.other_hr.value="0,00";
                document.fr.precio_nor.value="0,00";
                document.fr.bsc_nor.value="0,00";
                document.fr.isp_nor.value="0,00";
                document.fr.tho_nor.value="0,00";
                document.fr.other_20.value="0,00";
                $("#compania").val(0);
                $("#cancelar").attr("disabled",true);
                $("#compania").attr("disabled",false);
                edit=0;

                $("#contenedor").empty().append(html);
            });
        }
    }





    $("#cancelar").click(function(){
        document.fr.validity.value="";
        document.fr.transit.value="";
        document.fr.fecha_base.value="";
        //document.fr.frecueny.value="0";
        document.fr.import_services.value="0,00";
        document.fr.bl_fee.value="";
        document.fr.handling.value="";
        document.fr.thcd.value="";
        document.fr.d_o.value="";
        document.fr.gate_in.value="";
        document.fr.export_services.value="0,00";
        document.fr.gate_out.value="";
        document.fr.insurance.value="";
        document.fr.other_charges.value="0,00";
        document.fr.limit_days.value="0";
        document.fr.doc_limit_day.value="0";
        document.fr.precio_20.value="0,00";
        document.fr.bsc_20.value="0,00";
        document.fr.isp_20.value="0,00";
        document.fr.tho_20.value="0,00";
        document.fr.other_20.value="0,00";
        document.fr.precio_40.value="0,00";
        document.fr.bsc_40.value="0,00";
        document.fr.isp_40.value="0,00";
        document.fr.tho_40.value="0,00";
        document.fr.other_40.value="0,00";
        document.fr.precio_hr.value="0,00";
        document.fr.bsc_hr.value="0,00";
        document.fr.isp_hr.value="0,00";
        document.fr.tho_hr.value="0,00";
        document.fr.other_hr.value="0,00";
        document.fr.precio_nor.value="0,00";
        document.fr.bsc_nor.value="0,00";
        document.fr.isp_nor.value="0,00";
        document.fr.tho_nor.value="0,00";
        document.fr.other_20.value="0,00";
        $("#currecny").val("DOL");
        $("#cancelar").attr("disabled",true);
        $("#compania").attr("disabled",false);
        $("#agregar").attr("disabled",false);
        $("#compania").val(0);
            edit=0;


    })


</script>
</body>
</html>