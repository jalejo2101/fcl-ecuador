<?php
/**
 * Created by PhpStorm.
 * User: JuanJosé
 * Date: 07/08/14
 * Time: 09:17 AM
 */
include_once("Consultas.php");
$con=new Consultas();
$paises=$con->get_country_list();


$id=0;
$port=null;
$lst_paises="";
if($_GET["id"]!=null){
    $id=$_GET["id"];
    $prov=$con->get_provider($id);
    $prov=$prov[0];
    $codigo=$prov["codigo"];
    $compania=$prov["compania"];
    $direccion=$prov["direccion"];
    $telefono=$prov["telefono"];
    $email=$prov["email"];
    $pic=$prov["pic"];
    $servicios=str_split($prov["servicios_tma"]);
    $activo=($prov["estatus"]==1)?1:0;
    $lst_paises=$prov["paises"];
    /*$export_services=$prov["export_services"];
    $import_services=$prov["import_services"];
    $admin_charges=$prov["admin_charges"];
    $d_o=$prov["d_o"];
    $seal=$prov["seal"];
    $gate_in=$prov["gate_in"];
    $gate_out=$prov["gate_out"];
    $issuing=$prov["issuing"];
    $doc_fee=$prov["doc_fee"];
    $other_charges=$prov["other_charges"];
*/

    //echo ">>>".$port["estatus"];
}


?>
<!DOCTYPE html>
<html>
<head>
    <?php include("head.php");?>
    <title></title>
    <script>
        var paises= new Array();
        paises[0]=new Array();
        paises[1]=new Array();
        var lista;
    </script>

</head>
<body>
<div class="row">
    <div class="col-md-7 col-md-offset-3">
        <?php if($id==0){ ?>
            <h3>Insercion de Proveedor</h3>
        <?php }else{?>
            <h3>Modificacion de Proveedor</h3>
        <?php } ?>
    </div>
</div>

<div class="row">
    <div class="col-xs-2 col-xs-offset-1">
        <?php $op=13 ?>
        <?php include_once("menu.php")?>
    </div>
    <div class="col-xs-8">

        <form role="form" action="providers.php" method="post" enctype="multipart/form-data" name="fr">
            <?php if($id>0){ ?>
            <div class="form-group">
                <label for="id">Id</label>
                <input type="text" class="form-control" id="id" name="id" value="<?php echo $id ?>" readonly>
            </div>
            <?php } ?>

            <!-------------------------------------------------------------------------->
            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="localidad">Compañia</label>
                        <input type="text" class="form-control" id="compania" name="compania" placeholder="Compañia" value="<?php echo ($id>0)? $compania:"" ?>" >
                    </div>

                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="nombre">Telefono</label>
                        <input type="text" class="form-control" id="telefono" name="telefono" placeholder="Telefono" value="<?php echo ($id>0)? $telefono:"" ?>" >
                    </div>
                </div>
            </div>
            <!-------------------------------------------------------------------------->
            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="nombre">Direccion</label>
                        <input type="text" class="form-control" id="direccion" name="direccion" placeholder="Direccion" value="<?php echo ($id>0)? $telefono:"" ?>" >
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="fecha">Email</label>
                        <input type="text" class="form-control" id="email" name="email" placeholder="Email" value="<?php echo ($id>0)? $email:"" ?>" >
                    </div>
                </div>
            </div>
            <!-------------------------------------------------------------------------->
            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="fecha">PIC</label>
                        <input type="text" class="form-control" id="pic" name="pic" placeholder="PIC" value="<?php echo ($id>0)? $pic:"" ?>" >
                    </div>
                </div>
                <?php /*
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="export_services">Export Services</label>
                        <input type="text" class="form-control" id="export_services" name="export_services" placeholder="Export Services" value="<?php echo ($id>0)? $export_services:"" ?>" >
                    </div>
                </div>
                */?>
            </div>
            <!-------------------------------------------------------------------------->
            <?php /*

            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="import_services">Import Services</label>
                        <input type="text" class="form-control" id="import_services" name="import_services" placeholder="Import Services" value="<?php echo ($id>0)? $import_services:"" ?>" >
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="admin_charges">Admin Charges</label>
                        <input type="text" class="form-control" id="admin_charges" name="admin_charges" placeholder="Admin Charges" value="<?php echo ($id>0)? $admin_charges:"" ?>" >
                    </div>
                </div>
            </div>
            <!-------------------------------------------------------------------------->
            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="d_o">D/O</label>
                        <input type="text" class="form-control" id="d_o" name="d_o" placeholder="D/O" value="<?php echo ($id>0)? $d_o:"" ?>" >
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="seal">Seal</label>
                        <input type="text" class="form-control" id="seal" name="seal" placeholder="Seal" value="<?php echo ($id>0)? $seal:"" ?>" >
                    </div>
                </div>
            </div>
            <!-------------------------------------------------------------------------->
            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="gate_in">Gate In</label>
                        <input type="text" class="form-control" id="gate_in" name="gate_in" placeholder="Gate In" value="<?php echo ($id>0)? $gate_in:"" ?>" >
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="gate_out">Gate Out</label>
                        <input type="text" class="form-control" id="gate_out" name="gate_out" placeholder="Gate Out" value="<?php echo ($id>0)? $gate_out:"" ?>" >
                    </div>
                </div>
            </div>
            <!-------------------------------------------------------------------------->
            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="issuing">Issuing</label>
                        <input type="text" class="form-control" id="issuing" name="issuing" placeholder="Issuing" value="<?php echo ($id>0)? $issuing:"" ?>" >
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="doc_fee">Doc Fee</label>
                        <input type="text" class="form-control" id="doc_fee" name="doc_fee" placeholder="Doc Fee" value="<?php echo ($id>0)? $doc_fee:"" ?>" >
                    </div>
                </div>
            </div>
            <!-------------------------------------------------------------------------->
            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="other_charges">Other Charges</label>
                        <input type="text" class="form-control" id="other_charges" name="other_charges" placeholder="Other Charges" value="<?php echo ($id>0)? $other_charges:"" ?>" >
                    </div>
                </div>
            </div>
            */?>
<hr>







            <div class="form-group">
                <label for="fecha">Servicios</label>
                <div class="checkbox">
                    <label><input type="checkbox" name="chk2" id="chk2" <?php echo ($id>0 && $servicios[1]=="1")?"checked" :"" ?>>Terrestre</label>
                </div>
                <div class="checkbox">
                    <label><input type="checkbox" name="chk1" id="chk1" <?php echo ($id>0 && $servicios[0]=="1")?"checked" :"" ?>>Maritimo</label>
                </div>
                <div class="checkbox">
                    <label><input type="checkbox" name="chk4" id="chk4" <?php echo ($id>0 && $servicios[3]=="1")?"checked" :"" ?>>LCL</label>
                </div>
                <div class="checkbox">
                    <label><input type="checkbox" name="chk3" id="chk3" <?php echo ($id>0 && $servicios[2]=="1")?"checked" :"" ?>>Aereo</label>
                </div>
            </div>
            <hr>





            <div class="row">
                <div class="col-lg-5">
                    Presta servicios en:
                    <select multiple class="form-control" size="8" name="lista_servicios" id="lista_servicios" style="height: 180px; font-weight: bold">



                    </select>
                </div>
                <div class="col-lg-2">

                </div>
                <div class="col-lg-5">
                    Agregar Pais:
                    <select multiple class="form-control" size="8" name="lista_paises" id="lista_paises" style="height: 180px">
                    </select>
                </div>
            </div>
<?php /*
            <div class="form-group">
                <label for="texto">Tipo de Puerto</label>
                <select class="form-control" name="tipo_puerto" id="tipo_puerto">
                    <option value="Puerto">Puerto</option>
                    <option value="Aeropuerto">Aeropuerto</option>
                </select>
            </div>
*/ ?>
            <hr>
            <div class="checkbox">
                <label>
                    <input name="activo" type="checkbox" <?php echo ($id>0 && $activo==1)? "checked":""?> > Activo
                </label>
            </div>
            <input type="hidden" name="servicios">
            <input type="hidden" name="paises">

            <button type="button" onclick="enviar()" class="btn btn-default">Enviar</button>
            <input type="hidden" name="modo" value="<?php echo ($id==0)? "new":"update"?>">
        </form>
    </div>
</div>


<script>
    function enviar(){
        srv="";
        if($("#chk1").is(":checked")){
            srv=srv +"1";
        }else{
            srv=srv +"0";
        }
        if($("#chk2").is(":checked")){
            srv=srv +"1";
        }else{
            srv=srv +"0";
        }
        if($("#chk3").is(":checked")){
            srv=srv +"1";
        }else{
            srv=srv +"0";
        }
        var cad="";
        $("#lista_servicios option").each(function(){
            cad=cad+$(this).val()+";";
        });


        //alert(cad);

        document.fr.paises.value=cad;
        document.fr.servicios.value=srv;
        document.fr.submit();
    }
    function bus(id){
        b=false;
        //alert(eval(id)+"="+lista[i]);

        for(j=0; j<lista.length;j++){
            if(eval(id)==eval(lista[j])){
                b=true;
            }
        }
        //alert (b);
        return b;
    }

    function llenar(){
        co=0;
        $("#lista_paises").empty();

        for(i=0; i<paises[0].length;i++){
            if(!bus(paises[0][i])){
                $("#lista_paises").append("<option value='"+paises[0][i]+"'>"+paises[1][i]+"</option>");
           }
        }
        for(i=0; i<paises[0].length;i++){
            if(bus(paises[0][i])){
            $("#lista_servicios").append("<option value='"+paises[0][i]+"'>"+paises[1][i]+"</option>");
            }
        }


    }




</script>
<script type="text/javascript">

    $(document).ready(function(){
        //   $("#paises option[value='"+ p + "']").attr('selected', 'selected');
        //   $("#tipo_puerto option[value='"+ pt + "']").attr('selected', 'selected');

        <?php
            $lista=$con->get_lista_paises();

            $c=0;
            foreach($lista as $lst){
                echo "paises[0][".$c."]='".$lst["id"]."';";
                echo "paises[1][".$c."]='".$lst["pais"]."';\n\t\t";
                $c++;
            }
            echo 'lista="'.$lst_paises.'".split(";");';
        ?>

        llenar();
        $("#lista_servicios").change(function(){
            var lsv=$("#lista_servicios>option:selected");
            $("#lista_paises").append("<option value='"+lsv.val()+"'>"+lsv.text()+"</option>");
            $("#lista_servicios :selected").remove();
        });

        $("#lista_paises").change(function(){
            var lsv=$("#lista_paises>option:selected");
            $("#lista_servicios").append("<option value='"+lsv.val()+"'>"+lsv.text()+"</option>");
            $("#lista_paises :selected").remove();
        });


    });


</script>
</body>
</html>