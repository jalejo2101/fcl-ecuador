<?php
error_reporting(0);

include("DBManager.php");

class Consultas extends DBManager{
    var $con;
    function  Consultas(){
        $this->con=new DBManager;
    }
    /*public  function links(){
        if($this->con->conectar()==true){}
    }*/


    /***********************************/
    /*lista de imagenes Banner         */
    /***********************************/
    function get_lst_Banner_Big($lg=""){
        if($this->con->conectar()==true){
            $result=mysql_query("SELECT * FROM banner_big".$lg);
            while( $row=mysql_fetch_assoc($result))
            {
                $this->listab[]=$row ;
            }
            return $this->listab;
        }
    }
    function get_lst_Banner_Big_activo($lg=""){
        if($this->con->conectar()==true){
            $result=mysql_query("SELECT * FROM banner_big".$lg." WHERE activo=1");
            while( $row=mysql_fetch_assoc($result))
            {
                $this->listab[]=$row ;
            }
            return $this->listab;
        }
    }

    function get_Banner_Big($id,$lg=""){
        if($this->con->conectar()==true){
            $result=mysql_query("SELECT * FROM banner_big".$lg." WHERE id=$id");
            $row=mysql_fetch_assoc($result);
            return $row;
        }
    }


    /***********************************/
    /*lista de imagenes Banner pequeño */
    /***********************************/
    function get_lst_Banner_Small(){
        if($this->con->conectar()==true){
            $result=mysql_query("SELECT * FROM banner_small");
            while( $row=mysql_fetch_assoc($result))
            {
                $this->listas[]=$row ;
            }
            return $this->listas;
        }
    }
    function get_lst_Banner_Small_activo(){
        if($this->con->conectar()==true){
            $result=mysql_query("SELECT * FROM banner_small WHERE activo=1");
            while( $row=mysql_fetch_assoc($result))
            {
                $this->listas[]=$row ;
            }
            return $this->listas;
        }
    }

    function get_Banner_Small($id){
        if($this->con->conectar()==true){
            $result=mysql_query("SELECT * FROM banner_small WHERE id=$id");
            $row=mysql_fetch_assoc($result);
            return $row;
        }
    }



    /***********************************/
    /*lista de Noticias                */
    /***********************************/
    function get_lst_noticias($lg=""){
        if($this->con->conectar()==true){
            $result=mysql_query("SELECT * FROM news".$lg);
            while( $row=mysql_fetch_assoc($result))
            {
                $this->listan[]=$row ;
            }
            return $this->listan;
        }
    }
    function get_lst_noticias_activo($lg=""){
        if($this->con->conectar()==true){
            $result=mysql_query("SELECT * FROM news".$lg." WHERE activo=1 ORDER BY fecha DESC");
            while( $row=mysql_fetch_assoc($result))
            {
                $this->listan[]=$row ;
            }
            return $this->listan;
        }
    }

    function get_noticias($id,$lg=""){
        if($this->con->conectar()==true){
            $result=mysql_query("SELECT * FROM news".$lg." WHERE id=$id");
            $row=mysql_fetch_assoc($result);
            return $row;
        }
    }


    /***********************************/
    /*lista de Links                   */
    /***********************************/
    function get_lst_links($lg=""){
        if($this->con->conectar()==true){
            $result=mysql_query("SELECT * FROM link_footer".$lg);
            while( $row=mysql_fetch_assoc($result))
            {
                $this->lista_links[]=$row ;
            }
            return $this->lista_links;
        }
    }

    function get_lst_grupo($grupo,$lg=""){
        $this->lista=null ;
        if($this->con->conectar()==true){
            $result=mysql_query("SELECT * FROM link_footer".$lg." WHERE activo=1 AND grupo='$grupo'");
            while( $row=mysql_fetch_assoc($result))
            {
                $this->lista[]=$row ;
            }
            return $this->lista;
        }
    }


    function get_link($id,$lg=""){
        if($this->con->conectar()==true){
            $result=mysql_query("SELECT * FROM link_footer".$lg." WHERE id=$id");
            $row=mysql_fetch_assoc($result);
            return $row;
        }
    }

    function nombre_archivo($id, $tabla){
        $sql="SELECT imagen FROM $tabla WHERE id=$id";
        if($this->con->conectar()==true){
            $result=mysql_query($sql);
            $row=mysql_fetch_assoc($result);
            return $row["imagen"];
        }

    }

    function grabar($sql){
        if($this->con->conectar()==true){
            mysql_query($sql);
        }
    }



    /***********************************/
    /*Usuarios                         */
    /***********************************/
    function get_usuarios(){
        if($this->con->conectar()==true){
            $result=mysql_query("SELECT * FROM usuarios");
            while( $row=mysql_fetch_assoc($result))
            {
                $this->users[]=$row;
            }
            return $this->users;
        }
    }

    /***********************************/
    /*Usuario por id                   */
    /***********************************/
    function get_usuario_id($id){
        if($this->con->conectar()==true){
            $result=mysql_query("SELECT * FROM usuarios WHERE id=$id");
            while( $row=mysql_fetch_assoc($result))
            {
                $this->user_id[]=$row;
            }
            //echo "SELECT * FROM usuarios WHERE mail='$mail'";
            return $this->user_id;
        }
    }

    /***********************************/
    /*Usuario por mail                 */
    /***********************************/
    function get_usuario($mail){
        $this->user_mail=null;
        if($this->con->conectar()==true){
            $result=mysql_query("SELECT * FROM usuarios WHERE mail='$mail'");
            while( $row=mysql_fetch_assoc($result))
            {
                $this->user_mail[]=$row;
            }
            //echo "SELECT * FROM usuarios WHERE mail='$mail'";
            return $this->user_mail;
        }
    }


    function find_user($usr_mail,$usr_pass){
        if($this->con->conectar()==true){
            $result=mysql_query("SELECT * FROM usuarios WHERE mail='$usr_mail' AND pass='$usr_pass' AND status='1'");
            while( $row=mysql_fetch_assoc($result))
            {
                $this->user_info[]=$row;
            }
            return $this->user_info;
        }
    }






    /***********************************/
    /*Contactos                        */
    /***********************************/
    function get_contactos(){
        if($this->con->conectar()==true){
            $result=mysql_query("SELECT * FROM contact_us");
            while( $row=mysql_fetch_assoc($result))
            {
                $this->contacts[]=$row;
            }
            return $this->contacts;
        }
    }

    /***********************************/
    /*Contacto                        */
    /***********************************/
    function get_contact($id){
        if($this->con->conectar()==true){
            $result=mysql_query("SELECT * FROM contact_us WHERE id=$id");
            while( $row=mysql_fetch_assoc($result))
            {
                $this->contact[]=$row;
            }
            return $this->contact;
        }
    }


    /***********************************/
    /*Quick Contacts                   */
    /***********************************/
    function get_quick_contacts(){
        if($this->con->conectar()==true){
            $result=mysql_query("SELECT * FROM quick_contact");
            while( $row=mysql_fetch_assoc($result))
            {
                $this->q_contacts[]=$row;
            }
            return $this->q_contacts;
        }
    }

    /***********************************/
    /*Quick Contact by id                    */
    /***********************************/
    function get_quick_contact($id){
        if($this->con->conectar()==true){
            $result=mysql_query("SELECT * FROM quick_contact WHERE id=$id");
            while( $row=mysql_fetch_assoc($result))
            {
                $this->q_contact[]=$row;
            }
            return $this->q_contact;
        }
    }


    /***********************************/
    /* News Letters                    */
    /***********************************/
    function get_newsletters(){
        if($this->con->conectar()==true){
            $result=mysql_query("SELECT * FROM newsletter");
            while( $row=mysql_fetch_assoc($result))
            {
                $this->newsletter[]=$row;
            }
            return $this->newsletter;
        }
    }



    function find_admin($usr,$pass){

        if($this->con->conectar()==true){
            $result=mysql_query("SELECT * FROM admin WHERE cuenta='$usr' AND password='$pass'");
           // echo "SELECT * FROM admin WHERE cuenta='$usr' AND password='$pass'[".mysql_num_rows($result)."]";
            while( $row=mysql_fetch_assoc($result))
            {
                $user_info[]=$row;
            }

            return $user_info;
        }
    }

    function get_admins(){
        $this->user_admins=null;
        if($this->con->conectar()==true){
            $result=mysql_query("SELECT * FROM admin");
            while( $row=mysql_fetch_assoc($result))
            {
                $this->user_admin[]=$row;
            }
            //echo "SELECT * FROM usuarios WHERE mail='$mail'";
            return $this->user_admin;
        }
    }

    function get_admin($id){
        $this->user_admins=null;
        if($this->con->conectar()==true){
            $result=mysql_query("SELECT * FROM admin WHERE id=$id");
            while( $row=mysql_fetch_assoc($result))
            {
                $this->user_admin[]=$row;
            }
            //echo "SELECT * FROM usuarios WHERE mail='$mail'";
            return $this->user_admin;
        }
    }
    function get_admin_cuenta($user){
        $this->user_admins=null;
        if($this->con->conectar()==true){
            $result=mysql_query("SELECT * FROM admin WHERE cuenta='$user'");
            while( $row=mysql_fetch_assoc($result))
            {
                $this->cuenta_admin[]=$row;
            }
            //echo "SELECT * FROM usuarios WHERE mail='$mail'";
            return $this->cuenta_admin;
        }
    }

    function get_air_tracking($awb_number){
        $this->get_air_track=null;
        if($this->con->conectar()==true){
            $sql="SELECT * FROM air_tracking WHERE awb_number='$awb_number' LIMIT 1";
            $result=mysql_query($sql);
            //while(
            $row=mysql_fetch_assoc($result);//)
            //{
                $this->get_air_track=$row;
            //}
            //echo $sql;
            return $this->get_air_track;
        }
    }

    function get_air_tracking_detail($awb_number){
        $this->get_air_track_detail=null;
        if($this->con->conectar()==true){
            $sql="SELECT * FROM air_tracking_detail WHERE awb_number='$awb_number'";
            $result=mysql_query($sql);
            while( $row=mysql_fetch_assoc($result))
            {
                $this->get_air_track_detail[]=$row;
            }
            //echo "SELECT * FROM usuarios WHERE mail='$mail'";
            return $this->get_air_track_detail;
        }
    }

    function get_sea_tracking($bl_number){
        $this->get_sea_track=null;
        if($this->con->conectar()==true){
            $sql="SELECT * FROM sea_tracking WHERE bl_number='$bl_number' LIMIT 1";
            $result=mysql_query($sql);

            $row=mysql_fetch_assoc($result);//)

            $this->get_sea_track=$row;

            //echo $sql;
            return $this->get_sea_track;
        }
    }

    function get_sea_tracking_detail($bl_number){
        $this->get_sea_track_detail=null;
        if($this->con->conectar()==true){
            $sql="SELECT * FROM sea_tracking_detail WHERE bl_number='$bl_number' order by date desc;";
            $result=mysql_query($sql);
            while( $row=mysql_fetch_assoc($result))
            {
                $this->get_sea_track_detail[]=$row;
            }
            //echo "SELECT * FROM usuarios WHERE mail='$mail'";
            return $this->get_sea_track_detail;
        }
    }

    function get_type_tracking($track_number){
        if($this->con->conectar()==true){
            $sql="SELECT * FROM air_tracking WHERE awb_number='$track_number' LIMIT 1";
            $result_air=mysql_query($sql);
            $sql="SELECT * FROM sea_tracking WHERE  bl_number='$track_number' LIMIT 1";
            $result_sea=mysql_query($sql);
            if(mysql_num_rows($result_air)==1){
                return "air";
            }else if(mysql_num_rows($result_sea)==1){
                return "sea";
            }else{
                return "none";
            }
        }
    }

    function get_tracking($id_user="",$f="",$t=""){
        $this->get_track=null;
        if($f!="" && $t!=""){
            $f=substr($f,6,4)."-".substr($f,0,2)."-".substr($f,3,2);
            $t=substr($t,6,4)."-".substr($t,0,2)."-".substr($t,3,2);
            $d1="AND pick_up_date > '$f' AND pick_up_date < '$t'";
            $d2="AND eta > '$f' AND eta < '$t'";
        }else{
            $d1="";
            $d2="";
        }
        if($id_user!=""){
            $user= "AND id_user =".$id_user;
        }else{
            $user="";
        }

        if($this->con->conectar()==true){
            $sql="
            SELECT
                air_tracking.id_user,
                air_tracking.awb_number as tracking,
                usuarios.company_name,
                air_tracking.origen,
                air_tracking.destino,
                air_tracking.pick_up_date as pick_up,
                'air' as type
            FROM air_tracking, usuarios
            where usuarios.id=air_tracking.id_user $user $d1
            UNION ALL
            SELECT
                sea_tracking.id_user,
                sea_tracking.bl_number as tracking,
                usuarios.company_name,
                sea_tracking.origen,
                sea_tracking.destino,
                sea_tracking.eta as pick_up,
                'sea' as type
            FROM sea_tracking, usuarios
            where usuarios.id=sea_tracking.id_user $user $d2
            order by pick_up desc,company_name desc ";


            $result=mysql_query($sql);
            while( $row=mysql_fetch_assoc($result))
            {
                $this->get_track[]=$row;
            }
            //echo $sql;
            return $this->get_track;
        }
    }


    function validar_awb($awb){
        if($this->con->conectar()==true){
            $sql="SELECT awb_number FROM air_tracking WHERE awb_number='$awb' LIMIT 1";
            $result=mysql_query($sql);
            $n=mysql_num_rows($result);
            if($n==1){
                return false;
            }else{
                return true;
            }
        }
    }



    function validar_bl($bl){
        if($this->con->conectar()==true){
            $sql="SELECT bl_number FROM sea_tracking WHERE bl_number='$bl' LIMIT 1";
            $result=mysql_query($sql);
            $n=mysql_num_rows($result);
            if($n==1){
                return false;
            }else{
                return true;
            }
        }
    }

    /***********************************/
    /*Get user mail             */
    /**********************************/
    function get_user_mail_by_sea($bl){
        if($this->con->conectar()==true){
            $result=mysql_query("SELECT usuarios.mail FROM sea_tracking ,usuarios
                                 where usuarios.id=sea_tracking.id_user
                                 and sea_tracking.bl_number='$bl'");
            while( $row=mysql_fetch_assoc($result))
            {
                $this->sea_mail[]=$row;
            }
            return $this->sea_mail;
        }
    }

    function get_user_mail_by_air($awb){
        if($this->con->conectar()==true){
            $result=mysql_query("SELECT usuarios.mail FROM air_tracking ,usuarios
                                 where usuarios.id=air_tracking.id_user
                                 and air_tracking.awb_number='$awb'");
            while( $row=mysql_fetch_assoc($result))
            {
                $this->air_mail[]=$row;
            }
            return $this->air_mail;
        }
    }


    function get_mail_and_code($mail, $code){
        $this->mail_code=null;
        if($this->con->conectar()==true){
            $sql="select * from usuarios where mail='$mail' AND codigo='$code'";
            $result=mysql_query($sql);
            while( $row=mysql_fetch_assoc($result))
            {
                $this->mail_code[]=$row;
            }
            //echo ">>>>>>-".$sql;
            return $this->mail_code;
        }
    }

    /***********************************/
    /*Get Lista paises             */
    /**********************************/
    function get_country_list(){
        if($this->con->conectar()==true){
            $result=mysql_query("SELECT DISTINCT pais  FROM ports");
            //echo "SELECT DISTINCT pais  FROM airports";
            while( $row=mysql_fetch_assoc($result))
            {
                $this->country[]=$row;
            }
            return $this->country;
        }
    }

    /***********************************/
    /*Get Lista Puertos por Pais     */
    /**********************************/
    function get_port_list($pais="ECUADOR", $tipo_trans){
        if($this->con->conectar()==true){
            $prt=($tipo_trans!=null)?" AND tipo = '".$tipo_trans."'":"";
            $sql="SELECT * FROM ports WHERE pais='$pais'".$prt." Order BY ciudad ";
            $this->puertos=null;
            $result=mysql_query($sql);
            while( $row=mysql_fetch_assoc($result))
            {
                //var_dump($row);
                $this->puertos[]=$row;
            }
            return $this->puertos;
        }
    }

    /***********************************/
    /*Get Lista Ciudades por Pais     */
    /**********************************/
    function get_port_city_list($pais="ECUADOR"){
        if($this->con->conectar()==true){

            $sql="SELECT distinct ciudad FROM ports where pais='$pais' order by ciudad;";
            //echo $sql;
            $result=mysql_query($sql);
            while( $row=mysql_fetch_assoc($result))
            {
                $this->portsp[]=$row;
            }
            return $this->portsp;
        }
    }



    /***************************************/
    /*Get Lista Puertos por Pais y Ciudad  */
    /***************************************/
    function get_port_list2($pais, $ciudad){
        if($this->con->conectar()==true){
            $this->ports2=null;
            $sql="SELECT * FROM ports WHERE ciudad='$ciudad' AND pais='$pais' Order BY ciudad ";
            //echo $sql;
            $result=mysql_query($sql);
            while( $row=mysql_fetch_assoc($result))
            {
                $this->ports2[]=$row;
            }
            return $this->ports2;
        }
    }

    /***********************************/
    /*Get Ruta por Puertos     */
    /**********************************/
    function get_ruta_by_ports($p_o, $p_d){
        if($this->con->conectar()==true){
            $sql="SELECT id FROM rutas where puerto_o='$p_o' and puerto_d='$p_d'";
            $result=mysql_query($sql);
            $row=mysql_fetch_row($result);
            return $row[0];
        }
    }


    function get_port($id){
        if($this->con->conectar()==true){
            $sql="SELECT * FROM ports WHERE id='$id'";
           // echo $sql;
            $result=mysql_query($sql);
            while( $row=mysql_fetch_assoc($result))
            {
                $this->port[]=$row;
            }
            return $this->port;
        }
    }

    /***********************************/
    /*Get Lista Proveedores             */
    /**********************************/
    function get_providers_list(){
        if($this->con->conectar()==true){
            $sql="SELECT * FROM providers";
            //echo $sql;
            $result=mysql_query($sql);
            while( $row=mysql_fetch_assoc($result))
            {
                $this->providers[]=$row;
            }
            return $this->providers;
        }
    }

    function get_provider($id){
        if($this->con->conectar()==true){
            $sql="SELECT * FROM providers WHERE id='$id'";
            // echo $sql;
            $result=mysql_query($sql);
            while( $row=mysql_fetch_assoc($result))
            {
                $this->provider[]=$row;
            }
            return $this->provider;
        }
    }

    function get_lista_paises(){
        if($this->con->conectar()==true){
            $sql="SELECT * FROM paises";
            //echo $sql;
            $result=mysql_query($sql);
            while( $row=mysql_fetch_assoc($result))
            {
                $this->paises[]=$row;
            }
            return $this->paises;
        }
    }

    function get_sevices_list(){
        if($this->con->conectar()==true){
            $sql="SELECT * FROM services";
            //echo $sql;
            $result=mysql_query($sql);
            while( $row=mysql_fetch_assoc($result))
            {
                $this->services[]=$row;
            }
            return $this->services;
        }
    }

    function get_service($id){
        if($this->con->conectar()==true){
            $sql="SELECT * FROM services WHERE id='$id'";
            echo $sql;
            $result=mysql_query($sql);
            while( $row=mysql_fetch_assoc($result))
            {
                $this->service[]=$row;
            }
            return $this->service;
        }
    }

    /***************************************/
    /*Get Lista rutas  */
    /***************************************/
    function get_rutas(){
        if($this->con->conectar()==true){

            $sql="SELECT * FROM rutas WHERE estatus=1";
            //echo $sql;
            $result=mysql_query($sql);
            while( $row=mysql_fetch_assoc($result))
            {
                $this->rutas[]=$row;
            }
            return $this->rutas;
        }
    }
    function get_ruta($id){
        if($this->con->conectar()==true){

            $sql="SELECT * FROM rutas WHERE estatus=1 AND id='$id'";
            //echo $sql;
            $result=mysql_query($sql);
            while( $row=mysql_fetch_assoc($result))
            {
                $this->ruta[]=$row;
            }
            return $this->ruta;
        }
    }


    /***************************************/
    /*Get Lista tarifas por contenedor */
    /***************************************/
    function get_tarifas_generales($id_ruta){
        if($this->con->conectar()==true){
            $sql="SELECT
            t.id,
            t.id_ruta,
            t.id_proveedor,
            p.compania,
            t.validity,
            t.transit,
            t.fecha_base,
            t.frecueny,
            t.currecny,
            t.import_services,
            t.bl_fee,
            t.handling,
            t.thcd,
            t.d_o,
            t.gate_in,
            t.export_services,
            t.gate_out,
            t.insurance,
            t.other_charges,
            t.limit_days,
            t.doc_limit_day,
            t.precio_20,
            t.bsc_20,
            t.isp_20,
            t.tho_20,
            t.other_20,
            t.precio_20+t.bsc_20+t.isp_20+t.tho_20+t.other_20 as total_20,
            t.precio_40,
            t.bsc_40,
            t.isp_40,
            t.tho_40,
            t.other_40,
            t.precio_40+t.bsc_40+t.isp_40+t.tho_40+t.other_40 as total_40,
            t.precio_hr,
            t.bsc_hr,
            t.isp_hr,
            t.tho_hr,
            t.other_hr,
            t.precio_hr+t.bsc_hr+t.isp_hr+t.tho_hr+t.other_hr as total_hr,
            t.precio_nor,
            t.bsc_nor,
            t.isp_nor,
            t.tho_nor,
            t.other_nor,t.precio_nor+t.bsc_nor+t.isp_nor+t.tho_nor+t.other_nor as total_nor
            FROM tarifas_generales as t
            inner join providers as p on t.id_proveedor=p.id
            where t.id_ruta='$id_ruta'";
            echo $sql;
            $result=mysql_query($sql);
            while( $row=mysql_fetch_assoc($result))
            {
                $this->tarifas[]=$row;
            }
            return $this->tarifas;
        }
    }

    /*
        function get_tarifas_por_ruta_contenedor($id_ruta){
        if($this->con->conectar()==true){
            $sql="SELECT p.compania, t.id,
            t.id_ruta,
            t.id_proveedor,
            t.precio_20,
            t.bsc_20,
            t.isp_20,
            t.tho_20,
            t.other_20,
            t.precio_20+t.bsc_20+t.isp_20+t.tho_20+t.other_20 as total_20,
            t.precio_40,
            t.bsc_40,
            t.isp_40,
            t.tho_40,
            t.other_40,
            t.precio_40+t.bsc_40+t.isp_40+t.tho_40+t.other_40 as total_40,
            t.precio_hr,
            t.bsc_hr,
            t.isp_hr,
            t.tho_hr,
            t.other_hr,
            t.precio_hr+t.bsc_hr+t.isp_hr+t.tho_hr+t.other_hr as total_hr,
            t.precio_nor,
            t.bsc_nor,
            t.isp_nor,
            t.tho_nor,
            t.other_nor,
            t.precio_nor+t.bsc_nor+t.isp_nor+t.tho_nor+t.other_nor as total_nor,
            t.estatus,
            t.currecny
            FROM tarifas as t
            inner join providers as p on t.id_proveedor=p.id
            where t.id_ruta='$id_ruta'";
            //echo $sql;
            $result=mysql_query($sql);
            while( $row=mysql_fetch_assoc($result))
            {
                $this->tarifas[]=$row;
            }
            return $this->tarifas;
        }
    }
     */


    /***************************************/
    /*Get Lista tarifas por id de ruta */
    /***************************************/
    function get_tarifas_por_ruta($id_ruta){
        if($this->con->conectar()==true){
            $sql="SELECT p.compania, t.id,
            t.id_ruta,
            t.id_proveedor,
            t.validity,
            t.transit,
            t.frecueny,
            t.currecny,
            t.import_services,
            t.export_services,
            t.admin_charges,
            t.d_o,
            t.seal,
            t.gate_in,
            t.gate_out,
            t.issuing,
            t.doc_fee,
            t.other_charges,
            t.estatus,
            t.currecny
            FROM tarifas_generales as t
            inner join providers as p on t.id_proveedor=p.id
            where t.id_ruta='$id_ruta'";
            //echo $sql;
            $result=mysql_query($sql);
            while( $row=mysql_fetch_assoc($result))
            {
                $this->tarifasr[]=$row;
            }
            return $this->tarifasr;
        }
    }

    /***************************************/
    /*Get Lista tarifas por id de ruta y id proveedor */
    /***************************************/
    function get_tarifas_por_ruta_prov($id_ruta, $id_pr){
        if($this->con->conectar()==true){
            $sql="SELECT p.compania,
			t.id,
            t.id_ruta,
			t.id_proveedor,
			t.validity,
			t.transit,
			t.fecha_base,
			t.frecueny,
			t.currecny,
			t.import_services,
			t.bl_fee,
			t.handling,
			t.thcd,
			t.d_o,
			t.gate_in,
			t.export_services,
			t.gate_out,
			t.insurance,
			t.other_charges,
			t.limit_days,
			t.doc_limit_day,
			t.precio_20+t.bsc_20+t.isp_20+t.tho_20+t.other_20 as total_20,
            t.precio_40,
            t.bsc_40,
            t.isp_40,
            t.tho_40,
            t.other_40,
            t.precio_40+t.bsc_40+t.isp_40+t.tho_40+t.other_40 as total_40,
            t.precio_hr,
            t.bsc_hr,
            t.isp_hr,
            t.tho_hr,
            t.other_hr,
            t.precio_hr+t.bsc_hr+t.isp_hr+t.tho_hr+t.other_hr as total_hr,
            t.precio_nor,
            t.bsc_nor,
            t.isp_nor,
            t.tho_nor,
            t.other_nor,t.precio_nor+t.bsc_nor+t.isp_nor+t.tho_nor+t.other_nor as total_nor
            FROM tarifas_generales as t
            inner join providers as p on t.id_proveedor=p.id
            where t.id_ruta='$id_ruta'
            and id_proveedor='$id_pr'";
            echo ($sql);
            $result=mysql_query($sql);
            while( $row=mysql_fetch_assoc($result))
            {
                $this->tarifasrp[]=$row;
            }
            return $this->tarifasrp;
        }
    }


    /***************************************/
    /*Get Lista tarifas LCL */
    /***************************************/
    function get_tarifas_lcl($id_ruta){
        if($this->con->conectar()==true){
            $sql="SELECT p.compania,
            t.id,
            t.id_ruta,
            t.id_proveedor,
            t.flete,
            t.port_min,
            t.port,
            t.cargo_min,
            t.cargo,
            t.consolidation_min,
            t.consolidation,
            t.wharehouse_min,
            t.wharehouse,
            t.sed,
            t.bl,
            t.currecny
            FROM tarifas_lcl as t
            inner join providers as p on t.id_proveedor=p.id
            where t.id_ruta='$id_ruta'";
            //echo $sql;
            $result=mysql_query($sql);
            while( $row=mysql_fetch_assoc($result))
            {
                $this->tarifaslcl[]=$row;
            }
            return $this->tarifaslcl;
        }
    }


    /*function get_tarifas_lcl_old($id_ruta){
        if($this->con->conectar()==true){
            $sql="SELECT p.compania,
            t.id,
            t.id_ruta,
            t.id_proveedor,
            t.validity,
            t.flete_min,
            t.tarifa_flete,
            t.transbordo,
            t.trucking,
            t.exp_doc_fee,
            t.custom_clearance,
            t.consolidation,
            t.currecny
            FROM tarifas_lcl as t
            inner join providers as p on t.id_proveedor=p.id
            where t.id_ruta='$id_ruta'";
            //echo $sql;
            $result=mysql_query($sql);
            while( $row=mysql_fetch_assoc($result))
            {
                $this->tarifaslcl[]=$row;
            }
            return $this->tarifaslcl;
        }
    }*/



    /***************************************/
    /*Get Lista tarifas por puertos */
    /***************************************/
    function get_lista_tarifas_por_puertos($puerto_o,$puerto_d){

        if($this->con->conectar()==true){
            $sql="select
                  t.id,
                  t.id_ruta,
                  t.id_proveedor,
                  p.compania,
                  t.validity,
                  t.transit,
                  t.fecha_base,
                  t.frecueny,
                  t.currecny,
                  t.import_services,
                  t.bl_fee,
                  t.handling,
                  t.thcd,
                  t.d_o,
                  t.gate_in,
                  t.export_services,
                  t.gate_out,
                  t.insurance,
                  t.other_charges,
                  t.limit_days,
                  t.doc_limit_day,
                  t.precio_20,
                  t.bsc_20,
                  t.isp_20,
                  t.tho_20,
                  t.precio_20+t.bsc_20+t.isp_20+t.tho_20+t.other_20 as total_20,
                  t.precio_40,
                  t.bsc_40,
                  t.isp_40,
                  t.tho_40,
                  t.other_40,
                  t.precio_40+t.bsc_40+t.isp_40+t.tho_40+t.other_40 as total_40,
                  t.precio_hr,
                  t.bsc_hr,
                  t.isp_hr,
                  t.tho_hr,
                  t.other_hr,
                  t.precio_hr+t.bsc_hr+t.isp_hr+t.tho_hr+t.other_hr as total_hr,
                  t.precio_nor,
                  t.bsc_nor,
                  t.isp_nor,
                  t.tho_nor,
                  t.other_nor,
                  t.precio_nor+t.bsc_nor+t.isp_nor+t.tho_nor+t.other_nor as total_nor
                  from tarifas_generales t
                  inner join rutas r on
                  t.id_ruta=r.id
                  inner join providers p on
                  t.id_proveedor=p.id
            where r.puerto_o='$puerto_o' and r.puerto_d='$puerto_d'
            group by t.id_proveedor";
            $result=mysql_query($sql);
            //echo $sql;
            while( $row=mysql_fetch_assoc($result))
            {
                $this->tarifas_g[]=$row;
            }
            return $this->tarifas_g;
        }
    }


    /*function get_salidas_ruta($id){
        if($this->con->conectar()==true){

            $sql="SELECT * FROM salidas WHERE id_ruta='$id''";
            //echo $sql;
            $result=mysql_query($sql);
            while( $row=mysql_fetch_assoc($result))
            {
                $this->salidas_r[]=$row;
            }
            return $this->ruta;
        }
    }*/

    /***************************************/
    /*Get Lista salidas ruta */
    /***************************************/
    function get_salidas_ruta($id_ruta){
        if($this->con->conectar()==true){
            $sql="SELECT p.compania,
            s.id,
            s.id_proveedor,
            s.id_ruta,
            s.fecha_base,
            s.periodo,
            s.dias_limite,
            s.dias_limite_doc
            FROM salidas as s
            inner join providers as p on s.id_proveedor=p.id
            where s.id_ruta='$id_ruta'";
            //echo $sql;
            $result=mysql_query($sql);
            while( $row=mysql_fetch_assoc($result))
            {
                $this->salidas_r[]=$row;
            }
            return $this->salidas_r;
        }
    }

    /***************************************/
    /*Get Lista salidas por proveedor de ruta ruta */
    /***************************************/
    function get_salidas_proveedor_ruta($id_ruta, $id_pr){
        if($this->con->conectar()==true){
            $sql="SELECT fecha_base FROM salidas
            where id_ruta='$id_ruta' and id_proveedor='$id_pr'";
            //echo $sql;
            $result=mysql_query($sql);
            while( $row=mysql_fetch_assoc($result))
            {
                $this->salidas[]=$row;
            }
            return $this->salidas;
        }
    }







   /* function get_salidas_ruta($id_ruta){
        if($this->con->conectar()==true){
            $sql="SELECT p.compania,
            t.id,
            t.id_ruta,
            t.id_proveedor,
            t.flete,
            t.port_min,
            t.port,
            t.cargo_min,
            t.cargo,
            t.consolidation_min,
            t.consolidation,
            t.wharehouse_min,
            t.wharehouse,
            t.sed,
            t.bl,
            t.currecny
            FROM tarifas_lcl as t
            inner join providers as p on t.id_proveedor=p.id
            where t.id_ruta='$id_ruta'";
            //echo $sql;
            $result=mysql_query($sql);
            while( $row=mysql_fetch_assoc($result))
            {
                $this->tarifaslcl[]=$row;
            }
            return $this->tarifaslcl;
        }
    }*/







    /***************************************/
    /*Get Lista salidas de provincias      */
    /***************************************/


    function get_provincias(){
        if($this->con->conectar()==true){
            $sql="SELECT distinct provincia FROM tarifas_ciudades";
            //echo $sql;
            $result=mysql_query($sql);
            while( $row=mysql_fetch_assoc($result))
            {
                $this->prov[]=$row["provincia"];
            }
            return $this->prov;
        }
    }

    /***************************************/
    /* Get Lista salidas de ciudades
    /***************************************/

    function get_ciudades(){
        if($this->con->conectar()==true){
            $sql="SELECT * from tarifas_ciudades order by ciudad ASC";
            // echo $sql;
            $result=mysql_query($sql);
            while( $row=mysql_fetch_assoc($result))
            {
                $this->ciudad[]=$row;
            }
            return $this->ciudad;
        }
    }

    /***************************************/
    /* Get Lista salidas de ciudades segun
    /* la Provincia
    /***************************************/

    function get_ciudades_prv($provincia){
        if($this->con->conectar()==true){
            $sql="SELECT tarifas_ciudades from ciudades where provincia='$provincia' order by ciudad ASC;";
            // echo $sql;
            $result=mysql_query($sql);
            while( $row=mysql_fetch_assoc($result))
            {
                $this->ciudad[]=$row["ciudad"];
            }
            return $this->ciudad;
        }
    }
    /***************************************/
    /*Get Lista de Ciudades y provincias      */
    /***************************************/
    function get_ciudades_list(){
        if($this->con->conectar()==true){
            $sql="SELECT * FROM tarifas_ciudades";
            //echo $sql;
            $result=mysql_query($sql);
            while($row=mysql_fetch_assoc($result))
            {
                $this->city[]=$row;
            }
            return $this->city;
        }
    }

    /***************************************/
    /* Get Tarifa por Ciudad
    /***************************************/

    function get_tarifa_por_ciudad($id){
        if($this->con->conectar()==true){
            $sql="SELECT * from tarifas_ciudades where id='$id'";
            // echo $sql;
            $result=mysql_query($sql);
            while( $row=mysql_fetch_assoc($result))
            {
                $this->ciudad[]=$row;
            }
            return $this->ciudad;
        }
    }

    /***************************************/
    /* Get costo por ciudad
    /***************************************/

    function get_precio_por_ciudad($id){
        if($this->con->conectar()==true){
            $sql="SELECT precio from tarifas_ciudades where id=$id";
            // echo $sql;
            $result=mysql_query($sql);
            while( $row=mysql_fetch_assoc($result))
            {
                $this->ciudad[]=$row["ciudad"];
            }
            return $this->ciudad;
        }
    }

}

?>