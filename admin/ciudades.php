<?php include_once("Consultas.php");?>
<?php include_once("funciones.php");?>
<?php
if($_POST["modo"]=="new"){
    //agrega_cuidad();
}else if($_POST["modo"]=="update"){
    modifica_tarifa_por_ciudad();
}if($_POST["modo"]=="delete"){
    //elimina_ciudad();
}
$con=new Consultas();
$lst=$con->get_ciudades_list();

?>

<!DOCTYPE html>
<html>
<head>
    <?php include("head.php");?>
    <title></title>
    <script>
        function borrar(id)
        {
            document.fr.id.value=id;
            if(confirm("Esta seguro que desea eliminar ...?")){
                document.fr.submit();
            }
        }
    </script>
</head>
<body>
<div class="row">
    <div class="col-xs-6 col-xs-offset-3">
        <h3>Listado de Tarifas por Ciudad</h3>
    </div>
    <div class="col-xs-2" style="padding-top:15px">
        <!--button type="button" class="btn btn-primary" style="width: 100%" onclick="window.open('ciudad.php','_self','')">Agregar</button-->
    </div>
</div>

<div class="row">
    <div class="col-xs-2 col-xs-offset-1">
        <?php $op=14 ?>
        <?php include_once("menu.php")?>
    </div>
    <div class="col-xs-8">
        <table class="table table-hover">
            <thead>
            <tr style="background: #9acfea">
                <td style="width: 24%">Ciudad</td>
                <td style="text-align: left">Provincia</td>
                <td style="width: 8%; text-align: center">Cont 20 -15 TON</td>
                <td style="width: 8%; text-align: center">Cont 20 +15 TON</td>
                <td style="width: 8%; text-align: center">Cont 20 +25 TON</td>
                <td style="width: 8%; text-align: center">Cont 40 -15 TON</td>
                <td style="width: 8%; text-align: center">Cont 40 +15 TON</td>
                <td style="width: 8%; text-align: center">Cont 40 +25 TON</td>
                <td style="width: 5%; text-align: center">M</td>
                <!--td style="width: 5%; text-align: center">E</td-->
            </tr>
            </thead>
            <tbody>
            <?php
            foreach($lst as $item){ ?>
            <tr>
                <td><?php echo $item['ciudad']?></td>
                <td><?php echo $item['provincia']?></td>
                <td style="text-align: right"><?php echo $item['cont20']?></td>
                <td style="text-align: right"><?php echo $item['cont20_15']?></td>
                <td style="text-align: right"><?php echo $item['cont20_25']?></td>
                <td style="text-align: right"><?php echo $item['cont40']?></td>
                <td style="text-align: right"><?php echo $item['cont40_15']?></td>
                <td style="text-align: right"><?php echo $item['cont40_25']?></td>
                <td style="; text-align: center"><a href="ciudad.php?id=<?php echo $item["id"]?>"><img src="img/edit_icon.png"></a></td>
            </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<form name="fr" method="post" action="">
    <input type="hidden" name="id">
    <input type="hidden" name="modo" value="delete">
</form>
</body>
</html>





