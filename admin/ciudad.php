<?php
/**
 * Created by PhpStorm.
 * User: JuanJosé
 * Date: 07/08/14
 * Time: 09:17 AM
 */
include_once("Consultas.php");
$con=new Consultas();
//$paises=$con->get_country_list();

$id=0;
$port=null;
if($_GET["id"]!=null){
    $id=$_GET["id"];
    $serv=$con->get_tarifa_por_ciudad($id);
    $serv=$serv[0];
    $ciudad=$serv["ciudad"];
    $provincia=$serv["provincia"];
    $cont20=$serv["cont20"];
    $cont20_15=$serv["cont20_15"];
    $cont20_25=$serv["cont20_15"];
    $cont40=$serv["cont40"];
    $cont40_15=$serv["cont40_15"];
    $cont40_25=$serv["cont40_25"];

    //$descripcion=$serv["descripcion"];
    //$tipo_servicio=$serv["tipo_servicio"];
    //$activo=($serv["estatus"]==1)?1:0;
    //$lst_paises=$serv["paises"];
    //echo ">>>".$port["estatus"];
}

?>
<!DOCTYPE html>
<html>
<head>
    <?php include("head.php");?>
    <title></title>
</head>
<body>
<div class="row">
    <div class="col-md-7 col-md-offset-3">
        <?php if($id==0){ ?>
            <h3>Insercion de Precio</h3>
        <?php }else{?>
            <h3>Modificacion de Precio</h3>
        <?php } ?>
    </div>
</div>

<div class="row">
    <div class="col-xs-2 col-xs-offset-1">
        <?php $op=12 ?>
        <?php include_once("menu.php")?>
    </div>
    <div class="col-xs-8">

        <form role="form" action="ciudades.php" method="post" enctype="multipart/form-data">
            <?php if($id>0){ ?>
            <div class="form-group">
                <label for="id">Id</label>
                <input type="text" class="form-control" id="id" name="id" value="<?php echo $id ?>" readonly>
            </div>
            <?php } ?>
            <div class="form-group">
                <label for="localidad">Ciudad</label>
                <input type="text" class="form-control" id="ciudad" name="ciudad" value="<?php echo ($id>0)? $ciudad:"" ?>" readonly>
            </div>
            <div class="form-group">
                <label for="nombre">Provincia</label>
                <input type="text" class="form-control" id="provincia" name="provincia"  value="<?php echo ($id>0)? $provincia:"" ?>" readonly>
            </div>
            <div class="form-group">
                <label for="cont20-15">Cont20 -15 TON</label>
                <input type="text" class="form-control" id="cont20" name="cont20" placeholder="precio" value="<?php echo ($id>0)? $cont20:"" ?>">
            </div>
            <div class="form-group">
                <label for="cont20+15">Cont20 +15 TON</label>
                <input type="text" class="form-control" id="cont20_15" name="cont20_15" placeholder="precio" value="<?php echo ($id>0)? $cont20_15:"" ?>">
            </div>
            <div class="form-group">
                <label for="cont20+25">Cont20 +25 TON</label>
                <input type="text" class="form-control" id="cont20_25" name="cont20_25" placeholder="precio" value="<?php echo ($id>0)? $cont20_25:"" ?>">
            </div>
            <div class="form-group">
                <label for="cont40-15">Cont40 -15 TON</label>
                <input type="text" class="form-control" id="cont40" name="cont40" placeholder="precio" value="<?php echo ($id>0)? $cont40:"" ?>">
            </div>
            <div class="form-group">
                <label for="cont40+15">Cont40 +15 TON</label>
                <input type="text" class="form-control" id="cont40_15" name="cont40_15" placeholder="precio" value="<?php echo ($id>0)? $cont40_15:"" ?>">
            </div>
            <div class="form-group">
                <label for="cont40+25">Cont40 +25 TON</label>
                <input type="text" class="form-control" id="cont40_25" name="cont40_25" placeholder="precio" value="<?php echo ($id>0)? $cont40_25:"" ?>">
            </div>


            <button type="submit" class="btn btn-default">Enviar</button>
            <input type="hidden" name="modo" value="<?php echo ($id==0)? "new":"update"?>">
        </form>
    </div>
</div>
<script type="text/javascript">
    /*var p='<?php echo $pais ?>';
    var pt='<?php echo $tipo ?>';
    $(document).ready(function(){
        $("#paises option[value='"+ p + "']").attr('selected', 'selected');
        $("#tipo_puerto option[value='"+ pt + "']").attr('selected', 'selected');
    });*/
</script>
</body>
</html>


