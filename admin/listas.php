
<?php
/**
 * Created by PhpStorm.
 * User: JuanJosé
 * Date: 07/08/14
 * Time: 09:17 AM
 */
include_once("Consultas.php");
//include_once("head.php");
$con=new Consultas();

if(isset($_GET['pais'])){
    $pais=$_GET['pais'];
    $tp=$_GET['tp'];
    //Lista de Ciudades segun el pais
    if($tp==1){
        $lst=$con->get_port_city_list($pais);
        //var_dump($lst);
        ?>
        <label for="ciudad_o">Ciudad</label>
        <select class="form-control" name="ciudad_o" id="ciudad_o">
            <option value="">- - - -</option>
            <?php
            foreach($lst as $p){ ?>
            <option value="<?php echo $p['ciudad']?>"><?php echo $p['ciudad']?></option>
            <?php } ?>
        </select>
    <?php }

    //Lista de Puertos segun el pais y la Ciudad
    if($tp==2){
        $ciudad=$_GET['ciudad'];
        $lst=$con->get_port_list2($pais,$ciudad);
        ?>
        <label for="texto">Puerto</label>
        <select class="form-control" name="puerto_o" id="puerto_o">
            <option value="">- - - -</option>
            <?php
            foreach($lst as $p){ ?>
                <option value="<?php echo $p['nombre']?>"><?php echo $p['nombre']?></option>
            <?php } ?>
        </select>
    <?php }

    if($tp==3){
        $lst=$con->get_port_city_list($pais)
        ?>
        <label for="texto">Ciudad</label>
        <select class="form-control" name="ciudad_d" id="ciudad_d">
            <option value="">- - - -</option>
            <?php
            foreach($lst as $p){ ?>
                <option value="<?php echo $p['ciudad']?>"><?php echo $p['ciudad']?></option>
            <?php } ?>
        </select>
    <?php }

    //Lista de Puertos segun el pais y la Ciudad
    if($tp==4){
        $ciudad=$_GET['ciudad'];
        $lst=$con->get_port_list2($pais,$ciudad);
        ?>
        <label for="texto">Puerto</label>
        <select class="form-control" name="puerto_d" id="puerto_d">
            <option value="">- - - -</option>
            <?php
            foreach($lst as $p){ ?>
                <option value="<?php echo $p['nombre']?>"><?php echo $p['nombre']?></option>
            <?php } ?>
        </select>
    <?php }
}
?>
<script>


    $("#ciudad_o").change(function(){
        var p=$("#pais_o option:selected").val();
        var c=$("#ciudad_o option:selected").val();
        $.ajax({
            type: "GET",
            url: "listas.php",
            data: { tp: "2", pais: p, ciudad: c}
        }).done(function( html ) {
            $( "#pt_origen").empty().append( html );
        });
    });

    $("#ciudad_d").change(function(){
        var p=$("#pais_d option:selected").val();
        var c=$("#ciudad_d option:selected").val();
        $.ajax({
            type: "GET",
            url: "listas.php",
            data: { tp: "4", pais: p, ciudad: c}
        }).done(function( html ) {
            $( "#pt_destino").empty().append( html );
        });
    });

</script>


