<?php
session_start();
/**
 * Created by PhpStorm.
 * User: JuanJosé
 * Date: 07/08/14
 * Time: 09:17 AM
 */
include_once("Consultas.php");

$con=new Consultas();
if($_GET['exit']!=null){
    unset($_SESSION['tipo_usuario']);
    unset($_SESSION['cuenta']);
    unset($_SESSION['nombre']);
    unset($_SESSION['mail']);
}


if($_POST){
    $user=$_POST['user'];
    $password=md5($_POST['password']);
    $admin_usr=$con->find_admin($user,$password);
    if(count($admin_usr)==0 || $admin_usr==null){
        $err=true;
    }else{
        $_SESSION['tipo']=$admin_usr[0]['tipo_usuario'];
        $_SESSION['cuenta']=$admin_usr[0]['cuenta'];
        $_SESSION['nombre']=$admin_usr[0]['nombre'];
        $_SESSION['mail']=$admin_usr[0]['mail'];

        //echo ">>>cuenta".$_SESSION['nombre']."->>".$admin_usr[0]['id'];
        echo "<script>window.open('banners_big.php','_self','')</script>";
        //header('Location: banners_big.php');
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <?php include("head.php");?>
    <title></title>
</head>
<body>
<div class="row">
    <div class="col-md-6 col-md-offset-3 text-center">

            <h3>Freightlogistics</h3>
            <hr>

    </div>
</div>
<div class="row">
    <div class="col-xs-6 col-xs-offset-3">
        <form class="form-horizontal" role="form" method="post">
            <div class="form-group">
                <label for="user" class="col-xs-3 control-label text-left">Email</label>
                <div class="col-xs-9">
                    <input type="text" class="form-control" id="user" name="user" placeholder="User" required="">
                </div>
            </div>
            <div class="form-group">
                <label for="password" class="col-xs-3 control-label text-left">Password</label>
                <div class="col-xs-9">
                    <input type="password" class="form-control" name="password" id="password"  placeholder="Password" required="">
                </div>
            </div>
            <!--div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox"> No cerrar sesión
                        </label>
                    </div>
                </div>
            </div-->
            <div class="form-group">
                <div class="col-xs-offset-3 col-xs-7 text-center">
                    <button type="submit" class="btn btn-default">Entrar</button>
                </div>
            </div>
        </form>
        <hr>
        <div id="alerta" class="alert alert-danger text-center" style="display:<?php echo (!$err)?"none":"block" ?>">The user don't exist!</div>
    </div>
</div>
</body>
<script>

</script>

</html>