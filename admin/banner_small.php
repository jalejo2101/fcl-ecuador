<?php
/**
 * Created by PhpStorm.
 * User: JuanJosé
 * Date: 07/08/14
 * Time: 09:17 AM
 */
include_once("Consultas.php");
$con=new Consultas();

$id=0;
$banner=null;
if($_GET["id"]!=null){
    $id=$_GET["id"];
    $banner=$con->get_Banner_Small($id);
    $imagen=$banner["imagen"];
    $url=$banner["url"];
}


?>
<!DOCTYPE html>
<html>
<head>
    <?php include("head.php");?>
    <title></title>
</head>

<body>
<div class="row">
    <div class="col-md-7 col-md-offset-3">
        <?php if($id==0){ ?>
            <h3>Insercion de Banners</h3>
        <?php }else{?>
            <h3>Modificacion de Banners</h3>
        <?php } ?>
    </div>
</div>

<div class="row">
    <div class="col-xs-2 col-xs-offset-1">
        <?php $op=1 ?>
        <?php include_once("menu.php")?>
    </div>
    <div class="col-xs-8">
        <form role="form" action="banners_small.php" method="post" enctype="multipart/form-data">
            <?php if($id>0){ ?>
            <div class="form-group">
                <label for="id">Id</label>
                <input type="text" class="form-control" id="id" name="id" value="<?php echo $id ?>" readonly>
            </div>
            <?php } ?>

            <div class="form-group">
                <label for="file">Adjuntar imagen</label>
                <input type="file" id="file" name="file">
                <?php echo ($id>0)? "<help>".$banner["imagen"]."</help>":""?>
            </div>
            <div class="form-group">
                <label for="url">URL</label>
                <input type="text" class="form-control" id="url" name="url" placeholder="http://..." value="<?php echo ($id>0)? $url:"" ?>" >
            </div>
            <div class="checkbox">
                <label>
                    <input name="activo" type="checkbox" <?php echo ($id>0 && $banner["activo"]==1)? "checked":""?> > Activo
                </label>
            </div>
            <button type="submit" class="btn btn-default">Enviar</button>
            <input type="hidden" name="modo" value="<?php echo ($id==0)? "new":"update"?>">
        </form>
    </div>
</div>

</body>
</html>