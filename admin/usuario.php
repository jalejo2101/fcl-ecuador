<?php
/**
 * Created by PhpStorm.
 * User: JuanJosé
 * Date: 07/08/14
 * Time: 09:17 AM
 */
include_once("Consultas.php");
$con=new Consultas();

$id=0;
$item=null;

if($_GET["id"]!=null){
    $id=$_GET["id"];
    $con=new Consultas();
    $usr=$con->get_usuario_id($id);
    $usr=$usr[0];
    $company_name=$usr['company_name'];
    $contact_name=$usr['contact_name'];
    $ruc=$usr['ruc'];
    $address=$usr['address'];
    $country=$usr['country'];
    $city=$usr['city'];
    $state=$usr['state'];
    $zipcode=$usr['zipcode'];
    $local_number=$usr['local_number'];
    $mobile_number=$usr['mobile_number'];
    $mail=$usr['mail'];
    $transpref01=$usr['transpref01'];
    $transpref02=$usr['transpref02'];
    $transpref03=$usr['transpref03'];

    $ts_fb=$usr['ts_fb'];
    $ts_fp=$usr['ts_fp'];
    $ts_20=$usr['ts_20'];
    $ts_40=$usr['ts_40'];
    $ts_hq=$usr['ts_hq'];

    $pr_01=$usr['pr_01'];
    $pr_02=$usr['pr_02'];
    $pr_03=$usr['pr_03'];
    $pr_04=$usr['pr_04'];
    $frqship=$usr['frqship'];



}


?>
<!DOCTYPE html>
<html>
<head>
    <?php include("head.php");?>
    <title></title>
</head>
<body>
<div class="row">
    <div class="col-xs-4 col-xs-offset-3">
            <h3>Informacion de Usuario</h3>
    </div>
    <div class="col-xs-2" style="padding-top:15px">
        <button type="button" class="btn btn-primary" style="width: 100%" onclick="window.open('air_shipment.php?type=new&id_client=<?php echo $_GET["id"]?>','_self','')">Add Air Shipment</button>
    </div>
    <div class="col-xs-2" style="padding-top:15px">
        <button type="button" class="btn btn-primary" style="width: 100%" onclick="window.open('sea_shipment.php?type=new&id_client=<?php echo $_GET["id"]?>','_self','')">Add Sea Shipment</button>
    </div>



</div>

<div class="row">
    <div class="col-xs-2 col-xs-offset-1">
        <?php $op=7 ?>
        <?php include_once("menu.php")?>
    </div>
    <div class="col-xs-8">

        <form role="form" action="contacts.php" method="post" enctype="multipart/form-data">
            <section class="tracking-box" style="border: none">
                <h1><?php echo $contact_name ?> Profile</h1>
                <div id="register-panel">
                    <h2>Company Information</h2>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="company_name">Company Name</label>
                                <input readonly type="text" class="form-control" id="company_name" name="company_name" placeholder="company_name" value="<?php echo ($id>0)? $company_name:"" ?>" >
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="contact_name">Contact Name</label>
                                <input readonly type="text" class="form-control" id="contact_name" name="contact_name" placeholder="contact_name" value="<?php echo ($id>0)? $contact_name:"" ?>" >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="ruc">RUC</label>
                                <input readonly type="text" class="form-control" id="ruc" name="ruc" placeholder="ruc" value="<?php echo ($id>0)? $ruc:"" ?>" >
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="address">address</label>
                                <input readonly type="text" class="form-control" id="address" name="address" placeholder="address" value="<?php echo ($id>0)? $address:"" ?>" >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="country">Country</label>
                                <input readonly type="text" class="form-control" id="country" name="country" placeholder="country" value="<?php echo ($id>0)? $country:"" ?>" >
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="city">City</label>
                                <input readonly type="text" class="form-control" id="city" name="city" placeholder="city" value="<?php echo ($id>0)? $city:"" ?>" >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="state">State</label>
                                <input readonly type="text" class="form-control" id="state" name="state" placeholder="state" value="<?php echo ($id>0)? $state:"" ?>" >
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="zipcode">Zip Code</label>
                                <input readonly type="text" class="form-control" id="zipcode" name="zipcode" placeholder="zipcode" value="<?php echo ($id>0)? $zipcode:"" ?>" >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="local_number">Local Number</label>
                                <input readonly type="text" class="form-control" id="local_number" name="local_number" placeholder="local_number" value="<?php echo ($id>0)? $local_number:"" ?>" >
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="mobile_number">Mobile Number</label>
                                <input readonly type="text" class="form-control" id="mobile_number" name="mobile_number" placeholder="mobile_number" value="<?php echo ($id>0)? $mobile_number:"" ?>" >
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-xs-6">
                            <h2>Transport Preference</h2>
                            <table  class="radio-list">
                                <tr>
                                    <td> Full Container Load (FCL) </td>
                                    <td> <input type="radio" name="transpref01" value="1" <?php echo ($transpref01==1)? "checked":"" ?>><label>Yes</label> </td>
                                    <td> <input type="radio" name="transpref01" value="0" <?php echo ($transpref01==0)? "checked":"" ?>><label>No</label> </td>
                                </tr>
                                <tr>
                                    <td> Less Container Load (LCL)</td>
                                    <td> <input type="radio" name="transpref02" value="1" <?php echo ($transpref02==1)? "checked":"" ?>><label>Yes</label> </td>
                                    <td> <input type="radio" name="transpref02" value="0" <?php echo ($transpref02==0)? "checked":"" ?>><label>No</label> </td>
                                </tr>
                                <tr>
                                    <td> Air Cargo </td>
                                    <td> <input type="radio" name="transpref03" value="1" <?php echo ($transpref03==1)? "checked":"" ?>><label>Yes</label> </td>
                                    <td> <input type="radio" name="transpref03" value="0" <?php echo ($transpref03==0)? "checked":"" ?>><label>No</label> </td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-10">
                            <h2>Typical Sizes</h2>

                            <table  class="typicalsizes">
                                <tr>
                                    <?php $lst=["Few Boxes","Few Pallets","20","40","40HQ"]; ?>
                                    <td>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="ts_fb" id="ts_fb" value="1" <?php echo ($ts_fb==1)?"checked":"" ?>>Few Boxes</label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="ts_fp" id="ts_fp" value="1" <?php echo ($ts_fp==1)?"checked":"" ?>>Few Pallets</label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="ts_20" id="ts_20" value="1" <?php echo ($ts_20==1)?"checked":"" ?>>20</label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="ts_40" id="ts_40" value="1" <?php echo ($ts_40==1)?"checked":"" ?>>40</label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="ts_hq" id="ts_hq" value="1" <?php echo ($ts_hq==1)?"checked":"" ?>>40HQ</label>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-6">
                            <h2>Login Information</h2>
                            <div class="form-group">
                                <label for="mail">Email</label>
                                <input readonly type="text" class="form-control" id="mail" name="mail" placeholder="mail" value="<?php echo ($id>0)? $mail:"" ?>" >
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-9">
                            <table class="radio-list">
                                <tr>
                                    <td colspan="3"> I want to receive offers from FCL (highly recommended). </td>
                                    <td> <input type="radio" name="pr_01" value="1"  <?php echo ($pr_01==1)?"checked":"" ?>><label>Yes</label> </td>
                                    <td> <input type="radio" name="pr_01" value="0"  <?php echo ($pr_01==0)?"checked":"" ?>><label>No</label> </td>
                                </tr>
                                <tr>
                                    <td colspan="3"> Have you ever done import/export operations? </td>
                                    <td> <input type="radio" name="pr_02" value="1"  <?php echo ($pr_02==1)?"checked":"" ?>><label>Yes</label> </td>
                                    <td> <input type="radio" name="pr_02" value="0"  <?php echo ($pr_02==0)?"checked":"" ?>><label>No</label> </td>
                                </tr>
                                <tr>
                                    <td colspan="3"> Are you currently doing shipments? </td>
                                    <td> <input type="radio" name="pr_03" value="1" <?php echo ($pr_03==1)?"checked":"" ?>><label>Yes</label> </td>
                                    <td> <input type="radio" name="pr_03" value="0" <?php echo ($pr_03==0)?"checked":"" ?>><label>No</label> </td>
                                </tr>
                                <tr>
                                    <td colspan="3"> Do you have a single shipment or recurring shipments? </td>
                                    <td> <input type="radio" name="pr_04" value="1" <?php echo ($pr_04==1)?"checked":"" ?>><label>Yes</label> </td>
                                    <td> <input type="radio" name="pr_04" value="0" <?php echo ($pr_04==0)?"checked":"" ?>><label>No</label> </td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-10">
                            <h2>Current frequency of shipments</h2>
                            <table  class="radio-list">
                                <tr>
                                    <td> LESS THAN 5 SHIPMENTS </td>
                                    <td> <input type="radio" name="frqship" value="11" <?php echo ($frqship==11)?"checked":"" ?>><label>Day</label> </td>
                                    <td> <input type="radio" name="frqship" value="12" <?php echo ($frqship==12)?"checked":"" ?>><label>Week</label> </td>
                                    <td> <input type="radio" name="frqship" value="13" <?php echo ($frqship==13)?"checked":"" ?>><label>Month</label> </td>
                                    <td> <input type="radio" name="frqship" value="14" <?php echo ($frqship==14)?"checked":"" ?>><label>Year</label> </td>
                                </tr>
                                <tr>
                                    <td> 5 TO 20 SHIPMENTS </td>
                                    <td> <input type="radio" name="frqship" value="21" <?php echo ($frqship==21)?"checked":"" ?>><label>Day</label> </td>
                                    <td> <input type="radio" name="frqship" value="22" <?php echo ($frqship==22)?"checked":"" ?>><label>Week</label> </td>
                                    <td> <input type="radio" name="frqship" value="23" <?php echo ($frqship==23)?"checked":"" ?>><label>Month</label> </td>
                                    <td> <input type="radio" name="frqship" value="24" <?php echo ($frqship==24)?"checked":"" ?>><label>Year</label> </td>
                                </tr>
                                <tr>
                                    <td> 21 TO 50 SHIPMENTS </td>
                                    <td> <input type="radio" name="frqship" value="31" <?php echo ($frqship==31)?"checked":"" ?>><label>Day</label> </td>
                                    <td> <input type="radio" name="frqship" value="32" <?php echo ($frqship==32)?"checked":"" ?>><label>Week</label> </td>
                                    <td> <input type="radio" name="frqship" value="33" <?php echo ($frqship==33)?"checked":"" ?>><label>Month</label> </td>
                                    <td> <input type="radio" name="frqship" value="34" <?php echo ($frqship==34)?"checked":"" ?>><label>Year</label> </td>

                                </tr>
                                <tr>
                                    <td> MORE THAN 51 SHIPMENTS </td>
                                    <td> <input type="radio" name="frqship" value="41" <?php echo ($frqship==41)?"checked":"" ?>><label>Day</label> </td>
                                    <td> <input type="radio" name="frqship" value="42" <?php echo ($frqship==42)?"checked":"" ?>><label>Week</label> </td>
                                    <td> <input type="radio" name="frqship" value="43" <?php echo ($frqship==43)?"checked":"" ?>><label>Month</label> </td>
                                    <td> <input type="radio" name="frqship" value="44" <?php echo ($frqship==44)?"checked":"" ?>><label>Year</label> </td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-4 col-xs-offset-4">
                            <!--a onclick="document.profile.submit()" class="edit-save">Save</a-->
                            <button type="button" onclick="window.open('usuarios.php','_self','')" class="btn btn-default">Regresar</button>

                            <input type="hidden" name="modo" value="<?php echo ($id==0)? "new":"update"?>">
                        </div>
                    </div>
                </div>
            </section>
        </form>
    </div>
</div>

</body>
</html>