<?php
/**
 * Created by PhpStorm.
 * User: jalejo
 * Date: 15/05/16
 * Time: 11:04 PM
 */

$_20_std=$_POST['20_std'];
$_40_std=$_POST['40_std'];
$_40_hc=$_POST['40_hc'];
$_40_nor=$_POST['40_nor'];
$freight_pay=$_POST['freight_pay'];
$moneda_seg=$_POST['moneda_seg'];
$incomterm=$_POST['incomterm'];
$seg_t=$_POST['seg_t'];
$dec_value=$_POST['dec_value'];
$entrega=$_POST['entrega'];
$city_r=$_POST['city_r'];
$address_r=$_POST['address_r'];
$zip_r=$_POST['zip_r'];
$person_r=$_POST['person_r'];
$telf_r=$_POST['telf_r'];
$tipo_cnt=$_POST['tipo_cnt'];
$peso_r=$_POST['peso_r'];
$fecha_r=$_POST['fecha_r'];
$hora_r=$_POST['hora_r'];
$city_d=$_POST['city_d'];
$address_d=$_POST['address_d'];
$zip_d=$_POST['zip_d'];
$person_d=$_POST['person_d'];
$telf_d=$_POST['telf_d'];
$tramites_aduana=$_POST['tramites_aduana'];
$exceso_20=$_POST['$exceso_20'];
$exceso_40=$_POST['$exceso_40'];
$bl_destino=$_POST['bl_destino'];
$importer=$_POST['importer'];
$innen=$_POST['innen'];
$mipro=$_POST['mipro'];
$origin=$_POST['origin'];
$certificate=$_POST['certificate'];
$n_cont=$_20_std+$_40_std+$_40_hc+$_40_nor;

$flete_40std=0;
$flete_40hc=0;
$flete_40nor=0;
$tfhead=($tipof=='im')?" - [IMP]":" - [EXP]";

$tarifas=$con->get_tarifas_por_ruta_prov($id_ruta,$id_pr)[0];
?>
<section class="panel">

    <header><?php lang("Quotation Summary","Resumen de la cotizacion");echo $tfhead ?></header>

    <table>

        <tr>
            <th style="text-align: left;" class="celda_desc">
                <?php lang("PORT OF LOADING","PUERTO DE ORIGEN")?>
            </th>
            <td style="text-align: left">
                <?php echo $_SESSION['puerto_o']?>
            </td>
        </tr>
        <tr>
            <th style="text-align: left;" class="celda_desc">
                <?php lang("PORT OF LOADING","PUERTO DE DESTINO")?>
            </th>
            <td style="text-align: left">
                <?php echo $_SESSION['puerto_d']?>
            </td>
        </tr>
        <tr>
            <th style="text-align: left;" class="celda_desc">
                <?php lang("CONTAINERS 20","CONTENDORES 20")?>
            </th>
            <td style="text-align: left">
                <?php echo $n_cont?>
            </td>
        </tr>
        <tr>
            <th style="text-align: left;" class="celda_desc">
                <?php lang("CONTAINERS 40","CONTENDORES 40")?>
            </th>
            <td style="text-align: left">
                <?php echo $n_cont?>
            </td>
        </tr>
        <tr>
            <th style="text-align: left;" class="celda_desc">
                <?php lang("INSURANCE","SEGURO")?>
            </th>
            <td style="text-align: left">
                <?php echo $seg_t?>
            </td>
        </tr>
        <tr>
            <th style="text-align: left;" class="celda_desc">
                <?php lang("SHIPPING DATE","FECHA DE EMBARQUE");?>
            </th>
            <td style="text-align: left">
                <select name="fecha_salida" style="width: 220px; float: left">
                    <?php
                    try{
                        $lst=explode(";",$tarifas['fecha_base']);
                        foreach($lst as $dia){
                            $timezone = new DateTimeZone('America/Lima');
                            $d=explode(":",$dia)[0];
                            $f=explode(":",$dia)[1];

                            $d=substr($d,6,4)."-".substr($d,3,2)."-".substr($d,0,2);

                            $fecha1 = new DateTime($d);
                            $fecha2 = new DateTime();
                            $fecha2->add(new DateInterval('P7D'));

                            do{
                                $fecha1 = $fecha1->add(new DateInterval('P'.$f.'D'));
                            }while($fecha1 < $fecha2);
                            ?>
                            <option value="<?php echo $fecha1->format('d-m-Y') ?>"><?php echo $fecha1->format('d-m-Y') ?></option>
                        <?php }
                    }catch(Exception $e){ } ?>
                </select>
            </td>
        </tr>
        <tr>
            <th style="text-align: left;" class="celda_desc">
                <?php lang("TRANSIT TIME","TIEMPO DE TRANSITO")?>
            </th>
            <td style="text-align: left">
                <?php echo $tarifas['transit']?>
            </td>
        </tr>
        <tr>
            <th style="text-align: left;" class="celda_desc">
                <?php lang("CURRENCY","DIVISA")?>
            </th>
            <td style="text-align: left">
                <?php echo $tarifas['currecny']?>
            </td>
        </tr>
        <tr>
            <th style="text-align: left;" class="celda_desc">
                <?php lang("FREIGHT  PAYMENT TERMS","CONDICION DE  PAGO DE FLETE")?>
            </th>
            <td style="text-align: left">
                <?php echo $freight_pay ?>
            </td>
        </tr>
        <tr>
            <th style="text-align: left;" class="celda_desc">
                <?php lang("INCOTERM","INCOTERM")?>
            </th>
            <td style="text-align: left">
                <?php echo $incoterm ?>
            </td>
        </tr>
        <tr>
            <th style="text-align: left;" class="celda_desc" colspan="2">
                <hr>
                <strong>RESUMEN DE VALORES</strong>
                <hr>
            </th>
        </tr>



        <?php if($_20_std>0){?>
        <tr>
            <th style="text-align: left;" class="celda_desc">
                <?php lang("Basic Ocean Freight 20","Flete Basico 20")?>
            </th>
            <td style="text-align: left">
                <?php
                $flete_20=$_20_std*$tarifas['precio_20'];
                echo "<div class='tc_lb'>[STD]</div><div class='tc_cal'>".$_20_std." X ".num_f($tarifas['precio_20'])."</div><div class= 'tc_tot'>= ".num_f($flete_20)."</div>" ?>
            </td>
        </tr>

        <?php } ?>
        <tr>
            <th style="text-align:left; vertical-align: middle; padding-top: 4px;" class="celda_desc">
                <?php lang("Basic Ocean Freight 40 (STD/HC/NOR)","Flete Basico 40 (STD/HC/NOR)")?>
            </th>
            <td style="text-align: left; vertical-align: middle">
                <table class="tc">
                    <?php if($_40_std>0){?>
                        <tr>
                            <td>
                                <?php
                                $flete_40std=$_40_std*$tarifas['precio_40'];
                                echo "<div class='tc_lb'>[STD]</div><div class='tc_cal'>".$_40_std." X ".num_f($tarifas['precio_40'])."</div><div class= 'tc_tot'>= ".num_f($flete_40std)."</div>" ?>
                            </td>
                        </tr>
                    <?php } ?>
                    <?php if($_40_hc>0){?>
                        <tr>
                            <td>
                                <?php
                                $flete_40hc=$_40_hc*$tarifas['precio_hr'];
                                echo "<div class='tc_lb'>[HC] </div><div class='tc_cal'>".$_40_hc." X ".num_f($tarifas['precio_hr'])."</div><div class= 'tc_tot'>= ".num_f($flete_40hc)."</div>" ?>
                            </td>
                        </tr>
                    <?php } ?>
                    <?php if($_40_nor>0){?>
                        <tr>
                            <td>
                                <?php
                                $flete_40nor=$_40_nor*$tarifas['precio_nor'];
                                echo "<div class='tc_lb'>[NOR]</div><div class='tc_cal'>".$_40_nor." X ".num_f($tarifas['precio_nor'])."</div><div class= 'tc_tot'>= ".num_f($flete_40nor)."</div>" ?>
                            </td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <td>
                            <div class='tc_lb' style="width:150px; margin-left: 0px; margin-right: 0px; padding-top: 3px">TOTAL (STD/HC/NOR)</div>
                            <div class='tc_tot' style="border-top: solid #101010 1px; padding-top: 3px">=
                                <?php
                                //$t=($_40_std*$tarifas['precio_40'])+($_40_hc*$tarifas['precio_hr'])+($_40_nor*$tarifas['precio_nor']);
                                $t=$flete_40std+$flete_40hc+$flete_40nor;
                                echo num_f($t);
                                ?>
                            </div>
                        </td>
                    </tr>

                </table>
            </td>
        </tr>

        <!--tr>
            <th style="text-align: left;" class="celda_desc">
                <?php lang("Exceso Cont 20","Exceso Cont 20")?>
            </th>
            <td style="text-align: left">
                <?php
                $ex20=$con->get_precio_por_ciudad($city_r)*$exceso_20;
                echo "<div class='tc_lb' style='height: 5px'></div><div class='tc_cal' style='height: 5px'></div><div class= 'tc_tot'> ".num_f($flete_20)."</div>" ?>
            </td>
        </tr>

        <tr>
            <th style="text-align: left;" class="celda_desc">
                <?php lang("Exceso Cont 40","Exceso Cont 40")?>
            </th>
            <td style="text-align: left">
                <?php
                $ex40=$con->get_precio_por_ciudad($city_r)*$exceso_40;
                echo "<div class='tc_lb' style='height: 5px'></div><div class='tc_cal' style='height: 5px'></div><div class= 'tc_tot'> ".num_f($ex20)."</div>" ?>
            </td>
        </tr-->
        <tr>
            <th style="text-align: left;" class="celda_desc">
                <?php lang("BSC","BSC")?>
            </th>
            <td style="text-align: left">
                <?php
                $t=($_20_std*$tarifas['bsc_20'])+($_40_std*$tarifas['bsc_40'])+($_40_hc*$tarifas['bsc_hc'])+($_40_nor*$tarifas['bsc_nor'])*$n_cont;
                echo num_f($t);
                ?>
            </td>
        </tr>


        <tr>
            <th style="text-align: left;" class="celda_desc">
                <?php lang("ISP","ISP")?>
            </th>
            <td style="text-align: left">
                <?php
                $t=($_20_std*$tarifas['isp_20'])+($_40_std*$tarifas['isp_40'])+($_40_hc*$tarifas['isp_hc'])+($_40_nor*$tarifas['isp_nor'])*$n_cont;
                echo num_f($t)
                ?>
            </td>
        </tr>

        <tr>
            <th style="text-align: left;" class="celda_desc">
                <?php lang("THCO","THCO")?>
            </th>
            <td style="text-align: left">
                <?php
                $t=($_20_std*$tarifas['tho_20'])+($_40_std*$tarifas['tho_40'])+($_40_hc*$tarifas['tho_hc'])+($_40_nor*$tarifas['tho_nor'])*$n_cont;
                echo num_f($t);
                ?>
            </td>
        </tr>


        <tr>
            <th style="text-align: left;" class="celda_desc">
                <?php lang("OTHER SUB-CHARGES","Otros Recargos")?>
            </th>
            <td style="text-align: left">
                <?php echo num_f($tarifas['other_charges']*$n_cont)?>
            </td>
        </tr>
        <!---------------------------------------------------------------->
        <tr>
            <th style="text-align: left;" class="celda_desc">
                <hr>
                <strong>TOTAL</strong>
                <hr>
            </th>
            <td>
                <hr>
                0
                <hr>
            </td>
        </tr>

        <!--tr>
            <th style="text-align: left;" class="celda_desc">
                <?php lang("BL FEE ","BL FEE ")?>
            </th>
            <td style="text-align: left">
                <?php echo num_f($tarifas['bl_fee']*$n_cont)?>
            </td>
        </tr-->
        <?php if($tipof=='im'){?>
        <tr>
            <th style="text-align: left;" class="celda_desc">
                <?php lang("COSOTOS LOCALES, MIAMI","LOCAL CHARGES, MIAMI")?>
            </th>
            <td style="text-align: left">
                <?php //echo num_f($tarifas['bl_fee']*$n_cont)?>
            </td>
        </tr>
        <tr>
            <th style="text-align: left;" class="celda_desc">
                <?php lang("MUELLAJE","WHARFAGE")?>
            </th>
            <td style="text-align: left">
                <?php //echo num_f($tarifas['bl_fee']*$n_cont)?>
            </td>
        </tr>
        <tr>
            <th style="text-align: left;" class="celda_desc">
                <?php lang("MANEJO DE CARGA","CARGO HANDLING")?>
            </th>
            <td style="text-align: left">
                <?php //echo num_f($tarifas['bl_fee']*$n_cont)?>
            </td>
        </tr>
        <tr>
            <th style="text-align: left;" class="celda_desc">
                <?php lang("RECOGIDA","PICK UP")?>
            </th>
            <td style="text-align: left">
                <?php //echo num_f($tarifas['bl_fee']*$n_cont)?>
            </td>
        </tr>
        <tr>
            <th style="text-align: left;" class="celda_desc">
                <hr>
                <strong>TOTAL</strong>
                <hr>
            </th>
            <td>
                <hr>
                0
                <hr>
            </td>
        </tr>
        <!---------------------------------------------------------------->
        <?php } ?>


        <tr>
            <th style="text-align: left;" class="celda_desc">
                <?php lang("COSTOS LOCALES, ECUADOR","LOCAL CHARGES, ECUADOR")?>
            </th>
            <td style="text-align: left">
                <?php //echo num_f($tarifas['bl_fee']*$n_cont)?>
            </td>
        </tr>
        <?php if($tipof=="im"){?>
        <tr>
            <th style="text-align: left;" class="celda_desc">
                <?php lang("SERVICIO IMPORTACION","IMPORT SERVICES")?>
            </th>
            <td style="text-align: left">
                <?php //echo num_f($tarifas['bl_fee']*$n_cont)?>
            </td>
        </tr>
        <?php }else{ ?>
        <tr>
            <th style="text-align: left;" class="celda_desc">
                <?php lang("SERVICIO EXPORTACION","EXPORT SERVICES")?>
            </th>
            <td style="text-align: left">
                <?php //echo num_f($tarifas['bl_fee']*$n_cont)?>
            </td>
        </tr>
        <?php } ?>



        <tr>
            <th style="text-align: left;" class="celda_desc">
                <?php lang("DOCUMENTACION","DOCUMENT FEE")?>
            </th>
            <td style="text-align: left">
                <?php //echo num_f($tarifas['bl_fee']*$n_cont)?>
            </td>
        </tr>
        <tr>
            <th style="text-align: left;" class="celda_desc">
                <?php lang("ADMINISNTRACION DE CNTRS","CNTRS ADMIN")?>
            </th>
            <td style="text-align: left">
                <?php //echo num_f($tarifas['bl_fee']*$n_cont)?>
            </td>
        </tr>
        <tr>
            <th style="text-align: left;" class="celda_desc">
                <?php lang("THCD","THCD")?>
            </th>
            <td style="text-align: left">
                <?php echo num_f($tarifas['thcd']*$n_cont)?>
            </td>
        </tr>
        <tr>
            <th style="text-align: left;" class="celda_desc">
                <?php lang("CARTA DE SALIDA","DELIVERY ORDER")?>
            </th>
            <td style="text-align: left">
                <?php //echo num_f($tarifas['bl_fee']*$n_cont)?>
            </td>
        </tr>
        <tr>
            <th style="text-align: left;" class="celda_desc">
                <?php lang("SEGURO DE CONTENEDORES","CARGO INSURANCE")?>
            </th>
            <td style="text-align: left">
                <?php //echo num_f($tarifas['bl_fee']*$n_cont)?>
            </td>
        </tr>
        <tr>
            <th style="text-align: left;" class="celda_desc">
                <?php lang("RECEPCION-INSPECTION CNTRS","RECPECTION.INSPECTION OF CNTR")?>
            </th>
            <td style="text-align: left">
                <?php //echo num_f($tarifas['bl_fee']*$n_cont)?>
            </td>
        </tr>
        <!---------------------------------------------------------------->
        <tr>
            <th style="text-align: left;" class="celda_desc">
                <hr>
                <strong>TOTAL</strong>
                <hr>
            </th>
            <td>
                <hr>
                0
                <hr>
            </td>
        </tr>
        <tr>
            <th style="text-align: left;" class="celda_desc">
                <?php lang("SERVICIOS ADICIONALES  DESTINO","ADDITIONAL SERVICES")?>
            </th>
            <td style="text-align: left">
                <?php echo num_f($tarifas['d_o']*$n_cont)?>
            </td>
        </tr>
        <tr>
            <th style="text-align: left;" class="celda_desc">
                <?php lang("ADUANA","CUSTOMS CLEARANCE")?>
            </th>
            <td style="text-align: left">
                <?php echo num_f($tarifas['d_o']*$n_cont)?>
            </td>
        </tr>
        <?php if($tipof=="im"){?>
            <tr>
                <th style="text-align: left;" class="celda_desc">
                    <?php lang("TRAMITE CERTIFICADO INEN","INNEN CERTIFICATE")?>
                </th>
                <td style="text-align: left">
                    <?php echo num_f($tarifas['d_o']*$n_cont)?>
                </td>
            </tr>
            <tr>
                <th style="text-align: left;" class="celda_desc">
                    <?php lang("EMISION DE BL ORIGINALES","ORIGINAL BL ISSUANCE")?>
                </th>
                <td style="text-align: left">
                    <?php echo num_f($tarifas['d_o']*$n_cont)?>
                </td>
            </tr>
            <tr>
                <th style="text-align: left;" class="celda_desc">
                    <?php lang("ENTREGA DE CARGA","DELIVERY")?>
                </th>
                <td style="text-align: left">
                    <?php echo num_f($tarifas['d_o']*$n_cont)?>
                </td>
            </tr>
            <tr>
                <th style="text-align: left;" class="celda_desc">
                    <?php lang("COBRANZA FLETE COLLECT","COLLECT ION FREIHGT SERVICES ")?>
                </th>
                <td style="text-align: left">
                    <?php echo num_f($tarifas['d_o']*$n_cont)?>
                </td>
            </tr>
        <?php }else{?>
            <tr>
                <th style="text-align: left;" class="celda_desc">
                    <?php lang("TRAMITE CERTIFICADO DE ORIGEN","ORIGIN CERTIFICATE")?>
                </th>
                <td style="text-align: left">
                    <?php echo num_f($tarifas['import_services']*$n_cont)?>
                </td>
            </tr>
            <tr>
                <th style="text-align: left;" class="celda_desc">
                    <?php lang("EMISION DE BL ORIGINALES EN DESTINO","ORIGINAL BL ISSUANCE IN DESTINATION")?>
                </th>
                <td style="text-align: left">
                    <?php echo num_f($tarifas['export_services']*$n_cont)?>
                </td>
            </tr>
            <tr>
                <th style="text-align: left;" class="celda_desc">
                    <?php lang("RECOGIDA DE CARGA","PICK UP")?>
                </th>
                <td style="text-align: left">
                    <?php echo num_f($tarifas['export_services']*$n_cont)?>
                </td>
            </tr>
            <tr>
                <th style="text-align: left;" class="celda_desc">
                    <?php lang("COBRANZA FLETE PREPAGADO","PREPAID COLLECTION SERVICES ")?>
                </th>
                <td style="text-align: left">
                    <?php echo num_f($tarifas['export_services']*$n_cont)?>
                </td>
            </tr>
        <?php } ?>
        <!---------------------------------------------------------------->
        <tr>
            <th style="text-align: left;" class="celda_desc">
                <hr>
                <strong>TOTAL</strong>
                <hr>
            </th>
            <td style="padding-bottom: 0em; border-bottom: 0px">
                <hr>
                0
                <hr>
            </td>
        </tr>




    </table>

    <div style="margin: auto; width: 125px">
        <a class="atras_form" href="cotiza_datos.php?id_r=<?php echo $id_ruta; ?>&id_p=<?php echo $id_pr; ?>"> <?php lang("Back","Atras" )?> </a>
    </div>

</section>
