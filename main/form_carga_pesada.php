<?php include_once("../admin/Consultas.php");?>
<?php include_once("../admin/funciones.php");?>
    <h1>COTIZACION CARGA  RORO/PESADA/SOBREDIMENSIONADA</h1>

    <ul class="register">
        <li class="reg1">
            <label><?php lang("Embarcador","Embarcador" )?> </label>
            <input type="text" name="name" required>
        </li>

        <li class="reg2 first">
            <label><?php lang("Fecha de Solicitud","Fecha de Solicitud")?> </label>
            <input type="text" name="pickup" required>
        </li>
        <li class="reg2">
            <label><?php lang("Pais","Pais" )?> </label>
            <input type="text" name="zip" required>
        </li>

        <li class="reg2 first">
            <label><?php lang("Ciudad","Ciudad" )?> </label>
            <input type="text" name="phone" required>
        </li>
        <li class="reg2">
            <label><?php lang("Telefono de Contacto","Telefono de Contacto" )?> </label>
            <input type="text" name="mobil" required>
        </li>

        <li class="reg1">
            <label><?php lang("Direccion","Direccion" )?> </label>
            <input type="text" name="mail" required>
        </li>

        <li class="reg1">
            <label><?php lang("Email","Email" )?> </label>
            <input type="text" name="loading" >
        </li>

        <li class="reg1">
            <label><?php lang("Direccion de Entrega","Direccion de Entrega" )?> </label>
            <input type="text" name="discharge" >
        </li>

        <li class="reg2 first">
            <label><?php lang("Pais","Pais" )?> </label>
            <input type="text" name="commodity" >
        </li>
        <li class="reg2">
            <label><?php lang("Ciudad","Ciudad" )?> </label>
            <input type="text" name="fork" >
        </li>

        <li class="reg1 first" style=" ">
            <label><?php lang("Telefono de Contacto","Telefono de Contacto" )?> </label>
            <input type="text" name="contacto" style="margin-top: 6px;width: 310px; margin-right: 300px" >
        </li>


        <li class="reg1 first">
            <label><?php lang("Producto","Producto" )?> </label>
            <input type="text" name="nombre_p" style="margin-top: 6px" >
        </li>

        <li class="reg2 first">
            <label><?php lang("Apilable","Apilable" )?> </label>
            <select name="type_cont" style="margin-top: 8px; width: 100%">
                <option value="Si">Si</option>
                <option value="No">No</option>
            </select>
        </li>
        <li class="reg2">
            <label><?php lang("Estiba sobre Cubierta","Estiba sobre Cubierta" )?> </label>
            <select name="type_cont" style="margin-top: 8px; width: 100%">
                <option value="Si">Si</option>
                <option value="No">No</option>
            </select>
        </li>

        <li class="reg2 first">
            <label><?php lang("Idea de Flete","Idea de Flete" )?> </label>
            <input type="text" name="flete" style="margin-top: 6px" >
        </li>


        <li class="reg2">
            <label><?php lang("Fecha de Embarque","Fecha de Embarque" )?> </label>
            <input type="text" name="fecha_e" >
        </li>
        <li class="reg1 first">
            <label><?php lang("POR FAVOR ADJUNTAR EL ARCHIVO CON LA LISTA DE EMPAQUE DE LA CARGA","POR FAVOR INDIQUE EL ARCHIVO CON LA LISTA DE EMPAQUE DE LA CARGA" )?> </label>
            <input type="file" name="lista" style="width: 100%; height: 28px; margin-top: 8px">
        </li>


    </ul>
    <table style="width: 100%">
        <tr>
            <td style="text-align: center">
                <a onclick="validar()" class="<?php lang("reg-next","reg-nextESP")?>" style="position: relative"><?php lang("Enviar","Enviar" )?>  </a>
            </td>
        </tr>
        <tr>
            <!--td style="text-align: center; height: 40px; vertical-align: bottom">
                <a href="" style="color: #00389D"><?php lang("Descarga de formato para calcular volumen de carga","Descarga de formato para calcular volumen de carga" )?> </a>
            </td-->
        </tr>
    </table>
