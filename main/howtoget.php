<?php include_once("../admin/Consultas.php");?>
<?php include_once("../admin/funciones.php");?>
<?php
require_once '../include/header.php';
$lst_s=$con->get_lst_Banner_Small_activo();

$c=0;
foreach($lst_s as $item){
    $lst_url_s[$c] = $item["url"];
    $lst_img_s[$c] = $item["imagen"];
    $c++;
}

?>
<script>
    $(document).ready(function() {
        $('nav#menu-top ul.main-sect > li:nth-child(5) > a').addClass("page-on");
    });
</script>
<!-- =================== CONTENIDO  =================== -->         

        <div id="content">

            <?php
                require_once '../include/aside.php';
            ?>
            <div id="main-content" class="aliados">
                <nav>
                    <ul>
                        <li ><a <?php if($_SESSION["idioma"]=="esp"){echo "style='padding: 16px 1px 0px 16px;' ";} ?>><?php lang("Sea Freight","Carga Maritima");?></a></li>
                        <li ><a <?php if($_SESSION["idioma"]=="esp"){echo "style='padding: 16px 1px 0px 16px;' ";} ?>><?php lang("Air cargo","Carga Aérea");?></a></li>
                        <li ><a <?php if($_SESSION["idioma"]=="esp"){echo "style='padding: 16px 1px 0px 16px;' ";} ?>><?php lang("Pick up & Delivery","Recogida y Entrega");?></a></li>
                        <li ><a <?php if($_SESSION["idioma"]=="esp"){echo "style='padding: 16px 1px 0px 16px;' ";} ?>><?php lang("Customs","Aduanas");?></a></li>
                    </ul>
                </nav>

                <div class="grey-box">
					
                    <h2><b>How to get your rate</b></h2>
                    <p>Before using our freight calculator you will need to know the Weight of your cargo in Kilograms. The height, width and length in centimeters; once you have this information then:</p>						
                     <ol class="num-lists">
                        <li>Decide whether you want a quote for sea or air transportation.
                        <li>Choose if you will export from Ecuador or import to Ecuador
                        <li>Choose the type of shipping mode you will use (full container FCL or Consolidation cargo LCL).</li>
                        <li>For FCL shipments Click on the type /size of container you need for your cargo and click send to input full details.</li>
                        <li>Enter your shipping details - origin, destination, Pick up – Delivery address, Type of services and click “Get a quote” to obtain the rates.</li>
                        <li>For LCL shipments choose the delivery service required and click send to input full details.</li>
                        <li>Enter your shipping details - origin, destination, Pick up – Delivery address, Type of services and click “Get a quote” to obtain our rates.</li>
                        <li>Click on your selected rate and continue with the booking of your cargo.</li>
                        <li>Enter shipment address details - pickup and delivery location, commodity, insurance, etc.</li>
                        <li>Choose the payment method or get credit terms.</li>
                        <li>The first confirmation email hits your inbox letting you know of the successful booking.</li>
                        <li>Our representative will contact you to schedule the pick-up of the shipment.</li>
                        <li>Track your shipment step by step.</li>
                        <li>Cargo Delivered, Customer Satisfied.</li>
                    </ol>
                </div>



               <div id="logos">
                    <div class="viewport">
                        <ul class="overview">
                            <?php $c=1; ?>
                            <?php for($i=1; $i<=count($lst_url_s); $i++){ ?>
                                    <?php if($i==1) echo "<li>\n" ?>
                                    <a href="<?php echo $lst_url_s[$i-1] ?>"><img src="../img/banner_small/<?php echo $lst_img_s[$i-1] ?>"></a>
                                    <?php if($i%3==0) echo "</li><li>\n" ?>
                                    <?php
                                        if($i==count($lst_url_s)){
                                        echo "</li>\n" ;
                                    }?>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                
                <script type="text/javascript">
                        $(document).ready(function(){
                            $("#logos").tinycarousel({
                                    bullets  : true, interval  : true
                            });
                        });
                </script>
                
            </div>
        </div>


<!-- =================== FOOTER  ====================== -->   

<?php
    require_once '../include/footer.php';
?>