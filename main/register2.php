<?php include_once("../admin/Consultas.php");?>
<?php include_once("../admin/funciones.php");?>

<?php

    require_once '../include/header.php';
    $reg=false;
    if($_POST){

              require_once('../Captcha/recaptchalib.php');
              $privatekey = "6LfVSf0SAAAAACD-fljxMyEW49v-C2EVbFQC6uYD";
              $resp = recaptcha_check_answer ($privatekey,
                                            $_SERVER["REMOTE_ADDR"],
                                            $_POST["recaptcha_challenge_field"],
                                            $_POST["recaptcha_response_field"]);

              if (!$resp->is_valid) {
                // Lo que hace el captcha si no es correcto
                //die ("The reCAPTCHA wasn't entered correctly. Go back and try it again." .
                //"(reCAPTCHA said: " . $resp->error . ")");
?>

                <script>ControlRegister('.reg-step03','.reg-step01');</script>;
<?php             
              } else {
                 $reg=agrega_usuario(); 
                
              }        
    }

?>



 




<script>
    $(document).ready(function() {
        //$('nav#menu-top ul.main-sect > li:nth-child(2) a').addClass("page-on");
    });
    
    function ControlRegister(rIn, rOut){
        $(rOut).fadeOut(100);
        $(rIn).fadeIn(700);
        
        if(rIn==".reg-step01"){
            $('.action-line section').removeClass('step-dotactive , step-lineactive , step-dotoff , step-lineoff');
            $('.action-line section:nth-child(1)').addClass('step-dotactive');
            $('.action-line section:nth-child(2)').addClass('step-lineoff');
            $('.action-line section:nth-child(3)').addClass('step-dotoff');
            $('.action-line section:nth-child(4)').addClass('step-lineoff');
            $('.action-line section:nth-child(5)').addClass('step-dotoff');
        }
        
        if(rIn==".reg-step02"){
            $('.action-line section').removeClass('step-dotactive , step-lineactive , step-dotoff , step-lineoff');
            $('.action-line section:nth-child(1)').addClass('step-dotactive');
            $('.action-line section:nth-child(2)').addClass('step-lineactive');
            $('.action-line section:nth-child(3)').addClass('step-dotactive');
            $('.action-line section:nth-child(4)').addClass('step-lineoff');
            $('.action-line section:nth-child(5)').addClass('step-dotoff');
        }
        if(rIn==".reg-step03"){
            $('.action-line section').removeClass('step-dotactive , step-lineactive , step-dotoff , step-lineoff');
            $('.action-line section:nth-child(1)').addClass('step-dotactive');
            $('.action-line section:nth-child(2)').addClass('step-lineactive');
            $('.action-line section:nth-child(3)').addClass('step-dotactive');
            $('.action-line section:nth-child(4)').addClass('step-lineactive');
            $('.action-line section:nth-child(5)').addClass('step-dotactive');
        }
    }
</script>
<script>
    function validar(){
        err=false;
        pag=0;
        expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if(document.fr.company_name.value==""){
            err=true;
            msg="Please enter the Company Name";
            pag=1;
        }else if(document.fr.contact_name.value==""){
            err=true;
            msg="Please enter the Contact Name";
            pag=1;
        }else if(document.fr.country.value==""){
            err=true;
            msg="Please enter your Country";
            pag=1;
        }else if(document.fr.local_number.value==""){
            err=true;
            msg="Please enter your local number";
            pag=1;
        }else if(document.fr.mail.value==""){
            err=true;
            msg="Please enter the Email";
        }else if(!expr.test(document.fr.mail.value)){
            err=true;
            msg="The format of the mail is incorrect.";
        }else if(document.fr.pass.value!=document.fr.pass2.value){
            err=true;
            msg="Repeat the password";
        }else if(document.fr.pass.value.length<8){
            err=true;
            msg="Password is case sensitive, has a minimum length of 8 characters.";
        }else if(document.fr.pass.value.length<8){
            err=true;
            msg="Password is case sensitive, has a minimum length of 8 characters.";
        }else if(document.fr.security_q.value.length==""){
            err=true;
            msg="Enter Your Secret Question ";
        }else if(document.fr.security_a.value.length==""){
            err=true;
            msg="Enter Your Secret Answer ";
        }


        if(err==true && pag==1){
            ControlRegister('.reg-step01','.reg-step03');
            alert(msg);
        }else if(err==true && pag==0){
            alert(msg);
        }else{
            document.fr.submit()


        }


    }
</script>




<!-- =================== CONTENIDO  =================== -->         

        <div id="content">
            <div id="main-content-full" class="noaliados">
                <section class="tracking-box">
                    <h1><?php lang("REGISTER","REGISTRO" )?> </h1>
                    <div class="action-line" <?php echo ($reg)? "style='display:none'":"" ?>>
                        <section class="dot step-dotactive"></section>
                        <section class="line step-lineoff"></section>
                        <section class="dot step-dotoff"></section>
                        <section class="line step-lineoff"></section>
                        <section class="dot step-dotoff"></section>
                    </div>

                    <form name="fr" method="post" action="">
                        <div id="register-panel">
                            <!--------------------------------------------------------------------------->
                            <!--------------------------------------------------------------------------->
                            <section style="display:<?php echo (!$reg)?'none':'block' ?>">
                                <ul class="register">
                                    <li class="reg1 first"> <label><?php lang("Your registration was successful.","Su inscripción se ha realizado correctamente." )?> </label>
                                    <li class="reg1 first"> <label><?php lang("Check your email to reach an email with a link that will allow you to access your account.","Verifique su correo electrónico,  al mismo llegara un  link que le permitirá validar su cuenta para que pueda hacer uso de ella." )?> </label>
                                </ul>
                            </section>
                            <!--------------------------------------------------------------------------->
                            <!--------------------------------------------------------------------------->
                            <section class="reg-step01" <?php echo ($reg)? "style='display:none'":"" ?>>
                                <h2><?php lang("Company Information","Información de la empresa" )?> </h2>
                                <ul class="register">
                                    <li class="reg2 first"> <label><?php lang("Company Name<span>*</span>","Compañia<span>*</span>" )?> </label>
                                        <input type="text" name="company_name" placeholder="<?php lang("Enter company name","Ingrese Compañia" )?> " required>
                                    </li>
                                    <li class="reg2"> <label><?php lang("Contact Name<span>*</span>","Contacto<span>*</span>" )?> </label><input type="text" name="contact_name" id="contact_name" placeholder="<?php lang("Enter contact name","Ingrese Contacto" )?> " required="required"> </li>
                                    <li class="reg1"> <label><?php lang("R.U.C / COMPANY REGISTRATION NUMBER","R.U.C /Registro  de compañia" )?> </label><input type="text" name="ruc" placeholder="<?php lang("Enter R.U.C / COMPANY REGISTRATION NUMBER","Ingrese RUC / Registro  de compañia" )?> "> </li>
                                    <li class="reg2 first"> <label><?php lang("Street Address<span>*</span>","Dirección<span>*</span>" )?> </label><input type="text" name="address" placeholder="<?php lang("Enter your address","Ingrese Dirección" )?> " > </li>
                                    <li class="reg2"> <label><?php lang("Country","País" )?> </label><input type="text" name="country" placeholder="<?php lang("Enter your Country","Ingrese País" )?> "> </li>
                                    <li class="reg3 first"> <label><?php lang("City","Ciudad" )?> </label><input type="text" name="city" placeholder="<?php lang("Enter city","Ingrese Ciudad" )?> "> </li>
                                    <li class="reg3"> <label><?php lang("State","Estado" )?> </label><input type="text" name="state" placeholder="<?php lang("Enter state","Ingrese Estado" )?> "> </li>
                                    <li class="reg3"> <label><?php lang("ZIP Code","Codigo Postal" )?> </label><input type="text" name="zipcode" placeholder="<?php lang("Enter zip code","Ingrese Codigo Postal" )?> "> </li>
                                    <li class="reg2 first"> <label><?php lang("Local Number","Teléfono de Oficina" )?> </label><input type="text" name="local_number" placeholder="<?php lang("Enter local number","Ingrese Teléfono de Oficina" )?> "> </li>
                                    <li class="reg2"> <label><?php lang("Mobile Number","Teléfono Movil" )?> </label><input type="text" name="mobile_number" placeholder="<?php lang("Enter mobile number","Ingrese Teléfono Movil" )?> "> </li>
                                </ul>
                                <a onclick="ControlRegister('.reg-step02','.reg-step01');" class="<?php lang("reg-next","reg-nextESP")?>"><?php lang("Next","Siguiente" )?> </a>
                            </section>
                        
                            <section class="reg-step02">
                                <h2><?php lang("Transport Preference","Transporte habitual" )?> </h2>

                                <table  class="radio-list">
                                    <tr>
                                        <td> <?php lang("Full Container Load (FCL)","Contenedores Completos (FCL)" )?>  </td>
                                        <td> <input type="radio" name="transpref01" value="1"><label><?php lang("Yes","Si" )?></label> </td>
                                        <td> <input type="radio" name="transpref01" value="0"><label><?php lang("Si","No" )?></label> </td>
                                    </tr>
                                    <tr>
                                        <td> <?php lang("Less Container Load (LCL)","Carga Consolidada (LCL)" )?> </td>
                                        <td> <input type="radio" name="transpref02" value="1"><label><?php lang("Yes","Si" )?></label> </td>
                                        <td> <input type="radio" name="transpref02" value="0"><label><?php lang("Si","No" )?></label> </td>
                                    </tr>
                                    <tr>
                                        <td> <?php lang("Air Cargo","Carga Aerea" )?> </td>
                                        <td> <input type="radio" name="transpref03" value="1"><label><?php lang("Yes","Si" )?></label> </td>
                                        <td> <input type="radio" name="transpref03" value="0"><label><?php lang("Si","No" )?></label> </td>
                                    </tr>
                                </table>


                                <h2><?php lang("Typical Sizes","Tipo de carga" )?> </h2>

                                <table  class="typicalsizes">
                                    <tr>
                                        <td> <input type="checkbox" name="ts_fb" value="1"><label><?php lang("Few Boxes","Cajas" )?> </label> </td>
                                        <td> <input type="checkbox" name="ts_fp" value="1"><label><?php lang("Few Pallets","Paletas" )?> </label> </td>
                                        <td> </td>
                                    </tr>
                                    <tr>
                                        <td> <input type="checkbox" name="ts_20" value="1"><label>20</label> </td>
                                        <td> <input type="checkbox" name="ts_40" value="1"><label>40</label> </td>
                                        <td> <input type="checkbox" name="ts_hq" value="1"><label>40HQ</label> </td>
                                    </tr>
                                </table>

                                <a onclick="ControlRegister('.reg-step01','.reg-step02');" class="reg-back"><?php lang("Back","Atras" )?>  </a>
                                <a onclick="ControlRegister('.reg-step03','.reg-step02');" class="<?php lang("reg-next","reg-nextESP")?>"><?php lang("Next","Siguiente" )?>  </a>

                            </section>

                            <section class="reg-step03">
                                <h2><?php lang("Login Information","Información de ingreso" )?> </h2>

                                <ul class="register">
                                    <li class="reg1"> <label><?php lang("Email Address","Correo Electronico" )?> </label><input type="text" name="mail" placeholder="<?php lang("Enter email address","Ingrese Correo Electronico" )?> " required="required"> </li>
                                    <li class="reg2 first"> <label><?php lang("Password<span><span>*</span>","Clave<span>*</span>" )?> </label><input type="password" name="pass" required="required" > </li>
                                    <li class="reg2"> <label><?php lang("Password Confirmation<span>*</span>","Confirmación de Clave<span>*</span>" )?></label> <input type="password" name="pass2"                             oninvalid="this.setCustomValidity('Repeat your Password')" > </li>
                                    <li class="reg2 first"> <label><?php lang("Security Question<span>*</span>","Pregunta de Seguridad<span>*</span>" )?> </label><input type="text" name="security_q" placeholder="<?php lang("Enter your Security Question","Ingresa Pregunta de Seguridad" )?> "   required="required" > </li>
                                    <li class="reg2"> <label><?php lang("Securiry Answer<span>*</span>","Respuesta de Seguridad<span>*</span>" )?></label> <input type="text" name="security_a" placeholder="<?php lang("Enter your Securiry Answer","Ingresa Respuesta de Seguridad" )?> "             required="required" > </li>

                                </ul>

                                <table class="radio-list">
                                    <tr>
                                        <td colspan="3"> <?php lang("I want to receive offers from FCL (highly recommended).","Le gustaria recibir ofertas de FCL" )?>  </td>
                                        <td> <input type="radio" name="pr_01" value="1"><label><?php lang("Yes","Si" )?> </label> </td>
                                        <td> <input type="radio" name="pr_01" value="0"><label><?php lang("No","No" )?> </label> </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"> <?php lang("Have you ever done import/export operations? ","Ha realizado operaciones de Importacion / Exportacion" )?> </td>
                                        <td> <input type="radio" name="pr_02" value="1"><label><?php lang("Yes","Si" )?> </label> </td>
                                        <td> <input type="radio" name="pr_02" value="0"><label><?php lang("No","No" )?> </label> </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"> <?php lang("Are you currently doing shipments? ","Esta actualmente realizando embarques" )?> </td>
                                        <td> <input type="radio" name="pr_03" value="1"><label><?php lang("Yes","Si" )?> </label> </td>
                                        <td> <input type="radio" name="pr_03" value="0"><label><?php lang("No","No" )?> </label> </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"><?php lang("Do you have a single shipment or recurring shipments? ","Tiene embarques esporadicos o frecuentes" )?>  </td>
                                        <td> <input type="radio" name="pr_04" value="1"><label><?php lang("Single","Esporadicos" )?> </label> </td>
                                        <td> <input type="radio" name="pr_04" value="0"><label><?php lang("Recurring","Frecuentes" )?> </label> </td>
                                    </tr>
                                </table>


                                <h2><?php lang("Current frequency of shipments","Embarques frecuentes" )?> </h2>

                                <table  class="radio-list">
                                    <tr>
                                        <td><?php lang("LESS THAN 5 SHIPMENTS","MENOS DE 5 EMBARQUES" )?>   </td>
                                        <td> <input type="radio" name="frqship" value="11" ><label><?php lang("Day","Dia" )?> </label> </td>
                                        <td> <input type="radio" name="frqship" value="12"><label><?php lang("Week","Semana" )?> </label> </td>
                                        <td> <input type="radio" name="frqship" value="13"><label><?php lang("Month","Mes" )?> </label> </td>
                                        <td> <input type="radio" name="frqship" value="14"><label><?php lang("Year","Año" )?> </label> </td>
                                    </tr>
                                    <tr>
                                        <td><?php lang("5 TO 20 SHIPMENTS","DE 5 A 20 EMBARQUES" )?>   </td>
                                        <td> <input type="radio" name="frqship" value="21" ><label><?php lang("Day","Dia" )?> </label> </td>
                                        <td> <input type="radio" name="frqship" value="22"><label><?php lang("Week","Semana" )?> </label> </td>
                                        <td> <input type="radio" name="frqship" value="23"><label><?php lang("Month","Mes" )?> </label> </td>
                                        <td> <input type="radio" name="frqship" value="24"><label><?php lang("Year","Año" )?> </label> </td>
                                    </tr>
                                    <tr>
                                        <td><?php lang("21 TO 50 SHIPMENTS","DE 21 A 50 EMBARQUES" )?>   </td>
                                        <td> <input type="radio" name="frqship" value="31" ><label><?php lang("Day","Dia" )?> </label> </td>
                                        <td> <input type="radio" name="frqship" value="32"><label><?php lang("Week","Semana" )?> </label> </td>
                                        <td> <input type="radio" name="frqship" value="33"><label><?php lang("Month","Mes" )?> </label> </td>
                                        <td> <input type="radio" name="frqship" value="45"><label><?php lang("Year","Año" )?> </label> </td>
                                    </tr>
                                    <tr>
                                        <td> <?php lang("MORE THAN 51 SHIPMENTS","MÁS DE 51 EMBARQUES" )?>  </td>
                                        <td> <input type="radio" name="frqship" value="41" ><label><?php lang("Day","Dia" )?> </label> </td>
                                        <td> <input type="radio" name="frqship" value="42"><label><?php lang("Week","Semana" )?> </label> </td>
                                        <td> <input type="radio" name="frqship" value="43"><label><?php lang("Month","Mes" )?> </label> </td>
                                        <td> <input type="radio" name="frqship" value="44"><label><?php lang("Year","Año" )?> </label> </td>
                                    </tr>
                                </table>
                                <div style="margin: 0 auto;">
                                <?php
                          require_once('../Captcha/recaptchalib.php');
                          $publickey = "6LfVSf0SAAAAAJyu32QqlC5SO9-cGV9hZL98WAdG"; // you got this from the signup page
                          echo recaptcha_get_html($publickey);
                          ?>
                          </div>
                                <a class="reg-back" onclick="ControlRegister('.reg-step02','.reg-step03');"><?php lang("Back","Atras" )?>  </a>
                                <!--input type="submit" class="edit-save" style="width: 84px; border: none; top: 578px; bottom: -2px"-->
                                <a onclick="validar()" class="<?php lang("edit-save","edit-saveESP")?>" id="enviar"><?php lang("Save","Guardar" )?> </a>

                            </section>
                        </div>
                            
                    </form>
                </section>
            </div>
        </div>

<script>


</script>

<!-- =================== FOOTER  ====================== -->   


<?php
    require_once '../include/footer.php';
?>
