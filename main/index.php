<?php include_once("../admin/Consultas.php");?>
<?php include_once("../admin/funciones.php");?>
<?php
require_once '../include/header.php';
$lst_s=$con->get_lst_Banner_Small_activo();

$c=0;
foreach($lst_s as $item){
    $lst_url_s[$c] = $item["url"];
    $lst_img_s[$c] = $item["imagen"];
    $c++;
}

?>

<script>
    $(document).ready(function() {
        $('nav#menu-top ul.main-sect > li:nth-child(2) a').addClass("page-on");
    });
</script>
<!-- =================== CONTENIDO  =================== -->         

        <div id="content">

            <?php
                require_once '../include/aside.php';
            ?>
            
            <div id="main-content" class="aliados">

                <nav>

                    <ul>
 
                        <li ><a <?php if($_SESSION["idioma"]=="esp"){echo "style='padding: 16px 1px 0px 16px;' ";} ?>><?php lang("Sea Freight","Carga Maritima");?></a></li>

                        <li ><a <?php if($_SESSION["idioma"]=="esp"){echo "style='padding: 16px 1px 0px 16px;' ";} ?>><?php lang("Air cargo","Carga Aérea");?></a></li>

                        <li ><a <?php if($_SESSION["idioma"]=="esp"){echo "style='padding: 16px 1px 0px 16px;' ";} ?>><?php lang("Pick up & Delivery","Recogida y Entrega");?></a></li>

                        <li ><a <?php if($_SESSION["idioma"]=="esp"){echo "style='padding: 16px 1px 0px 16px;' ";} ?>><?php lang("Customs","Aduanas");?></a></li>

                    </ul>

                </nav>

                <div class="grey-box">

                    <form>

                        <h5><?php lang("ONLINE FREIGHT QUOTE","COTIZADOR DE CARGA ONLINE")?></h5>

                        <h5><?php lang("CHOOSE THE BEST RATE FOR YOUR CARGO","ELIJA LA MEJOR TARIFA PARA SU CARGA")?></h5>

                        <ul class="BestRate">
                            <li><input type="radio" name="best-rate" value="sea"><label><?php lang("sea","mar")?></label></li>
                            <li><input type="radio" name="best-rate" value="air"><label><?php lang("air","aire")?></label></li>
                            <li><input type="radio" name="best-rate" value="household"><label><?php lang("household","domestico")?></label></li>
                            <li><input type="radio" name="best-rate" value="export"><label><?php lang("export","exportación")?></label></li>
                            <li><input type="radio" name="best-rate" value="import"><label><?php lang("import","importación")?></label></li>
                        </ul>

                        <h5><?php lang("METHOD OF SHIPMENT","FORMA DE ENVÍO")?></h5>

                        <ul class="MethodShipment">

                            <li>

                                <p><?php lang("ORIGIN","ORIGEN")?></p>

                                <select>

                                    <option>Miami</option>

                                    <option>Venezuela</option>

                                    <option>Panama</option>

                                    <option>Colombia</option>

                                </select>

                                <p><?php lang("ZIPCODE","CÓDIGO POSTAL")?></p>

                                <input type="text" name="origin-zip" placeholder="ZIPCODE">

                            </li>

                            

                            <li>

                                <p><?php lang("DESTINATION","DESTINO")?></p>

                                <select>
                                    <option>Miami</option>
                                    <option>Venezuela</option>
                                    <option>Panama</option>
                                    <option>Colombia</option>
                                </select>

                                <p><?php lang("ZIPCODE","CÓDIGO POSTAL")?></p>

                                <input type="text" name="destination-zip" placeholder="ZIPCODE">

                            </li>

                        </ul>

                        <input type="submit" value="">

                    </form>

                </div>

                <div id="logos">
                    <div class="viewport">
                        <ul class="overview">
                            <?php $c=1; ?>
                            <?php for($i=1; $i<=count($lst_url_s); $i++){ ?>
                                    <?php if($i==1) echo "<li>\n" ?>
                                    <a href="<?php echo $lst_url_s[$i-1] ?>"><img src="../img/banner_small/<?php echo $lst_img_s[$i-1] ?>"></a>
                                    <?php if($i%3==0) echo "</li><li>\n" ?>
                                    <?php
                                        if($i==count($lst_url_s)){
                                        echo "</li>\n" ;
                                    }?>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                
                <script type="text/javascript">
                        $(document).ready(function(){
                            $("#logos").tinycarousel({
                                    bullets  : true, interval  : true
                            });
                        });
                </script>
                
            </div>
        </div>


<!-- =================== FOOTER  ====================== -->   

<?php
    require_once '../include/footer.php';
?>