<?php


require_once('../Captcha/recaptchalib.php');

$privatekey = "6LfVSf0SAAAAACD-fljxMyEW49v-C2EVbFQC6uYD";
$resp = recaptcha_check_answer ($privatekey,
    $_SERVER["REMOTE_ADDR"],
    $_POST["recaptcha_challenge_field"],
    $_POST["recaptcha_response_field"]);

if ($resp->is_valid) {
    ?>success<?
    $_SESSION['captcha'] = 1;
}
else
{
    die ("The reCAPTCHA wasn't entered correctly. Go back and try it again." .
        "(reCAPTCHA said: " . $resp->error . ")");
}
?>