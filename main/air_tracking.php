<?php include_once("../admin/Consultas.php");?>
<?php include_once("../admin/funciones.php");?>

<?php
    require_once '../include/header.php';
    $con=new Consultas();

    $awb_number=$_GET["awb_number"];
    $track=$con->get_air_tracking($awb_number);

    $lst_d=$con->get_air_tracking_detail($awb_number);

?>

<script>

</script>
<!-- =================== CONTENIDO  =================== -->
        <div id="content">
            <div id="main-content-full" class="noaliados">
                <section class="tracking-box">
                    <h1>TRACK SHIPMET/ RASTREO DE ENVIOS</h1>
                    <h2>Air Tracking / Aereo</h2>
                    <?php if(isset($_SESSION['id'])){?>
                    <ul class="profile-nav">
                        <li><a><img src="../img/icon-prof-on.png"><p>MY PROFILE</p></a></li>
                        <li><a href="shipment.php"><img src="../img/icon-ship-off.png"><p>SHIPMENT</p></a></li>
                        <li><a href="chgpass.php"><img src="../img/icon-pass-off.png"><p>CHANGE PASSWORD</p></a></li>
                    </ul>
                    <?php } ?>
                    <table border="1" id="track_head">
                        <tr > 
                            <td>AWB NUMBER / GUÍA</td>
                            <td><?php echo $track["awb_number"] ?></td>
                        </tr>
                        <tr>
                            <td>Origin / Origen</td>
                            <td><?php echo $track["origen"] ?></td>
                        </tr>
                        
                        <tr>
                            <td>Destination / Destino</td>
                            <td><?php echo $track["destino"] ?></td>
                        </tr>
                        <tr>
                            <td>Number of Package / Nro. de Bultos</td>
                            <td><?php echo $track["numero_pck"] ?></td>
                        </tr>
                    </table>

                    <table border="1" id="track_detail" class="table">
                        <thead>
                            <tr>
                                <td>DEPARTURE AIRPORT <br>AREOPTO –SALIDA</td>
                                <td>DEPARTURE DATE<br>FECHA- SALIDA</td>
                                <td>ARRIVAL AIRPORT<br>AREOPTO-LLEGADA</td>
                                <td>ARRIVALDATE<br>FECHA – ARRIBO</td>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach($lst_d as $item){ ?>
                            <tr>
                                <td><?php echo $item["dep_airport"]?></td>
                                <td><?php echo $item["dep_date"]?></td>
                                <td><?php echo $item["arr_airport"]?></td>
                                <td><?php echo $item["arr_date"]?></td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                    <img class="print_button" src="../img/print.png">
                </section>
            </div>
        </div>

<script>


</script>

<!-- =================== FOOTER  ====================== -->   


<?php
    require_once '../include/footer.php';
?>
