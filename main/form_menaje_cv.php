<?php include_once("../admin/Consultas.php");?>
<?php include_once("../admin/funciones.php");?>

    <h1>COTIZACION MENAJE DE CASA CON VEHICULO</h1>

    <ul class="register">
        <li class="reg1">
            <label><?php lang("Nombre del dueño del menaje","Nombre del dueño del menaje" )?> </label>
            <input type="text" name="name" required>
        </li>

        <li class="reg2 first">
            <label><?php lang("Fecha de Solicitud","Fecha de Solicitud")?> </label>
            <input type="text" name="pickup" required>
        </li>
        <li class="reg2">
            <label><?php lang("Pais","Pais" )?> </label>
            <input type="text" name="zip" required>
        </li>

        <li class="reg2 first">
            <label><?php lang("Ciudad","Ciudad" )?> </label>
            <input type="text" name="phone" required>
        </li>
        <li class="reg2">
            <label><?php lang("Telefono de Contacto","Telefono de Contacto" )?> </label>
            <input type="text" name="mobil" required>
        </li>

        <li class="reg1">
            <label><?php lang("Direccion","Direccion" )?> </label>
            <input type="text" name="mail" required>
        </li>

        <li class="reg1">
            <label><?php lang("Email","Email" )?> </label>
            <input type="text" name="loading" >
        </li>

        <li class="reg1">
            <label><?php lang("Direccion de Entrega","Direccion de Entrega" )?> </label>
            <input type="text" name="discharge" >
        </li>

        <li class="reg2 first">
            <label><?php lang("Pais","Pais" )?> </label>
            <input type="text" name="commodity" >
        </li>
        <li class="reg2">
            <label><?php lang("Ciudad","Ciudad" )?> </label>
            <input type="text" name="fork" >
        </li>

        <li class="reg2 first">
            <label><?php lang("Telefono de Contacto","Telefono de Contacto" )?> </label>
            <input type="text" name="stack" >
        </li>
        <li class="reg2">
            <label><?php lang("Servicio","Servicio" )?> </label>
            <select name="type_cont" style="margin-top: 8px; width: 100%">
                <option value="20">Puerto a Puerto</option>
                <option value="40">Puerta a Puerta</option>
            </select>
        </li>

        <li class="reg1 first">
            <label><?php lang("Tipo de Contenedor","Tipo de Contenedor" )?> </label>
            <select name="type_cont" style="margin-top: 8px; width: 330px; margin-bottom: 0px; margin-right: 340px">
                <option value="20">20</option>
                <option value="40">40</option>
                <option value="40hc">40HQ</option>
            </select>
        </li>



        <li class="reg2 first">
            <label><?php lang("Año del Vehiculo","Año del Vehiculo" )?> </label>
            <input type="text" name="anno_v" >
        </li>
        <li class="reg2">
            <label><?php lang("Marca / Modelo","Marca / Modelo" )?> </label>
            <input type="text" name="marca_m" >
        </li>

        <li class="reg2 first">
            <label><?php lang("Atraccion","Atraccion" )?> </label>
            <select name="atraccion" style="margin-top: 8px; width: 100%; margin-bottom: 0px">
                <option value="4 X 4">4 X 4</option>
                <option value="4 X 2">4 X 2</option>
                <option value="6 X 4">6 X 4</option>
            </select>
        </li>
        <li class="reg2">
            <label><?php lang("Fecha de Embarque","Fecha de Embarque" )?> </label>
            <input type="text" name="fecha_e" >
        </li>

        <li class="reg1 first">
            <label><?php lang("Lista de embarque de menaje","Lista de embarque de menaje" )?> </label>
            <input type="file" name="lista" style="width: 100%; height: 28px; margin-top: 8px">
        </li>

    </ul>
    <table style="width: 100%">
        <tr>
            <td style="text-align: center">
                <a onclick="validar()" class="<?php lang("reg-next","reg-nextESP")?>" style="position: relative"><?php lang("Enviar","Enviar" )?>  </a>
            </td>
        </tr>
        <tr>
            <td style="text-align: center; height: 40px; vertical-align: bottom">
                <a href="" style="color: #00389D"><?php lang("Descarga de formato para calcular volumen de carga","Descarga de formato para calcular volumen de carga" )?> </a>
            </td>
        </tr>
    </table>
