<?php include_once("../admin/Consultas.php");?>
<?php include_once("../admin/funciones.php");?>
<?php require_once '../include/header.php'; ?>
<?php
if(!isset($_SESSION['user'])){
    header ("location:index.php");
}


$con=new Consultas();
if(isset($_GET["from"]) && isset($_GET["to"])){
    $lst=$con->get_tracking($_SESSION["id"],$_GET["from"],$_GET["to"]);
}else{
    $lst=$con->get_tracking($_SESSION["id"]);
}
?>

<script>
/*  $(function() {
    $( "#datepicker" ).datepicker();
  });*/
</script>

    <script>
        $(function() {
            $( "#from" ).datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 3,
                onClose: function( selectedDate ) {
                    $( "#to" ).datepicker( "option", "minDate", selectedDate );
                }
            });
            $( "#to" ).datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 3,
                onClose: function( selectedDate ) {
                    $( "#from" ).datepicker( "option", "maxDate", selectedDate );
                }
            });
        });
    </script>
<!-- =================== CONTENIDO  =================== -->         

        <div id="content">

            <div id="main-content-full" class="noaliados">
                
                
                <section class="tracking-box">
                    <h1><?php lang("MY SHIPMENTS","MIS EMBARQUES");?></h1>
                    
                    <ul class="profile-nav">
                        <li><a href="myprofile.php"><img src="../img/icon-prof-off.png"><p><?php lang("MY PROFILE","MI PERFIL");?></p></a></li>
                        <li><a><img src="../img/icon-ship-on.png"><p><?php lang("SHIPMENT","EMBARQUES");?></p></a></li>
                        <li><a href="chgpass.php"><img src="../img/icon-pass-off.png"><p><?php lang("CHANGE PASSWORD","CAMBIAR CLAVE");?></p></a></li>
                    </ul>
                    <form method="get" action="">
                    <?php lang("FROM","DE");?><input type="text" id="from" name="from"> <?php lang("TO","A");?> <input type="text" id="to" name="to">
                        <button type="submit">OK</button>
                    </form>


                    <div id="shipment-panel">
                        <section class="reg-step01">
                            <table class="track-logs">
                        <thead>
                            <tr>
                                <th style="width: 25%"><?php lang("TRACKING NUMBER","DOCUMENTO DE EMBARQUE");?></th>
                                <th style="width: 30"><?php lang("ORIGIN","ORIGEN");?></th>
                                <th style="width: 30%"><?php lang("DESTINATION","DESTINO");?></th>
                                <th style="width: 15%"><?php lang("PICK UP DATE","FECHA DE RECOGIDA");?></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach($lst as $item){?>
                            <tr>
                                <td><a href="index.php?track=<?php echo $item["tracking"]?>"><?php echo $item["tracking"]?></a></td>
                                <td><?php echo $item["origen"]?>  </td>
                                <td><?php echo $item["destino"]?> </td>
                                <td><?php echo $item["pick_up"]?> </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>

                        
                        </section>
                    
                    </div>
                </section>
                
            </div>
        </div>


<!-- =================== FOOTER  ====================== -->   

<?php
    require_once '../include/footer.php';
?>