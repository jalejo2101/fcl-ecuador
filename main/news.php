<?php include_once("../admin/Consultas.php");?>
<?php include_once("../admin/funciones.php");?>

<?php
    require_once '../include/header.php';
    $lg=($_SESSION["idioma"]=="esp")?"_esp":"";

    $lst=$con->get_lst_noticias_activo($lg);
    $ini=1;
    if($_GET["p"]!=null){
        $ini=$_GET["p"];
    }
    $n=count($lst);

?>

<script>
    $(document).ready(function() {
    });
</script>
<!-- =================== CONTENIDO  =================== -->         

        <div id="content">
            <div id="main-content-full" class="noaliados">
                <h1>NEWS</h1>
                <?php for($i=($ini-1)*3, $c=0; $i<count($lst); $i++, $c++){ ?>
                <article class="big-news">
                    <aside><img src="../img/news/<?php echo $lst[$i]["imagen"] ?>" style="width: 156px"></aside>
                    <section>
                        <h2><?php echo $lst[$i]["titulo"] ?></h2>
                        <h6><?php echo date("F d Y", strtotime($lst[$i]["fecha"])) ?></h6>
                        <p><?php echo $lst[$i]["texto"] ?></p>
                        <!--a href="#">Read more</a-->
                    </section>
                </article>
                <?php if($c==2) break; ?>
                <?php } ?>


                <nav class="paginador">
                    <ul>
                        <?php
                        $pag=($n%3==0)?(int)($n/3):(int)($n/3)+1;
                        $next=$ini;
                        $prev=1;
                        if( ($n - $ini*3) > 0 ) $next=$ini + 1;
                        if($ini > 1)     $prev=$ini-1;
                        ?>
                        <li><a href="news.php?p=<?php echo $prev ?>"><</a></li>
                        <?php
                        for($i=0; $i<$pag; $i++){ ?>
                            <li <?php echo ($ini==($i+1))?"class='select'":"" ?>><a href="news.php?p=<?php echo $i+1 ?>"><?php echo $i+1 ?></a></li>
                        <?php } ?>
                        <li><a href="news.php?p=<?php echo $next ?>">></a></li>
                    </ul>
                </nav>
            </div>
        </div>


<!-- =================== FOOTER  ====================== -->   

<?php
    require_once '../include/footer.php';
?>