<?php include_once("../admin/Consultas.php");?>
<?php include_once("../admin/funciones.php");?>
<?php
require_once '../include/header.php';
$lst_s=$con->get_lst_Banner_Small_activo();

$c=0;
foreach($lst_s as $item){
    $lst_url_s[$c] = $item["url"];
    $lst_img_s[$c] = $item["imagen"];
    $c++;
}

?>
<script>
    $(document).ready(function() {
        $('nav#menu-top ul.main-sect > li:nth-child(4) > a').addClass("page-on");
    });
</script>
<!-- =================== CONTENIDO  =================== -->         

        <div id="content">

            <?php
                require_once '../include/aside.php';
            ?>

            <div id="main-content" class="aliados">

                <nav>

                    <ul>

                        <li ><a <?php if($_SESSION["idioma"]=="esp"){echo "style='padding: 16px 1px 0px 16px;' ";} ?>><?php lang("Sea Freight","Carga Maritima");?></a></li>

                        <li ><a <?php if($_SESSION["idioma"]=="esp"){echo "style='padding: 16px 1px 0px 16px;' ";} ?>><?php lang("Air cargo","Carga Aérea");?></a></li>

                        <li ><a <?php if($_SESSION["idioma"]=="esp"){echo "style='padding: 16px 1px 0px 16px;' ";} ?>><?php lang("Pick up & Delivery","Recogida y Entrega");?></a></li>

                        <li ><a <?php if($_SESSION["idioma"]=="esp"){echo "style='padding: 16px 1px 0px 16px;' ";} ?>><?php lang("Customs","Aduanas");?></a></li>

                    </ul>

                </nav>

                

                <div class="grey-box">
					
                    <h2><b><?php lang("PICK UP DELIVERY","RECOGIDA Y  ENTREGA DE CARGA ");?></b></h2>					
					
                    <p><?php lang(
                    "Regardless of type of shipment (FCL, LCL, Air), Freightlogistics, through our red of agents can pick up or deliver your goods from any origin to any destination",
                    "Independientemente del tipo de envío (COMPLETO, CONSOLIDADO, AEREO), Freightlogistics, a través de su red de agentes puede recoger o entregar sus mercancías desde cualquier origen a cualquier destino.");?></p>				
                   
                </div>

                <a href="index.php" class="banner-content"><img  src="../img/<?php lang("banner-calcbig.png","banner-calcbigESP.png");?>"></a>

                <div id="logos">
                    <div class="viewport">
                        <ul class="overview">
                            <?php $c=1; ?>
                            <?php for($i=1; $i<=count($lst_url_s); $i++){ ?>
                                    <?php if($i==1) echo "<li>\n" ?>
                                    <a href="<?php echo $lst_url_s[$i-1] ?>"><img src="../img/banner_small/<?php echo $lst_img_s[$i-1] ?>"></a>
                                    <?php if($i%3==0) echo "</li><li>\n" ?>
                                    <?php
                                        if($i==count($lst_url_s)){
                                        echo "</li>\n" ;
                                    }?>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                
                <script type="text/javascript">
                        $(document).ready(function(){
                            $("#logos").tinycarousel({
                                    bullets  : true, interval  : true
                            });
                        });
                </script>
                
            </div>
        </div>


<!-- =================== FOOTER  ====================== -->   

<?php
    require_once '../include/footer.php';
?>