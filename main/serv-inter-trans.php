<?php include_once("../admin/Consultas.php");?>
<?php include_once("../admin/funciones.php");?>
<?php
require_once '../include/header.php';
$lst_s=$con->get_lst_Banner_Small_activo();

$c=0;
foreach($lst_s as $item){
    $lst_url_s[$c] = $item["url"];
    $lst_img_s[$c] = $item["imagen"];
    $c++;
}

?>
<script>
    $(document).ready(function() {
        $('nav#menu-top ul.main-sect > li:nth-child(4) > a').addClass("page-on");
    });
</script>
<!-- =================== CONTENIDO  =================== -->         

        <div id="content">

            <?php
                require_once '../include/aside.php';
            ?>

            <div id="main-content" class="aliados">

                <nav>

                    <ul>

                        <li ><a <?php if($_SESSION["idioma"]=="esp"){echo "style='padding: 16px 1px 0px 16px;' ";} ?>><?php lang("Sea Freight","Carga Maritima");?></a></li>

                        <li ><a <?php if($_SESSION["idioma"]=="esp"){echo "style='padding: 16px 1px 0px 16px;' ";} ?>><?php lang("Air cargo","Carga Aérea");?></a></li>

                        <li ><a <?php if($_SESSION["idioma"]=="esp"){echo "style='padding: 16px 1px 0px 16px;' ";} ?>><?php lang("Pick up & Delivery","Recogida y Entrega");?></a></li>

                        <li ><a <?php if($_SESSION["idioma"]=="esp"){echo "style='padding: 16px 1px 0px 16px;' ";} ?>><?php lang("Customs","Aduanas");?></a></li>

                    </ul>

                </nav>

                

                <div class="grey-box">

                    <h2><b><?php lang("INTERNATIONAL TRANSPORTATION","TRANSPORTE INTERNACIONAL  ");?></b></h2>						
                    <p><?php lang(
                    "Freighlogistics, as agents for Licensed <b>NVOCC class 'A'</b> is able to offer you International Sea and Air transportation for your import and Export Cargo, our search freight calculator allow you to find an extensive range of services to move your cargo <b>from / to Ecuador:</b>",
                    "Freighlogistics,  como agente de TRANSITARIOS y  <b>NVOCC  con licencia 'A'</b>coordina la reserva de espacios  marítimos y aéreos para el embarque de su carga  de importación y exportación, nuestra calculadora de búsqueda de fletes le permitirá encontrar una amplia gama de servicios para mover su carga <b>desde / hacia  Ecuador:</b>");?> </p>							
                    
                    <h2><b><?php lang("SEA TRANSPORTATION","TRANSPORTACIÓN MARÍTIMA");?></b></h2>
                  						
                    <h3><b><?php lang("FULL CONTAINER LOADED (FCL)","EMBARQUE DE CONTENDORES COMPLETOS  (FCL)");?></b></h3>
                    
                    <p><?php lang(
                    "F.C.L. - an abbreviation for <b>'Full Container Load'.</b> The term used to describe ocean freight, when the shipper has the exclusive right to use a sea container to load the cargo, It is most cost-effective to charter a <b>20, 40 or 45 foot container</b> fix for most goods. FCL usually turns out to be the best choice to optimize your import or export operations by ocean, making it more effective. Shipment of goods worldwide into shipping containers will always be safer and more economical than using a different ocean freight, such as <b>Ro-Ro and Break-Bulk.</b></p>",
                    "F.C.L. - Una abreviatura para <b>'Full Container Load'.</b> El término utilizado para describir la carga marítima, cuando el cargador tiene el derecho exclusivo a utilizar un contenedor marítimo para cargar la carga, es más rentable alquilar un contenedor de <b>20, 40 or 45 </b> pies para la mayoría de las mercaderías a embarcarse. FCL normalmente resulta ser la mejor opción para optimizar sus operaciones de importación o de exportación vía marítima, por lo que es más eficaz. El envío de mercancías en todo el mundo en  contenedores  siempre será más seguro y más económico que el uso de un flete marítimo diferente como <b>como Ro-Ro y break-bulk.</b></p>  ");?>					

                    <h3><b><?php lang("LCL Shipments","EMBARQUES CONSOLIDADOS  (L.C.L) ");?></b></h3>							

                    <p><?php lang(
                        "For small shipments, we offer you the option of an LCL service (Less than a Container Load), which is available for both export and import. By purchasing this service, you will pay for the exact volume you are sending without the obligation to hire a full container service",
                        "Para envíos pequeños, le ofrecemos la opción de un servicio Consolidado (L.C.L) que está disponible tanto para la exportación e importación. Con la compra de este servicio, usted pagará por el volumen exacto que está enviando y sin la obligación de contratar el servicio de un contenedor completo");?></p>								


                    <h2><b><?php lang("AIR TRANSPORTATION","TRANSPORTE AEREO  ");?></b></h2>						
                    <p><?php lang(
                        "For those in need of a faster alternative to traditional sea freight shipping, we offer you the option of an air freight services. Air freight is the fastest way to ship your cargo, allowing your goods to arrive at their destination between 1 and 5 days",
                        "Para aquellos que necesitan de una alternativa más rápida para el envío de su carga, le ofrecemos la opción de un servicio de carga aérea. El flete aéreo es la manera más rápida para enviar su carga, permitiendo que sus mercancías lleguen a su destino entre 1 y 5 días");?></p>

                    <p>
                        <a href="index.php" style="color: #ffffff"><b><?php lang("GET YOUR RATES NOW","OBTENGAN SU TARFIA AHORA");?></b></a>
                    </p>

                </div>



                <div id="logos">
                    <div class="viewport">
                        <ul class="overview">
                            <?php $c=1; ?>
                            <?php for($i=1; $i<=count($lst_url_s); $i++){ ?>
                                    <?php if($i==1) echo "<li>\n" ?>
                                    <a href="<?php echo $lst_url_s[$i-1] ?>"><img src="../img/banner_small/<?php echo $lst_img_s[$i-1] ?>"></a>
                                    <?php if($i%3==0) echo "</li><li>\n" ?>
                                    <?php
                                        if($i==count($lst_url_s)){
                                        echo "</li>\n" ;
                                    }?>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                
                <script type="text/javascript">
                        $(document).ready(function(){
                            $("#logos").tinycarousel({
                                    bullets  : true, interval  : true
                            });
                        });
                </script>
                
            </div>
        </div>


<!-- =================== FOOTER  ====================== -->   

<?php
    require_once '../include/footer.php';
?>