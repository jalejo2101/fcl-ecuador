
<?php include_once("../admin/Consultas.php");?>
<?php include_once("../admin/funciones.php");?>
<?php session_start();?>
<?php
$puerto_o=$_GET["puerto_o"];
$puerto_d=$_GET["puerto_d"];
$_SESSION['puerto_o']=$puerto_o;
$_SESSION['puerto_d']=$puerto_d;


$con=new Consultas();

$lst=$con->get_lista_tarifas_por_puertos($puerto_o,$puerto_d)

?>
<style>
    .w_c{ width: 84px !important; }
    .f_h{ color: #ffffff !important; background-color: #00387D; padding: 5px 0}
</style>


<table border="1" id="track_detail" class="table" width="100%">
    <thead>
    <tr>
        <td class="w_c f_h">Container<br>20</td>
        <td class="w_c f_h">Container<br>40</td>
        <td class="w_c f_h">Container<br>40 HR</td>
        <td class="w_c f_h">Container<br>40 NOR</td>
        <td class="w_c f_h">Transit<br>Days</td>
        <td class="w_c f_h">Saling<br>Frecuency</td>
        <td class="w_c f_h">Shiping<br>Date</td>
        <td class="w_c f_h">Book Your<br>Cargo</td>
    </tr>
    </thead>
    <tbody>
    <?php foreach($lst as $item){
        $f=substr($item["fecha_salida"],8,2)."-".substr($item["fecha_salida"],5,2)."-".substr($item["fecha_salida"],0,4);
        ?>
        <tr>
            <td class="w_c"><?php echo $item["total_20"]?></td>
            <td class="w_c"><?php echo $item["total_40"]?></td>
            <td class="w_c"><?php echo $item["total_hr"]?></td>
            <td class="w_c"><?php echo $item["total_nor"]?></td>
            <td class="w_c"><?php echo $item["transit"]?></td>
            <td class="w_c"><?php echo $item["frecueny"]?></td>
            <td class="w_c"><?php echo $f?></td>
            <td class="w_c"><input type="button" onclick="window.open('cotiza_datos.php?id_r=<?php echo $item["id_ruta"]?>&id_p=<?php echo $item["id_proveedor"]?>','_self','')" style="width: 50px" value="<?php lang("Go","Ir")?>" ></td>
        </tr>
    <?php }?>
    </tbody>
</table>
<?php if(count($lst)==0){ ?>
    <div style="text-align: center; width: 50%; background-color: #afd9ee; height: auto; margin: auto; margin-top: 15px; padding-top: 20px; padding-bottom: 20px; border-radius: 10px">
        <?php lang("We cannot find  searate for your inquiry, please contact a sales representative <a href='mailto:sales@fcl-ecuador.com'> sales@fcl-ecuador.com","No se encontraron tarifas para esta ruta por favor comuníquese con ejecutivo  de venta <a href='mailto:sales@fcl-ecuador.com'> sales@fcl-ecuador.com")?> 
    </div>
<?php } ?>