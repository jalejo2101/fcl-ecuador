
<?php
    require_once '../include/header.php';
?>

<script>
    $(document).ready(function() {
        //$('nav#menu-top ul.main-sect > li:nth-child(2) a').addClass("page-on");
    });
</script>
<!-- =================== CONTENIDO  =================== -->         

        <div id="content">

            <div id="main-content-full" class="noaliados">
                <section class="tracking-box">
                    <h1>Track Shipment</h1>
                    <h2 class="sea">Sea Tracking </h2>
                    
                    <h3>Container Details</h3>
   
                    <table class="track-item">
                        <tr>
                            <td>Container Type</td>
                            <td>
                                <ul>
                                    <li>MSCU4468122</li>
                                    <li>40Ft COLLAPSIBLE STACK   BED</li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>Shipped To</td>
                            <td>FELIXSTOWE, GB</td>
                        </tr>
                        <tr>
                            <td>Final POD</td>
                            <td>FELIXSTOWE, GB8</td>
                        </tr>
                        <tr>
                            <td>ETA</td>
                            <td>05/062014</td>
                        </tr>
                    </table>
                    
                    <table class="track-logs">
                        <thead>
                            <tr>
                                <th>Location</th>
                                <th>Description</th>
                                <th>Date</th>
                                <th>Vessel</th>
                                <th>Voyage</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>ANTWERP, VAN, BE</td>
                                <td>Estimated Time of Arrival</td>
                                <td>04/06/2014</td>
                                <td>MSC VAISHNAVI R</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>GUAYAQUIL, G, EC</td>
                                <td>Loaded</td>
                                <td>18/05/2014</td>
                                <td>MSC VAISHNAVI R</td>
                                <td>NQ420R</td>
                            </tr>
                            <tr>
                                <td>GUAYAQUIL, G, EC</td>
                                <td>Gate in full</td>
                                <td>15/05/2014</td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>GUAYAQUIL, G, EC</td>
                                <td>Empty to shipper</td>
                                <td>13/05/2014</td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                 
                    
                    <a class="print" href="#">Print</a>
                </section>
                
            </div>
        </div>


<!-- =================== FOOTER  ====================== -->   

<?php
    require_once '../include/footer.php';
?>