<?php session_start();?>
<?php include_once("../admin/Consultas.php");?>
<?php include_once("../admin/funciones.php");?>

<?php
    if(!isset($_SESSION['user'])){
        header ("location:index.php");
    }


    require_once '../include/header.php';
    $con=new Consultas();



    if($_POST){
        modifica_usuario($_SESSION['id']);
        $reg=true;
    }

    if(isset($_SESSION['user'])){
    $user=$con->get_usuario($_SESSION['user']);
        $user=$user[0];
    }



?>
<!-- =================== CONTENIDO  =================== -->
        <div id="content">
            <div id="main-content-full" class="noaliados">
                <section class="tracking-box">
                    <h1><?php lang("MY PROFILE","MI PERFIL");?></h1>
                    <ul class="profile-nav">
                        <li><a><img src="../img/icon-prof-on.png"><p><?php lang("MY PROFILE","MI PERFIL");?></p></a></li>
                        <li><a href="shipment.php"><img src="../img/icon-ship-off.png"><p><?php lang("SHIPMENT","EMBARQUES");?></p></a></li>
                        <li><a href="chgpass.php"><img src="../img/icon-pass-off.png"><p><?php lang("CHANGE PASSWORD","CAMBIAR CLAVE");?></p></a></li>
                    </ul>
                    
                    <div id="register-panel">
                        <section class="reg-step01">
                            <h2><?php lang("Company Information","Información de la Compañía");?></h2>
                            <form name="profile" method="post" action="myprofile.php">
                                <ul class="register">
                                    <li class="reg2 first"><label><?php lang("Company Name","Nombre de la Compañía");?><span>*</span></label><input  type="text" name="company_name" value="<?php echo $user['company_name']?>"></li>
                                    <li class="reg2"> <label><?php lang("Contact Name","Nombre del Contacto");?><span>*</span></label><input  type="text" name="contact_name"  value="<?php echo $user['contact_name']?>"> </li>
                                    <li class="reg1"> <label><?php lang("R.U.C / COMPANY REGISTRATION NUMBER","R.U.C  / Numero de Registro Tributario de la Compañía");?></label><input  type="text" name="ruc"  value="<?php echo $user['ruc']?>"> </li>
                                    <li class="reg2 first"> <label><?php lang("Street Address","Dirección");?><span>*</span></label><input  type="text" name="address"  value="<?php echo $user['address']?>"> </li>
                                    <li class="reg2"> <label><?php lang("Country","País");?></label><input  type="text" name="country"  value="<?php echo $user['country']?>"> </li>
                                    <li class="reg3 first"> <label><?php lang("City","Ciudad");?></label><input  type="text" name="city"  value="<?php echo $user['city']?>"> </li>
                                    <li class="reg3"> <label><?php lang("State","Estado");?></label><input  type="text" name="state"  value="<?php echo $user['state']?>"> </li>
                                    <li class="reg3"> <label><?php lang("ZIP Code","Código Postal");?></label><input  type="text" name="zipcode"  value="<?php echo $user['zipcode']?>"> </li>
                                    <li class="reg2 first"> <label><?php lang("Local Number","Teléfono Local");?></label><input  type="text" name="local_number"  value="<?php echo $user['local_number']?>"> </li>
                                    <li class="reg2"> <label><?php lang("Mobile Number","Teléfono Móvil");?></label><input  type="text" name="mobile_number"  value="<?php echo $user['mobile_number']?>"> </li>
                                </ul>

                                <h2><?php lang("Transport Preference","Tipo de Embarque");?></h2>

                                <table  class="radio-list">
                                    <tr>
                                        <td><?php lang("Full Container Load (FCL)","Contenedor Completo (FCL)");?>  </td>
                                        <td> <input type="radio" name="transpref01" value="1" <?php echo ($user['transpref01']==1)? "checked":"" ?>><label><?php lang("Yes","Si");?></label> </td>
                                        <td> <input type="radio" name="transpref01" value="0" <?php echo ($user['transpref01']==0)? "checked":"" ?>><label><?php lang("No","No");?></label> </td>
                                    </tr>
                                    <tr>
                                        <td><?php lang("Less Container Load (LCL)","Carga Consolidada");?> </td>
                                        <td> <input type="radio" name="transpref02" value="1" <?php echo ($user['transpref02']==1)? "checked":"" ?>><label><?php lang("Yes","Si");?></label> </td>
                                        <td> <input type="radio" name="transpref02" value="0" <?php echo ($user['transpref02']==0)? "checked":"" ?>><label><?php lang("No","No");?></label> </td>
                                    </tr>
                                    <tr>
                                        <td><?php lang("Air Cargo","Carga Aérea");?>  </td>
                                        <td> <input type="radio" name="transpref03" value="1" <?php echo ($user['transpref03']==1)? "checked":"" ?>><label><?php lang("Yes","Si");?></label> </td>
                                        <td> <input type="radio" name="transpref03" value="0" <?php echo ($user['transpref03']==0)? "checked":"" ?>><label><?php lang("No","No");?></label> </td>
                                    </tr>
                                </table>


                                <h2><?php lang("Typical Sizes","Tipo de Carga");?></h2>

                                <table  class="typicalsizes">
                                    <tr>
                                        <?php
                                        $lst=["Few Boxes","Few Pallets","20","40","40HQ"];
                                        ?>
                                        <td> <input type="checkbox" name="ts_fb" value="1" <?php echo ($user['ts_fb']==1)?"checked":"" ?>><label><?php lang("Few Boxes","Cajas");?></label> </td>
                                        <td> <input type="checkbox" name="ts_fp" value="1" <?php echo ($user['ts_fp']==1)?"checked":"" ?>><label><?php lang("Few Pallets","Paletas");?></label> </td>
                                    </tr>
                                    <tr>
                                        <td> <input type="checkbox" name="ts_20" value="1" <?php echo ($user['ts_20']==1)?"checked":"" ?>><label>20</label> </td>
                                        <td> <input type="checkbox" name="ts_40" value="1" <?php echo ($user['ts_40']==1)?"checked":"" ?>><label>40</label> </td>
                                        <td> <input type="checkbox" name="ts_hq" value="1" <?php echo ($user['ts_hq']==1)?"checked":"" ?>><label>40HQ</label> </td>
                                    </tr>
                                </table>

                                <h2><?php lang("Login Information","Información de Ingreso");?></h2>

                                <ul class="register">
                                    <li class="reg1"> <label><?php lang("Email Address","Correo Electronico");?></label><input readonly type="text" name="mail" placeholder="<?php lang("Enter Email Address","Ingresa Correo Electronico");?>" value="<?php echo $user['mail']?>"> </li>
                                    <!--li class="reg2 first"> <label><?php lang("Password","Clave");?><span>*</span></label><input type="text" name="pass" placeholder="Enter contact name" > </li>
                                    <li class="reg2"> <label><?php lang("Password Confirmation","Confirmación de Clave");?></label><span>*</span><input type="text" name="pass1" placeholder="Enter number"> </li>
                                    <li class="reg2 first"> <label><?php lang("Security Question","Pregunta de Seguridad");?><span>*</span></label><input type="text" name="security_q" placeholder="Enter your address" value="<?echo $user['security_q']?>"> </li>
                                    <li class="reg2"> <label><?php lang("Securiry Answer","Respuesta de Seguridad");?></label><span>*</span><input type="text" name="security_a" placeholder="Enter your address" value="<?echo $user['security_a']?>"> </li-->
                                </ul>

                                <table class="radio-list">
                                    <tr>
                                        <td colspan="3"><?php lang("I want to receive offers from FCL (highly recommended).","Quiero recibir las ofertas de FCL (muy recomendable ) .");?>  </td>
                                        <td> <input type="radio" name="pr_01" value="1"  <?php echo ($user['pr_01']==1)?"checked":"" ?>><label><?php lang("Yes","Si");?></label> </td>
                                        <td> <input type="radio" name="pr_01" value="0"  <?php echo ($user['pr_01']==0)?"checked":"" ?>><label>No</label> </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"><?php lang("Have you ever done import / export operations?","Ha realizado operaciones de Importacion / Exportacion");?>  </td>
                                        <td> <input type="radio" name="pr_02" value="1"  <?php echo ($user['pr_02']==1)?"checked":"" ?>><label><?php lang("Yes","Si");?></label> </td>
                                        <td> <input type="radio" name="pr_02" value="0"  <?php echo ($user['pr_02']==0)?"checked":"" ?>><label>No</label> </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"><?php lang("Are you currently doing shipments?","Esta actualmente realizando embarques");?>  </td>
                                        <td> <input type="radio" name="pr_03" value="1" <?php echo ($user['pr_03']==1)?"checked":"" ?>><label><?php lang("Yes","Si");?></label> </td>
                                        <td> <input type="radio" name="pr_03" value="0" <?php echo ($user['pr_03']==0)?"checked":"" ?>><label>No</label> </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"><?php lang("Do you have a single shipment or recurring shipments?","Tiene embarques esporadicos o frecuentes");?>  </td>
                                        <td> <input type="radio" name="pr_04" value="1" <?php echo ($user['pr_04']==1)?"checked":"" ?>><label><?php lang("Yes","Si");?></label> </td>
                                        <td> <input type="radio" name="pr_04" value="0" <?php echo ($user['pr_04']==0)?"checked":"" ?>><label>No</label> </td>
                                    </tr>
                                </table>


                                <h2><?php lang("Current frequency of shipments","Embarques frecuentes");?></h2>

                                <table  class="radio-list">
                                    <tr>
                                        <td><?php lang("LESS THAN 5 SHIPMENTS","MENOS DE 5 EMBARQUES");?>  </td>
                                        <td> <input type="radio" name="frqship" value="11" <?php echo ($user['frqship']=='11')?"checked":"" ?>><label><?php lang("Day","Dia");?></label> </td>
                                        <td> <input type="radio" name="frqship" value="12" <?php echo ($user['frqship']=='12')?"checked":"" ?>><label><?php lang("Week","Semana");?></label> </td>
                                        <td> <input type="radio" name="frqship" value="13" <?php echo ($user['frqship']=='13')?"checked":"" ?>><label><?php lang("Month","Mes");?></label> </td>
                                        <td> <input type="radio" name="frqship" value="14" <?php echo ($user['frqship']=='14')?"checked":"" ?>><label><?php lang("Year","Año");?></label> </td>
                                    </tr>
                                    <tr>
                                        <td><?php lang("5 TO 20 SHIPMENTS","DE 5 A 20 EMBARQUES");?>  </td>
                                        <td> <input type="radio" name="frqship" value="21" <?php echo ($user['frqship']=='21')?"checked":"" ?>><label><?php lang("Day","Dia");?></label> </td>
                                        <td> <input type="radio" name="frqship" value="22" <?php echo ($user['frqship']=='22')?"checked":"" ?>><label><?php lang("Week","Semana");?></label> </td>
                                        <td> <input type="radio" name="frqship" value="23" <?php echo ($user['frqship']=='23')?"checked":"" ?>><label><?php lang("Month","Mes");?></label> </td>
                                        <td> <input type="radio" name="frqship" value="24" <?php echo ($user['frqship']=='24')?"checked":"" ?>><label><?php lang("Year","Año");?></label> </td>
                                    </tr>
                                    <tr>
                                        <td><?php lang("21 TO 50 SHIPMENTS","DE 21 A 50 EMBARQUES");?>  </td>
                                        <td> <input type="radio" name="frqship" value="31" <?php echo ($user['frqship']=='31')?"checked":"" ?>><label><?php lang("Day","Dia");?></label> </td>
                                        <td> <input type="radio" name="frqship" value="32" <?php echo ($user['frqship']=='32')?"checked":"" ?>><label><?php lang("Week","Semana");?></label> </td>
                                        <td> <input type="radio" name="frqship" value="33" <?php echo ($user['frqship']=='33')?"checked":"" ?>><label><?php lang("Month","Mes");?></label> </td>
                                        <td> <input type="radio" name="frqship" value="34" <?php echo ($user['frqship']=='34')?"checked":"" ?>><label><?php lang("Year","Año");?></label> </td>
                                    </tr>
                                    <tr>
                                        <td><?php lang("MORE THAN 51 SHIPMENTS","MÁS DE 51 EMBARQUES");?>  </td>
                                        <td> <input type="radio" name="frqship" value="41" <?php echo ($user['frqship']=='41')?"checked":"" ?>><label><?php lang("Day","Dia");?></label> </td>
                                        <td> <input type="radio" name="frqship" value="42" <?php echo ($user['frqship']=='42')?"checked":"" ?>><label><?php lang("Week","Semana");?></label> </td>
                                        <td> <input type="radio" name="frqship" value="43" <?php echo ($user['frqship']=='43')?"checked":"" ?>><label><?php lang("Month","Mes");?></label> </td>
                                        <td> <input type="radio" name="frqship" value="44" <?php echo ($user['frqship']=='44')?"checked":"" ?>><label><?php lang("Year","Año");?></label> </td>
                                    </tr>
                                </table>
                                <a onclick="document.profile.submit()" class="edit-saveESP"><?php lang("Save","Guardar");?></a>
                            </form>
                        </section>
                    
                    </div>
                </section>
                
            </div>
        </div>


<!-- =================== FOOTER  ====================== -->   

<?php
    require_once '../include/footer.php';
?>