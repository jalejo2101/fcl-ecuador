<?php session_start();?>
<?php include_once("../admin/Consultas.php");?>
<?php include_once("../admin/funciones.php");?>

<?php
    require_once '../include/header.php';
    $con=new Consultas();
    $lista=$con->get_country_list();
    if(isset($_POST["tipo_flete"])) {

        $tipof = ($_POST["tipo_flete"]);
        $tipot = ($_POST["tipo_tr"]);
        $pais_o = ($_POST["p_o"]);
        $pais_d = ($_POST["p_d"]);
        $servicios = ($_POST["servicios"]); 
        
        $_SESSION["tipo_flete"]=$tipof;
        $_SESSION["tipo_tr"]=$tipot;
        $_SESSION["p_o"]=$pais_o;
        $_SESSION["p_d"]=$pais_d;
        $_SESSION["servicios"]=$servicios;


    }
    elseif(isset($_GET["back"])){
        $tipof = $_SESSION["tipo_flete"];
        $tipot = $_SESSION["tipo_tr"];
        $pais_o = $_SESSION["p_o"];
        $pais_d = $_SESSION["p_d"];
        $servicios = $_SESSION["servicios"];
    }
    else{?>
        <script>window.location.href="cotiza_tipo.php"</script>
    <?php } ?>

    <?php

    $tit=($tipof=="ex")?langVar("Export","Exportacion"):langVar("Import","Importacion");
    $tit.=($tipof=="ae")?langVar(" / Air"," / Aerea"):langVar(" / Sea"," / Maritima");

    //if($tipof=="im"){
        $lista1=$con->get_port_list($pais_o, $tipot);
        $lista2=$con->get_port_list($pais_d, $tipot);
        $p1=$pais_o;
        $p2=$pais_d;


    /*}else if($tipof=="ex"){
        $lista1=$con->get_port_list($pais_d);
        $lista2=$con->get_port_list($pais_o);
        $p1=$pais_d;
        $p2=$pais_o;
    }else{

    }*/
    ?>



<script>
</script>
<!-- =================== CONTENIDO  =================== -->
        <div id="content">
            <div id="main-content-full" class="noaliados">
                <section class="tracking-box" id="flete">
                    <h1><?php echo($tit) ?></h1>
                    <form name="fr" method="post" action="">
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 50%">
                                <h2><?php lang("Origin Port","Puerto de Origen")?></h2><br>
                                <h2><?php echo $p1?></h2>
                                <select name="puerto_o" id="puerto_o">
                                    <?php foreach ($lista1 as $port){ ?>
                                            <option value="<?php echo($port["nombre"]) ?>"><?php echo($port["nombre"]) ?></option>
                                    <?php } ?>
                                </select>
                            </td>
                            <td style="width: 50%">
                                <h2><?php lang("Destination Port","Puerto de Destino")?></h2><br>
                                <h2><?php echo $p2?></h2>
                                <select name="puerto_d" id="puerto_d">
                                    <?php foreach ($lista2 as $port){ ?>
                                        <option value="<?php echo($port["nombre"]) ?>"><?php echo($port["nombre"]) ?></option>
                                    <?php } ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 50%"> <a class="<?php lang("reg-back","reg-back")?>" style="position: relative; float: left" href="cotiza_tipo.php?back=1"><?php lang("Back","Atras" )?>  </a> </td>
                            <td style="width: 50%"> <a id="mostrar" class="reg-show" style="position: relative; float: right"><?php lang("Show","Mostrar" )?>  </a> </td>
                        </tr>
                        </table>
                    </form>

                   <div id="lst_proveedores">

                   </div>

                </section>


            </div>
        </div>
<script>

    $(function(){
        $("#proveedores").val("ECUADOR");
        $("#mostrar").click(function(){
            $.ajax({
                method:"GET",
                url: "lista_tarifas.php",
                data: {puerto_o:$("#puerto_o").val(),puerto_d:$("#puerto_d").val()}
            }).done(function( html ) {
                    $( "#lst_proveedores" ).empty().append( html );
            });
        });
    });







    function validar(){
        var op1=document.fr.tipo_flete.value;
        var op2=document.fr.tipo_tr.value;
        var b=true;
        if(op1=="") {
            b=false;
        }
        if(op2=="") {
            b=false;
        }

        if(b){
            document.fr.submit();
        }else{
            $("#msg").html("*Debe seleccionar el tipo de flete y el tipo de transporte");
        }
    }
</script>

<!-- =================== FOOTER  ====================== -->   


<?php
    require_once '../include/footer.php';
?>
