<?php session_start();?>
<?php
if($_SESSION["idioma"]==""){
    $_SESSION["idioma"]="esp";
}
if(isset($_POST["idioma"])){
    $_SESSION["idioma"]=$_POST["idioma"];
}
/*
if(isset($_GET["back"])){

        $tipof = $_SESSION["tipo_flete"];
        $tipot = $_SESSION["tipo_tr"]=$tipot;
        $pais_o = $_SESSION["p_o"]=$pais_o;
        $pais_d = $_SESSION["p_d"]=$pais_d;
        $servicios = $_SESSION["servicios"]=$servicios; 
}
*/
$tipof=$_SESSION["tipo_flete"];

?>

<?php include_once("../admin/Consultas.php");?>
<?php include_once("../admin/funciones.php");?>

<?php
    require_once '../include/header.php';
    $con=new Consultas();
    $lista=$con->get_country_list();
    $lista_s=$con->get_sevices_list();



?>
<script>
</script>
<!-- =================== CONTENIDO  =================== -->
        <div id="content">
            <div id="main-content-full" class="noaliados">
                <section class="tracking-box" id="flete">
                    <h1><?php lang("GET YOUR INSTANT FREIGHT","OBTENGA SU COTIZACION EN LINEA")?></h1>
                    <form name="fr" method="post" action="cotiza_ruta.php">
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 50%">
                                <h2> <?php lang("Shipping Type","Tipo de Envio")?> </h2>
                                <div class="radio-list" style="margin-bottom: 20px">
                                    <input type="radio" name="tipo_flete" id="tf1" value="im" class="css-checkbox"> <label for="tf1" class="css-label"><?php lang("Import","Importación" )?></label><br> <br>
                                    <input type="radio" name="tipo_flete" id="tf2" value="ex" class="css-checkbox"> <label for="tf2" class="css-label"><?php lang("Export","Exportación" )?></label> <br>
                                    <!-- <input type="radio" name="tipo_flete" value="ot" style="margin-right: 10px"><h2> <?php //lang("Other","Otro" )?></h2><br> -->
                                </div>
                            </td>
                            <td>
                                <h2><?php lang("Transport Type","Tipo de Transporte" )?></h2>
                                <div class="radio-list">
                                   
                                    <input type="radio" name="tipo_tr" id="tt1" value="Aeropuerto" class="css-checkbox"> <label for="tt1" class="css-label"><?php lang("Air","Aereo" )?></label><br> <br>
                                    <input type="radio" name="tipo_tr" id="tt2" value="Puerto" class="css-checkbox"> <label for="tt2" class="css-label"><?php lang("Sea","Maritimo" )?></label><br>
           
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 50%">
                                <h2><?php lang("Origin","Pais de Origen" )?></h2>
                            </td>
                            <td>
                                <select id="pais_o">
                                    <?php foreach($lista as $p){?>
                                        <option value="<?php echo($p["pais"])?>"><?php echo($p["pais"])?></option>
                                    <?php } ?>
                                </select>
                                <input type="hidden" name="p_o" id="p_o">
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 50%">
                                <h2><?php lang("Destination","Pais de Destino" )?></h2>
                            </td>
                            <td>
                                <select id="pais_d">
                                    <?php foreach($lista as $p){?>
                                        <option value="<?php echo($p["pais"])?>"><?php echo($p["pais"])?></option>
                                    <?php } ?>
                                </select>
                                <input type="hidden" name="p_d" id="p_d">
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 50%">

                                <h2><?php lang("Service Type","Tipo de Servicio" )?></h2>
                            </td>
                            <td>

                                <select name="servicios" id="servicios">
                                    <?php
                                    $c=0;
                                    foreach($lista_s as $s){?>
                                        <option value="<?php echo $c++ ?>"><?php lang(explode(';',$s["servicio"])[1],explode(';',$s["servicio"])[0])?></option>
                                    <?php } ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 50%; color: red; vertical-align: top"><div id="msg"></div></td>
                            <td style="width: 50%">
                                <a onclick="validar()" class="<?php lang("reg-next","reg-nextESP")?>" style="position: relative; float: left"><?php lang("Next","Siguiente" )?>  </a>
                            </td>
                        </tr>
                        </table>



                        <section class="reg-step01" style="margin-top: 35px" id="form">


                        </section>

                    </form>

                </section>




            </div>
        </div>
<script>
    $(function(){
       $("#pais_o").val("ECUADOR");
       $("#pais_d").val("ESTADOS UNIDOS");

        $("input[name=tipo_flete]").click(function () {
            if($(this).val()=='im'){
                $("#pais_o").val("ESTADOS UNIDOS");
                $("#pais_d").val("ECUADOR");
                $("#pais_d").attr("disabled", true);
                $("#pais_o").attr("disabled", false);
                $("#p_o").val($("#pais_o").val());
                $("#p_d").val($("#pais_d").val());

            }
            if($(this).val()=='ex'){
                $("#pais_o").val("ECUADOR");
                $("#pais_d").val("ESTADOS UNIDOS");
                $("#pais_o").attr("disabled", true);
                $("#pais_d").attr("disabled", false);
                $("#p_o").val($("#pais_o").val());
                $("#p_d").val($("#pais_d").val());
            }
            if($(this).val()=='ot'){
                $("#pais_o").attr("disabled", false);
                $("#pais_d").attr("disabled", false);
                $("#p_o").val($("#pais_o").val());
                $("#p_d").val($("#pais_d").val());
            }
        });
    });

    function validar(){
        var op1=document.fr.tipo_flete.value;
        var op2=document.fr.tipo_tr.value;
        $("#p_o").val($("#pais_o").val());
        $("#p_d").val($("#pais_d").val());

        var b=true;
        if(op1=="") {
            b=false;
        }
        if(op2=="") {
            b=false;
        }

        if(b) {
            //document.fr.submit();
            var srv = $("#servicios").val();
            if (srv == 2) url = "form_menaje_consolidado.php";
            if (srv == 3) url = "form_menaje_casa.php";
            if (srv == 4) url = "form_menaje_cv.php";
            if (srv == 5) url = "form_carga_pesada.php";
            if (srv == 6) url = "form_carga_peligrosa.php";
            if (srv == 7) url = "form_carga_refrigerada.php";
            if (srv >= 0 && srv <= 1){
                document.fr.submit();
            }


            if (srv >= 2 && srv <= 7) {
                $.ajax({
                    url: url,
                    cache: false
                })
                    .done(function (html) {
                        $("#form").empty().append(html);
                    });
            }
        }else{
            $("#msg").html("<?php lang("*Select the type of freight and the transport type","*Debe seleccionar el tipo de flete y el tipo de transporte")?>");
        }



    }
</script>

<!-- =================== FOOTER  ====================== -->   


<?php
    require_once '../include/footer.php';
?>
