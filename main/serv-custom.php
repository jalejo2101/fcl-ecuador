<?php include_once("../admin/Consultas.php");?>
<?php include_once("../admin/funciones.php");?>
<?php
require_once '../include/header.php';
$lst_s=$con->get_lst_Banner_Small_activo();

$c=0;
foreach($lst_s as $item){
    $lst_url_s[$c] = $item["url"];
    $lst_img_s[$c] = $item["imagen"];
    $c++;
}

?>
<script>
    $(document).ready(function() {
        $('nav#menu-top ul.main-sect > li:nth-child(4) > a').addClass("page-on");
    });
</script>
<!-- =================== CONTENIDO  =================== -->         

        <div id="content">

            

            <?php
                require_once '../include/aside.php';
            ?>

            <div id="main-content" class="aliados">

                <nav>

                    <ul>

                        <li ><a <?php if($_SESSION["idioma"]=="esp"){echo "style='padding: 16px 1px 0px 16px;' ";} ?>><?php lang("Sea Freight","Carga Maritima");?></a></li>

                        <li ><a <?php if($_SESSION["idioma"]=="esp"){echo "style='padding: 16px 1px 0px 16px;' ";} ?>><?php lang("Air cargo","Carga Aérea");?></a></li>

                        <li ><a <?php if($_SESSION["idioma"]=="esp"){echo "style='padding: 16px 1px 0px 16px;' ";} ?>><?php lang("Pick up & Delivery","Recogida y Entrega");?></a></li>

                        <li ><a <?php if($_SESSION["idioma"]=="esp"){echo "style='padding: 16px 1px 0px 16px;' ";} ?>><?php lang("Customs","Aduanas");?></a></li>

                    </ul>

                </nav>

                

                <div class="grey-box">
					
                    <h2><b><?php lang("CUSTOMS BROKERAGE","SERVICIOS DE ADUANA ");?></b></h2>		
			
                    <p><?php lang(
                        "We can take care of all the customs formalities that your cargo needs to be shipped and to be released from customs. All goods to be import or export need to comply with customs formalities that have to be done by an authorized customs broker, since it involves Payment of Taxes and Duties.",
                        "Podemos hacernos cargo de todas las formalidades aduaneras que su carga necesita para su envío y retiro de  Aduana. Todos los bienes sean de importación o exportación deben cumplir con las formalidades aduaneras que debe realizarlo  un agente de aduanas autorizado, ya que implica el pago de impuestos y derechos.");?>	</p>

                    <h3><b><?php lang(
                        "Documents requested in Ecuador for import customs clearance:",
                        "Documentos solicitados en Ecuador para el despacho de aduana de carga de importación: ");?></b></h3>
                    <ul class="lists">
                        <li><?php lang("Commercial Invoice.","Factura comercial. ");?></li>		
                        <li><?php lang("Packing list.","Lista de embalaje. ");?></li>	
                        <li><?php lang(
                        "For some goods: ISO Certificate, Origin certificate, Phytosanitary Certificate",
                        "Para algunos bienes: Certificado de Normalización INEN, Certificado de Origen, Certificado Fitosanitarios.");?></li>

                    </ul>
                </div>
                <a href="index.php" class="banner-content"><img  src="../img/<?php lang("banner-calcbig.png","banner-calcbigESP.png");?>"></a>


                <div id="logos">
                    <div class="viewport">
                        <ul class="overview">
                            <?php $c=1; ?>
                            <?php for($i=1; $i<=count($lst_url_s); $i++){ ?>
                                    <?php if($i==1) echo "<li>\n" ?>
                                    <a href="<?php echo $lst_url_s[$i-1] ?>"><img src="../img/banner_small/<?php echo $lst_img_s[$i-1] ?>"></a>
                                    <?php if($i%3==0) echo "</li><li>\n" ?>
                                    <?php
                                        if($i==count($lst_url_s)){
                                        echo "</li>\n" ;
                                    }?>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                
                <script type="text/javascript">
                        $(document).ready(function(){
                            $("#logos").tinycarousel({
                                    bullets  : true, interval  : true
                            });
                        });
                </script>
                
            </div>
        </div>


<!-- =================== FOOTER  ====================== -->   

<?php
    require_once '../include/footer.php';
?>