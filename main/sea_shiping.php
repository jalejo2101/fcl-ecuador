<?php include_once("../admin/Consultas.php");?>
<?php include_once("../admin/funciones.php");?>

<?php
    require_once '../include/header.php';
    $con=new Consultas();
    $bl_number=$_GET["bl_number"];
    $track=$con->get_sea_tracking($bl_number);
    $lst_d=$con->get_sea_tracking_detail($bl_number);
?>
<script>
</script>
<!-- =================== CONTENIDO  =================== -->
        <div id="content">
            <div id="main-content-full" class="noaliados">
                <section class="tracking-box">
                    <h1>GET YOUR INSTANT FREIGHT / OBTENGA SU COTIZACION EN LINEA</h1>
                    <h2>Sea Tracking / Maritimo</h2>
                    <?php if(isset($_SESSION['id'])){?>
                        <ul class="profile-nav">
                            <li><a><img src="../img/icon-prof-on.png"><p>MY PROFILE</p></a></li>
                            <li><a href="shipment.php"><img src="../img/icon-ship-off.png"><p>SHIPMENT</p></a></li>
                            <li><a href="chgpass.php"><img src="../img/icon-pass-off.png"><p>CHANGE PASSWORD</p></a></li>
                        </ul>
                    <?php } ?>


                    <table border="1" id="track_head">
                        <tr>
                            <td style="width: 50%">BL NUMBER / BL NUMERO</td>
                            <td><?php echo $track["bl_number"] ?></td>
                        </tr>
                        <tr>
                            <td style="width: 50%">Container / Contenedor</td>
                            <td><?php echo $track["type"] ?></td>
                        </tr>
                        <tr>
                            <td style="width: 50%">Origin / Origen</td>
                            <td><?php echo $track["origen"] ?></td>
                        </tr>                        
                        <tr>
                            <td style="width: 50%">Destination / Destino</td>
                            <td><?php echo $track["destino"] ?></td>
                        </tr>
                       
                        <tr>
                            <td>ETA / ESTIMADO DE ARRIBO</td>
                            <td><?php echo $track["eta"] ?></td>
                        </tr>
                    </table>

                    <table border="1" id="sea_track_detail" class="table">
                        <thead>
                            <tr>
                                <td style="width: 20%">LOCATION <br>UBICACIÓN</td>
                                <td style="width: 30%">DESCRIPTION<br>DESCRIPCION</td>
                                <td style="width: 13%">DATE<br>FECHA</td>
                                <td style="width: 24%">VESSEL<br>NAVE</td>
                                <td style="width: 13%">VOYAGE<br>VIAJE</td>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach($lst_d as $item){ ?>
                            <tr>
                                <td><?php echo $item["location"]?></td>
                                <td><?php echo $item["description"]?></td>
                                <td><?php echo $item["date"]?></td>
                                <td><?php echo $item["vessel"]?></td>
                                <td><?php echo $item["voyage"]?></td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                    <img class="print_button" src="../img/print.png">
                </section>
            </div>
        </div>
<script>


</script>

<!-- =================== FOOTER  ====================== -->   


<?php
    require_once '../include/footer.php';
?>
