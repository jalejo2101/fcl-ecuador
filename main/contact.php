
<?php include_once("../admin/Consultas.php");?>
<?php include_once("../admin/funciones.php");?>
<?php

require_once '../include/header.php';

if($_POST['mail']!=null){

    agrega_contacto();
    //Header("Location: contact.php");
    
}?>



    <script>
        $(document).ready(function() {
            //$('nav#menu-top ul.main-sect > li:nth-child(6) a').addClass("page-on");
        });
    </script>
    <script>
        function validar(){
            err=false;
            pag=0;
            //expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

            /*if(document.fr.client_name.value==""){
                err=true;
                var m='<?php lang("Please enter your Name.","Introduzca su Nombre.")?>'
                msg=m;
                pag=1;
            }else if(document.fr.mail.value==""){
                err=true;
                var m='<?php lang("Please enter the Email.","Por favor ingrese el Correo Electrónico.")?>'
                msg=m;
            }else if(!expr.test(document.fr.mail.value)){
                err=true;
                var m='<?php lang("The format of the mail is incorrect.","El formato del correo electrónico es incorrecto.")?>'
                msg=m;
            }*/

/*
            if(err==true && pag==1){
                ControlRegister('.reg-step01','.reg-step03');
                alert(msg);
            }else if(err==true && pag==0){
                alert(msg);
            }else{
*/
                var r=validateCaptcha();

                //alert(r);

                if(r){
                    document.contact_us.submit()
              }

     //       }*/


        }
    </script>
    <!-- =================== CONTENIDO  =================== -->

    <div id="content">

        <?php
        require_once '../include/aside.php';
        ?>

        <div id="main-content" class="noaliados">

            <div class="white-box-small">

                <h2><?php lang("CONTACT US","CONTACTENOS")?></h2>

                <form id="contact-us" method="post" name="contact_us" onSubmit="return validateCaptcha()">
                    <h4><?php lang("Company Name","Empresa")?></h4>
                    <input type="text" name="company_name" value="">
                    <h4><?php lang("Name","Nombre")?> <span>*</span></h4>
                    <input type="text" id="client_name" name="client_name" required="">
                    <h4><?php lang("Email","Correo")?> <span>*</span></h4>
                    <input type="email" name="mail" value="">
                    <h4><?php lang("Phone","Telefono")?></h4>
                    <input type="tel" name="phone" value="">
                    <h4><?php lang("Subject","Asunto")?></h4>
                    <select name="subject">
                        <option value="<?php lang("Rate inquiry","Solicitud de Tarifas")?>"><?php lang("Rate inquiry","Solicitud de Tarifas")?></option>
                        <option value="<?php lang("Agency inquiry","Relación de Agentes")?>"><?php lang("Agency inquiry","Relación de Agentes")?></option>
                        <option value="<?php lang("Schedule","Itinerarios")?>"><?php lang("Schedule","Itinerarios")?></option>
                        <option value="<?php lang("Others","Otros")?>"><?php lang("Others","Otros")?></option>
                    </select>
                    <h4><?php lang("Message","Mensaje")?><span>*</span></h4>
                    <textarea placeholder="<?php lang("Enter your message here...","Introduzca su mensaje aqui...")?>" name="message"></textarea>

                    <div style="margin: 0 auto;">
                        <div class="g-recaptcha" data-sitekey="6LeePRoTAAAAAP3Y7R5ht631Jwczr8poIXEZmtkB"></div>

                        <?php/*
                        require_once('../Captcha/recaptchalib.php');

                        $publickey = "6LfVSf0SAAAAAJyu32QqlC5SO9-cGV9hZL98WAdG"; // you got this from the signup page

                        //echo recaptcha_get_html($publickey);*/
                        ?>
                        <p><?php // echo recaptcha_get_html($publickey); ?></p>
                        <p style="color: red;" id="captchaStatus">&nbsp;</p>



                    </div>

                    <!--a onclick="validar()" class="<?php lang("edit-save","edit-saveESP")?>" id="enviar"><?php lang("SUBMIT","ENVIAR")?> </a-->


                    <input type="submit" id="contacto" value="<?php lang("SUBMIT","ENVIAR")?>" >

                </form>

                <aside class="mini-aside">
                    <h6><?php lang("Call Us","Contactenos")?></h6>
                    <ul>
                        <li><?php lang("Office","Oficina")?>: 593-4-6015076</li>
                        <li><?php lang("Mobile","Movil")?>: 593-992836034</li>
                        <li>AOH: 593-992836034</li>
                    </ul>
                </aside>
                <img class="map" src="../img/map.png">
            </div>



        </div>
    </div>


    <!-- =================== FOOTER  ====================== -->
    <script type="text/javascript">
        function validateCaptcha()
        {
            challengeField = $("input#recaptcha_challenge_field").val();
            responseField = $("input#recaptcha_response_field").val();
            //alert(challengeField);
            //alert(responseField);
            //return false;
            var html = $.ajax({
                type: "POST",
                url: "recapcha.php",
                data: "recaptcha_challenge_field=" + challengeField + "&recaptcha_response_field=" + responseField,
                async: false
            }).responseText;

            if(html == "success")
            {
                $("#captchaStatus").html(" ");

                // Uncomment the following line in your application
                return true;
            }
            else
            {
                $("#captchaStatus").html("Your captcha is incorrect. Please try again");

                Recaptcha.reload();
                return false;
            }
        }

    </script>
<?php
require_once '../include/footer.php';
?>