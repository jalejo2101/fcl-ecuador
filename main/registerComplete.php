<?php include_once("../admin/Consultas.php");?>
<?php include_once("../admin/funciones.php");?>

<?php

    require_once '../include/header.php';    
    ?>



<!-- =================== CONTENIDO  =================== -->         

        <div id="content">
            <div id="main-content-full" class="noaliados">
                <section class="tracking-box">
                    <h1><?php lang("REGISTER","REGISTRO" )?> </h1>
                    
                    <div id="register-panel">
                        <section style="display:block">
                                <ul class="register">
                                    <li class="reg1 first"> <label><?php lang("Your registration was successful.","Su inscripción se ha realizado correctamente." )?> </label>
                                    <li class="reg1 first"> <label><?php lang("Check your email to reach an email with a link that will allow you to access your account.","Verifique su correo electrónico,  al mismo llegara un  link que le permitirá validar su cuenta para que pueda hacer uso de ella." )?> </label>
                                </ul>
                        </section>
                    </div>   

                    
                </section>
            </div>
        </div>

<script>


</script>

<!-- =================== FOOTER  ====================== -->   


<?php
    require_once '../include/footer.php';
?>
