<?php session_start();?>
<?php include_once("../admin/Consultas.php");?>
<?php include_once("../admin/funciones.php");?>
<?php
header("Content-Type: text/html;charset=utf-8");
if($_SESSION["idioma"]==""){
    $_SESSION["idioma"]="esp";
}
if(isset($_POST["idioma"])){
    $_SESSION["idioma"]=$_POST["idioma"];
}

$tipot=$_SESSION["tipo_tr"];
$tipof=$_SESSION["tipo_flete"];
$id_ruta=$_GET["id_r"];
$id_pr=$_GET["id_p"];

$con= new Consultas();
//$lst_salidas= $con->get_salidas_proveedor_ruta($id_salida);


?>




<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <link href="../css/jquery-ui.css" type="text/css" rel="stylesheet">
    <link href="../css/reset.css" type="text/css" rel="stylesheet">
    <link href="../css/layout.css" type="text/css" rel="stylesheet">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

    <!-- JSs -->

    <script src="../js/jquery-1.9.1.js"></script>
    <script src="../js/jquery.tinycarousel.js"></script>
    <script src="../js/jquery-ui.js"></script>

    <title>Freightlogistics</title>

    <link rel="shortcut icon" type="image/png" href="../img/favicon.png">
</head>

<body>


<script>
    $(document).ready(function() {

    });
</script>
<!-- =================== CONTENIDO  =================== -->         

    <div id="content" style="width: 700px">
    <section id="middle"></section>
    <?php if(!isset($_POST['next'])){?>

        <form id="cotizacion" method="post" action="">
            <!--************************* Contenedores *************************-->

            <section class="panel">
            <table id="containers">
                <tr>
                    <th colspan="4" class="head_panel" style="text-align: center">
                        <?php lang("CHOOSE QUANTITY OF CONTAINERS","ELIJA LA CANTIDAD DE CONTENEDORES A EMBARCAR")?>
                    </th>
                </tr>
                <tr>
                    <th style="width: 25%">20 STD</th>
                    <th style="width: 25%">40 STD</th>
                    <th style="width: 25%">40 HC</th>
                    <th style="width: 25%">40 NOR</th>
                </tr>
                <tr>
                    <td style="text-align: center">
                        <select name="20_std">
                            <?php for($i=0; $i<=99; $i++){
                                echo "<option value='$i'>$i</option>";
                            } ?>
                        </select>
                    </td>
                    <td style="text-align: center">
                        <select name="40_std">
                            <?php for($i=0; $i<=99; $i++){
                                echo "<option value='$i'>$i</option>";
                            } ?>
                        </select>
                    </td>
                    <td style="text-align: center">
                        <select name="40_hc">
                            <?php for($i=0; $i<=99; $i++){
                                echo "<option value='$i'>$i</option>";
                            } ?>
                        </select>
                    </td>
                    <td style="text-align: center">
                        <select name="40_nor">
                            <?php for($i=0; $i<=99; $i++){
                                echo "<option value='$i'>$i</option>";
                            } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" style="border-bottom: solid #00389D 1px; height: 10px"></td>
                </tr>
            </table>
            <table>
                <tr>
                    <th style="text-align: left;" class="celda_desc">
                        <?php lang("FREIGHT PAYMENT","PAGO DE FLETE")?>
                    </th>
                    <td style="text-align: left">
                        <select name="freight_pay" class="">
                            <option value="<?php lang('Prepaid','Prepagado')?>"><?php lang('Prepaid','Prepagado')?></option>
                            <option value="<?php lang('Collet','Por cobrar')?>"><?php lang('Collet','Por cobrar')?></option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th style="text-align: left;" class="celda_desc">
                        <?php lang("SHIPPPING CONDITIONS","CONDICIONES DE EMBARQUE")?>
                    </th>
                    <td style="text-align: left">
                        <input type="text" name="incomterm" value="<?php lang("PORT/PORT","PUERTO/PUERTO")?>" readonly>
                    </td>
                </tr>
                <!--/table>
            </section>
            <!--************************* Servicios *************************-->
            <!--section class="panel">
                <table>
                <tr>
                    <th class="head_panel" colspan="2" style="text-align: center"><?php lang("ADDITIONAL SERVICES","SERVICIOS ADICIONALES")?></th>
                </tr-->
                <tr>
                    <th style="text-align: left;" class="celda_desc">

                            <?php lang("TRANSPORTATION INSURANCE","SEGURO DE TRANSPORTE")?>

                    </th>
                    <td style="text-align: left">
                        <input id="op1" type="radio" name="seg_t" value="yes"><label  style="margin-right:40px">Yes</label>
                        <input id="op2" type="radio" name="seg_t" value="no" checked><label>No</label>
                    </td>
                </tr>
                <tr id="seguro" style="display:none">
                    <th style="text-align: left;" class="celda_desc">
                        <?php lang("DECLARED VALUE","VALOR DECLARADO")?>
                    </th>
                    <td style="text-align: left">
                        <input type="text" name="dec_value" style="width: 100px; float: left; margin-right: 10px">
                        <select name="moneda_seg" style="width: 60px; float: left">
                            <option value="DOL">DOL</option>
                            <option value="EUR">EUR</option>
                        </select>
                        <a href=""><label style="line-height: 23px; margin-left: 10px; cursor: pointer; color: #2a6496">
                                <?php lang("Insurance conditions","Condiciones del seguro")?></label>
                        </a>
                    </td>
                </tr>
                <tr>
                    <th style="text-align: left;" class="celda_desc">
                        <?php if($tipof=="im"){
                            lang("DELIVERY SERVICES","ENTREGA DE CARGA");
                        }else{
                            lang("PICK UP SERVICES","RECOGIDA");
                        } ?>
                    </th>
                    <td style="text-align: left">
                        <?php if($tipof=="im"){?>
                        <select name="entrega" style=" float: left" id="entrega">
                            <option value="yes">YES</option> <!--***************** I WILL TAKE GOODS  MYSELF *****************-->
                            <option value="no">NO</option> <!--***************** I WILL NEED INLAND SERVICES *****************-->
                        </select>
                        <?php }else{ ?>
                            <select name="recogida" style=" float: left" id="recogida">
                                <option value="yes">YES</option>
                                <option value="no">NO</option>
                            </select>
                        <?php } ?>
                    </td>
                </tr>
            </table>
            </section>
            <!--************************* Origen *************************-->
            <section class="panel" id="panel_1" style="display:<?php echo ($tipof=="im")?"none":"block"?>">
                <table>
                    <tr>
                        <th class="head_panel" style="text-align: center"><?php lang("ORIGIN CITY","CIUDAD DE ORIGEN")?></th>
                    </tr>
                </table>

                <table>

                    <tr>
                        <th style="text-align: left;" class="celda_desc">
                            <?php lang("CITY","CIUDAD")?>
                        </th>
                        <td style="text-align: left">
                            <select name="city_r" style="width: 220px; float: left">
                                <?php
                                $lst_prv=$con->get_ciudades();
                                foreach($lst_prv as $p){ ?>
                                    <option value="<?php echo $p ?>"><?php echo $p ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <th style="text-align: left;" class="celda_desc">
                            <?php lang("PICK UP ADDRESS","DIRECCION DE RECOGIDA")?>
                        </th>
                        <td style="text-align: left">
                            <input type="text" name="address_r" <?php //echo ($tipof!="im")?'required=""':''?>>
                        </td>
                    </tr>
                    <tr>
                        <th style="text-align: left;" class="celda_desc">
                            <?php lang("ZIP CODE","CODIGO DE AREA")?>
                        </th>
                        <td style="text-align: left">
                            <input type="text" name="zip_r" <?php //echo ($tipof!="im")?'required=""':''?>>
                        </td>
                    </tr>
                    <tr>
                        <th style="text-align: left;" class="celda_desc">
                            <?php lang("PERSON IN CHARGE","PERSONA RESPONSABLE DE LA ENTREGA")?>
                        </th>
                        <td style="text-align: left">
                            <input type="text" name="person_r" <?php //echo ($tipof!="im")?'required=""':''?>>
                        </td>
                    </tr>
                    <tr>
                        <th style="text-align: left;" class="celda_desc">
                            <?php lang("PHONE NUMBER","TELEFONO")?>
                        </th>
                        <td style="text-align: left">
                            <input type="text" name="telf_r"  <?php //echo ($tipof!="im")?'required=""':''?>>
                        </td>
                    </tr>
                    <tr>
                        <th style="text-align: left;" class="celda_desc">
                            <?php lang("TYPE OF CONTAINER","TIPO DE CONTENEDOR ")?>
                        </th>
                        <td style="text-align: left">
                            <select name="tipo_cnt" style="width: 60px; float: left">
                                <option value="20">20</option>
                                <option value="40">40</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th style="text-align: left;" class="celda_desc">
                            <?php lang("WEIGHT","PESO")?> (Kgs)
                        </th>
                        <td style="text-align: left">
                            <input type="text" name="peso_r"  <?php //echo ($tipof!="im")?'required=""':''?>>
                        </td>
                    </tr>
                    <tr>
                        <th style="text-align: left;" class="celda_desc">
                            <?php lang("PICK UP","RECOGIDA ")?>
                        </th>
                        <td style="text-align: left">
                            <?php lang("DATE","FECHA")?> <input type="text" name="fecha_r" style="width: 127px; margin: 0 10px;">
                            <?php lang("TIME","HORA")?> <input type="text" name="hora_r" style="width: 127px; margin-left: 10px">
                        </td>
                    </tr>
                </table>
            </section>
            <!--************************* Destino *************************-->
            <section class="panel" id="panel_2" style="display:<?php echo ($tipof=="ex")?"none":"block"?>">
                <table>
                    <tr>
                        <th class="head_panel" style="text-align: center"><?php lang("DESTINATION CITY","CIUDAD DE DESTINO")?></th>
                    </tr>
                </table>
                <section id="direcciones">
                    <table>
                        <!--tr>
                            <th style="text-align: left; background-color:#808080; color: #FFffFF; border-radius: 15px;height: 10px" class="celda_desc">
                                <?php lang("ADDRESS #1","DIRECCION #1")?>
                            </th>
                            <td style="text-align: left">
                            </td>
                        </tr-->

                        <tr>
                            <th style="text-align: left;" class="celda_desc">
                                <?php lang("CITY","CIUDAD")?>
                            </th>
                            <td style="text-align: left">
                                <select name="city_d" style="width: 220px; float: left">
                                    <?php
                                    $lst_prv=$con->get_ciudades();
                                    foreach($lst_prv as $p){ ?>
                                        <option value="<?php echo $p ?>"><?php echo $p ?></option>
                                    <?php } ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th style="text-align: left;" class="celda_desc">
                                <?php lang("DELIVERY ADDRESS","DIRECCION DE ENTREGA")?>
                            </th>
                            <td style="text-align: left">
                                <input type="text" name="address_d"  <?php //echo ($tipof=="im")?'required=""':''?>>
                            </td>
                        </tr>
                        <tr>
                            <th style="text-align: left;" class="celda_desc">
                                <?php lang("ZIP CODE","CODIGO DE AEREA")?>
                            </th>
                            <td style="text-align: left">
                                <input type="text" name="zip_d" <?php //echo ($tipof=="im")?'required=""':''?>>
                            </td>
                        </tr>
                        <tr>
                            <th style="text-align: left;" class="celda_desc">
                                <?php lang("PERSON IN CHARGE","PERSONA RESPONSABLE DE LA RECEPCION")?>
                            </th>
                            <td style="text-align: left">
                                <input type="text" name="person_d" <?php //echo ($tipof=="im")?'required=""':''?>>
                            </td>
                        </tr>
                        <tr>
                            <th style="text-align: left;" class="celda_desc">
                                <?php lang("PHONE NUMBER","TELEFONO")?>
                            </th>
                            <td style="text-align: left">
                                <input type="text" name="telf_d" <?php //echo ($tipof=="im")?'required=""':''?>>
                            </td>
                        </tr>
                    </table>
                </section>




                <!--section>
                    <table>
                    <tr>
                        <th style="text-align: left;" class="celda_desc">

                        </th>
                        <td style="text-align: left">
                            <label id="add_address" style="line-height: 23px; margin-left: 10px; cursor: pointer; color: #2a6496">
                                <?php lang("Add another address","Direccion adicional")?></label>

                        </td>
                    </tr>
                    </table>
                </section-->

            </section>

            <!--section class="panel" id="panel_2">
                <table>
                    <tr>
                        <th class="head_panel" style="text-align: center"><?php lang("SHIPPING DATE ","DIA DE ENTREGA")?></th>
                    </tr>
                </table>
                <section id="direcciones">
                    <table>
                        <tr>
                            <th style="text-align: left;" class="celda_desc">
                                <?php lang("SHIPPING DATE ","DIA DE ENTREGA")?>
                            </th>
                            <td style="text-align: left">

                            </td>
                        </tr>
                    </table>
                </section>
            </section-->


            <section class="panel" id="panel_2">
                <table>
                    <tr>
                        <th class="head_panel" style="text-align: center"><?php lang("ADDITIONAL SERVICES","SERVICIOS ADICIONALES")?></th>
                    </tr>
                </table>
                <section id="direcciones">
                    <table>
                        <!--tr>
                            <th style="text-align: left;" class="celda_desc">
                                <?php lang("CUSTOMS CLEARANCE","DESPACHO DE ADUANA")?>
                            </th>
                            <td style="text-align: left">
                                <input type="text" name="despacho" required="">
                            </td>
                        </tr-->
                        <tr>
                            <th style="text-align: left;" class="celda_desc">
                                <?php lang("CUSTOMS BROKERAGE","TRAMITES DE ADUANA")?>
                            </th>
                            <td style="text-align: left">
                                <select name="tramites_aduana" style=" float: left" id="tramites_aduana">
                                    <option value="yes">YES</option>
                                    <option value="no">NO</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th style="text-align: left;" class="celda_desc">
                                <?php lang("ISSUANCE OF BL IN DESTINATION","EMISION DE BL EN DESTINO")?>
                            </th>
                            <td style="text-align: left">
                                <select name="bl_destino" style=" float: left" id="bl_destino">
                                    <option value="yes">YES</option>
                                    <option value="no">NO</option>
                                </select>
                            </td>
                        </tr>



                        <tr>
                            <th style="text-align: left;" class="celda_desc">
                                <?php lang("IMPORTER/EXPORTER REGISTRATION","REGISTRO DE IMPORTADOR - EXPORTADOR")?>
                            </th>
                            <td style="text-align: left">
                                <input type="text" name="importer" >
                            </td>
                        </tr>
                        <tr>
                            <th style="text-align: left;" class="celda_desc">
                                <?php lang("INNEN CERTIFICATE","CERTIFICADO INEN")?>
                            </th>
                            <td style="text-align: left">
                                <input type="text" name="innen" >
                            </td>
                        </tr>
                        <tr>
                            <th style="text-align: left;" class="celda_desc">
                                <?php lang("MIPRO REGISTRATION","REGISTRO MIPRO")?>
                            </th>
                            <td style="text-align: left">
                                <input type="text" name="mipro" >
                            </td>
                        </tr>
                        <tr>
                            <th style="text-align: left;" class="celda_desc">
                                <?php lang("ORIGIN CERTIFICATE","CERTIFICADO DE ORIGEN")?>
                            </th>
                            <td style="text-align: left">
                                <input type="text" name="origin" >
                            </td>
                        </tr>
                        <tr>
                            <th style="text-align: left;" class="celda_desc">
                                <?php lang("PHITOSANITARY CERTIFICATE","CERTIFICADO FITOSANITARIO")?>
                            </th>
                            <td style="text-align: left">
                                <input type="text" name="certificate" >
                            </td>
                        </tr>
                    </table>
                </section>

            </section>
            <div style="margin: auto; width: 250px">
                <a href="cotiza_ruta.php?back=1"> <button onclick="" name="back" class="btnSig" style="text-align: center; margin-right: 2%;"><?php lang("Back","Atras" )?></button> </a>
                <button onclick="" name="next" class="btnSig" style="text-align: center; margin-left: 2%;"><?php lang("Next","Siguiente" )?></button> 
            </div>

            <!---------------------------------------------->
        </form>



    <!------------------------------------------------------->
    <?php }else{ ?>
        <?php

        $_20_std=$_POST['20_std'];
        $_40_std=$_POST['40_std'];
        $_40_hc=$_POST['40_hc'];
        $_40_nor=$_POST['40_nor'];
        $freight_pay=$_POST['freight_pay'];
        $moneda_seg=$_POST['moneda_seg'];
        $incomterm=$_POST['incomterm'];
        $seg_t=$_POST['seg_t'];
        $dec_value=$_POST['dec_value'];
        $entrega=$_POST['entrega'];
        $city_r=$_POST['city_r'];
        $address_r=$_POST['address_r'];
        $zip_r=$_POST['zip_r'];
        $person_r=$_POST['person_r'];
        $telf_r=$_POST['telf_r'];
        $tipo_cnt=$_POST['tipo_cnt'];
        $peso_r=$_POST['peso_r'];
        $fecha_r=$_POST['fecha_r'];
        $hora_r=$_POST['hora_r'];
        $city_d=$_POST['city_d'];
        $address_d=$_POST['address_d'];
        $zip_d=$_POST['zip_d'];
        $person_d=$_POST['person_d'];
        $telf_d=$_POST['telf_d'];
        $tramites_aduana=$_POST['tramites_aduana'];
        $bl_destino=$_POST['bl_destino'];
        $importer=$_POST['importer'];
        $innen=$_POST['innen'];
        $mipro=$_POST['mipro'];
        $origin=$_POST['origin'];
        $certificate=$_POST['certificate'];
        $n_cont=$_20_std+$_40_std+$_40_hc+$_40_nor;

        $tarifas=$con->get_tarifas_por_ruta_prov($id_ruta,$id_pr)[0];
        ?>


        <section class="panel">
            <table>
                <tr>
                    <th colspan="4" class="head_panel" style="text-align: center; text-transform: uppercase">
                        <?php lang("Quotation Summary","Resumen de la cotizacion")?>
                    </th>
                </tr>
                <tr>
                    <th style="text-align: left;" class="celda_desc">
                        <?php lang("PORT OF LOADING","PUERTO DE ORIGEN")?>
                    </th>
                    <td style="text-align: left">
                        <?php echo $_SESSION['puerto_o']?>
                    </td>
                </tr>
                <tr>
                    <th style="text-align: left;" class="celda_desc">
                        <?php lang("PORT OF LOADING","PUERTO DE DESTINO")?>
                    </th>
                    <td style="text-align: left">
                        <?php echo $_SESSION['puerto_d']?>
                    </td>
                </tr>
                <tr>
                    <th style="text-align: left;" class="celda_desc">
                        <?php lang("CONTAINERS","CONTENDORES")?>
                    </th>
                    <td style="text-align: left">
                        <?php echo $n_cont?>
                    </td>
                </tr>
                <tr>
                    <th style="text-align: left;" class="celda_desc">
                        <?php lang("INSURANCE","SEGURO")?>
                    </th>
                    <td style="text-align: left">
                        <?php echo $seg_t?>
                    </td>
                </tr>
                <tr>
                    <th style="text-align: left;" class="celda_desc">
                        <?php lang("SHIPPING DATE","FECHA DE EMBARQUE");?>
                    </th>
                    <td style="text-align: left">
                        <select name="fecha_salida" style="width: 220px; float: left">
                            <?php
                            try{
                                $lst=explode(";",$tarifas['fecha_base']);
                                foreach($lst as $dia){
                                    $timezone = new DateTimeZone('America/Lima');
                                    $d=explode(":",$dia)[0];
                                    $f=explode(":",$dia)[1];

                                    $d=substr($d,6,4)."-".substr($d,3,2)."-".substr($d,0,2);

                                    $fecha1 = new DateTime($d);
                                    $fecha2 = new DateTime();
                                    $fecha2->add(new DateInterval('P7D'));

                                    do{
                                        $fecha1 = $fecha1->add(new DateInterval('P'.$f.'D'));
                                    }while($fecha1 < $fecha2);
                                    ?>
                                    <option value="<?php echo $fecha1->format('d-m-Y') ?>"><?php echo $fecha1->format('d-m-Y') ?></option>
                                <?php }
                            }catch(Exception $e){ } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th style="text-align: left;" class="celda_desc">
                        <?php lang("TRANSIT TIME","TIEMPO DE TRANSITO")?>
                    </th>
                    <td style="text-align: left">
                        <?php echo $tarifas['transit']?>
                    </td>
                </tr>
                <tr>
                    <th style="text-align: left;" class="celda_desc">
                        <?php lang("CURRENCY","DIVISA")?>
                    </th>
                    <td style="text-align: left">
                        <?php echo $tarifas['currecny']?>
                    </td>
                </tr>
                <tr>
                    <th style="text-align: left;" class="celda_desc">
                        <?php lang("FREIGHT  PAYMENT TERMS","CONDICION DE  PAGO DE FLETE")?>
                    </th>
                    <td style="text-align: left">
                        <?php echo $freight_pay ?>
                    </td>
                </tr>
                <?php if($_20_std>0){?>
                <tr>
                    <th style="text-align: left;" class="celda_desc">
                        <?php lang("Basic Ocean Freight 20","Flete Basico 20")?>
                    </th>
                    <td style="text-align: left">
                        <?php echo "<div class='tc_lb'>[STD]</div><div class='tc_cal'>".$_20_std." X ".num_f($tarifas['precio_20'])."</div><div class= 'tc_tot'>= ".num_f($_20_std*$tarifas['precio_20'])."</div>" ?>
                    </td>
                </tr>
                <?php } ?>
                <tr>
                    <th style="text-align:left; vertical-align: top; padding-top: 4px;" class="celda_desc">
                        <?php lang("Basic Ocean Freight 40 (STD/HC/NOR)","Flete Basico 40 (STD/HC/NOR)")?>
                    </th>
                    <td style="text-align: left">
                        <table class="tc">
                            <?php if($_40_std>0){?>
                            <tr>
                                <td>
                                    <?php echo "<div class='tc_lb'>[STD]</div><div class='tc_cal'>".$_40_std." X ".num_f($tarifas['precio_40'])."</div><div class= 'tc_tot'>= ".num_f($_40_std*$tarifas['precio_40'])."</div>" ?>
                                </td>
                            </tr>
                            <?php } ?>
                            <?php if($_40_hc>0){?>
                            <tr>
                                <td>
                                    <?php echo "<div class='tc_lb'>[HC] </div><div class='tc_cal'>".$_40_hc." X ".num_f($tarifas['precio_hr'])."</div><div class= 'tc_tot'>= ".num_f($_40_hc*$tarifas['precio_hr'])."</div>" ?>
                                </td>
                            </tr>
                            <?php } ?>
                            <?php if($_40_nor>0){?>
                            <tr>
                                <td>
                                    <?php echo "<div class='tc_lb'>[NOR]</div><div class='tc_cal'>".$_40_nor." X ".num_f($tarifas['precio_nor'])."</div><div class= 'tc_tot'>= ".num_f($_40_nor*$tarifas['precio_nor'])."</div>" ?>
                                </td>
                            </tr>
                            <?php } ?>
                            <tr>
                                <td>
                                    <div class='tc_lb' style="width:150px; margin-left: 0px; margin-right: 0px; padding-top: 3px">TOTAL (STD/HC/NOR)</div>
                                    <div class='tc_tot' style="border-top: solid #101010 1px; padding-top: 3px">=
                                    <?php
                                    $t=($_40_std*$tarifas['precio_40'])+($_40_hc*$tarifas['precio_hr'])+($_40_nor*$tarifas['precio_nor']);
                                    echo num_f($t);
                                    ?>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <th style="text-align: left;" class="celda_desc">
                        <?php lang("BSC","BSC")?>
                    </th>
                    <td style="text-align: left">
                        <?php
                        $t=($_20_std*$tarifas['bsc_20'])+($_40_std*$tarifas['bsc_40'])+($_40_hc*$tarifas['bsc_hc'])+($_40_nor*$tarifas['bsc_nor'])*$n_cont;
                        echo num_f($t);
                        ?>
                    </td>
                </tr>

                <tr>
                    <th style="text-align: left;" class="celda_desc">
                        <?php lang("ISP","ISP")?>
                    </th>
                    <td style="text-align: left">
                        <?php
                        $t=($_20_std*$tarifas['isp_20'])+($_40_std*$tarifas['isp_40'])+($_40_hc*$tarifas['isp_hc'])+($_40_nor*$tarifas['isp_nor'])*$n_cont;
                        echo num_f($t)
                        ?>
                    </td>
                </tr>

                <tr>
                    <th style="text-align: left;" class="celda_desc">
                        <?php lang("THO","THO")?>
                    </th>
                    <td style="text-align: left">
                        <?php
                        $t=($_20_std*$tarifas['tho_20'])+($_40_std*$tarifas['tho_40'])+($_40_hc*$tarifas['tho_hc'])+($_40_nor*$tarifas['tho_nor'])*$n_cont;
                        echo num_f($t);
                        ?>
                    </td>
                </tr>


                <tr>
                    <th style="text-align: left;" class="celda_desc">
                        <?php lang("OTHER SUB-CHARGES","Otros Recargos")?>
                    </th>
                    <td style="text-align: left">
                        <?php echo num_f($tarifas['other_charges']*$n_cont)?>
                    </td>
                </tr>

                <tr>
                    <th style="text-align: left;" class="celda_desc">
                        <?php lang("BL FEE ","BL FEE ")?>
                    </th>
                    <td style="text-align: left">
                        <?php echo num_f($tarifas['bl_fee']*$n_cont)?>
                    </td>
                </tr>
                <tr>
                    <th style="text-align: left;" class="celda_desc">
                        <?php lang("THCD","THCD")?>
                    </th>
                    <td style="text-align: left">
                        <?php echo num_f($tarifas['thcd']*$n_cont)?>
                    </td>
                </tr>

                <?php if($tipof=='im'){?>
                <tr>
                    <th style="text-align: left;" class="celda_desc">
                        <?php lang("IMPORT SERVICES","SERVICIO DE IMPORTACION")?>
                    </th>
                    <td style="text-align: left">
                        <?php echo num_f($tarifas['import_services']*$n_cont)?>
                    </td>
                </tr>
                <?php }else{?>
                <tr>
                    <th style="text-align: left;" class="celda_desc">
                        <?php lang("EXPORT SERVICE","SERVICIO DE EXPORTACION")?>
                    </th>
                    <td style="text-align: left">
                        <?php echo num_f($tarifas['export_services']*$n_cont)?>
                    </td>
                </tr>
                <?php } ?>

                <tr>
                    <th style="text-align: left;" class="celda_desc">
                        <?php lang("D O","D O")?>
                    </th>
                    <td style="text-align: left">
                        <?php echo num_f($tarifas['d_o']*$n_cont)?>
                    </td>
                </tr>




            </table>
        </section>
    <?php } ?>


    <!------------------------------------------------------->



    <?php //echo ($seg_t=='yes')? lang("YES: ","SI: ").$freight_pay:"NO" ?>



    </div>


<!-- =================== FOOTER  ====================== -->


<script type="text/javascript">
    var n=2;
    $(document).ready(function(){
        $("#op1").click(function(){
            $("#seguro").css({"display":"table-row"});
        });
        $("#op2").click(function(){
            $("#seguro").css({"display":"none"});
        });

       /* $("#ec1").click(function(){
            $("#entrega").css({"display":"table-row"});
        });
        $("#ec2").click(function(){
            $("#entrega").css({"display":"none"});
        });*/


        $("#add_address").click(function(){
            $("#direcciones")
                .append('<table>' +
                '<tr>'+
                '<th class="celda_n">'+
                '<?php lang("ADDRESS ","DIRECCION ")?>#'+ (n++) +
                '</th>'+
                '<td>&nbsp;</td>'+
                '</tr>'+
                '</table>').append($("#addr").html());
        });

        $("#entrega").change(function(){
            if ($("#entrega").val()=="no"){
                $("#panel_2").css({"display":"none"})
            }else{
                $("#panel_2").css({"display":"block"})
            }
        });
        $("#recogida").change(function(){
            if ($("#recogida").val()=="no"){
                $("#panel_1").css({"display":"none"})
            }else{
                $("#panel_1").css({"display":"block"})
            }
        });

    });
</script>


</body>
</html>