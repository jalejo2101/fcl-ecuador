<?php include_once("../admin/Consultas.php");?>
<?php include_once("../admin/funciones.php");?>
<?php
require_once '../include/header.php';
$lst_s=$con->get_lst_Banner_Small_activo();

$c=0;
foreach($lst_s as $item){
    $lst_url_s[$c] = $item["url"];
    $lst_img_s[$c] = $item["imagen"];
    $c++;
}

?>

<script>

    $(document).ready(function() {

        $('nav#menu-top ul.main-sect > li:nth-child(3) a').addClass("page-on");

    });

</script>

<!-- =================== CONTENIDO  =================== -->         



        <div id="content">



            <?php

                require_once '../include/aside.php';

            ?>





            <div id="main-content" class="aliados">



                <div class="white-box-small">

					

                    <h2><?php lang("ABOUT US","QUIENES SOMOS")?></h2>						

				    <h3><?php lang("Welcome to F.C.L.","Bienvenido a F.C.L.")?></h3>

                    <img src="../img/about-img.jpg">

                        <p><b>F.C.L.</b>

                            <?php lang(

                                  "acts as a sales agent for a number of worldwide licensed NVOCC or Freight Forwarders and gives customers the ability to get instant sea & air freight quotes online. We provide in Origin and Destination comprehensive services for international sea & air freight transportation:  Pickup, Delivery, Door to Door, Port to Port.",

                                  "actúa como agente de ventas para una serie de  Mayoristas Internacionales  de Fletes Marítimos y Aéreos, los mismo que cuentan con  licencia NVOCC - IATA,  pudiendo de esta forma ofrecer a nuestros clientes  la capacidad de obtener sus cotizaciones instantáneas a través de una tecnología de punta."

                            )?>                            

                        </p><p>

                            <?php lang(

                                  "",

                                  "Los servicios integrados en Origen y Destino para el  transporte marítimo y aéreo de mercancías van desde la  recolección de carga en la fábrica del vendedor hasta la entrega en puerta de nuestros clientes. Nuestra tecnología nos permite informar directamente en nuestra pagina web cada movimiento que realiza su carga."

                            )?>                            

                        </p>



                        <p><b>F.C.L.</b> 

                             <?php lang(

                                  "was founded in Guayaquil, Ecuador in 2007 as an innovating sales agent, we have used  the most update technology to deliver a fast and suitable system that allow user to choose the best option for their cargo transportation in less than 2 minutes.",

                                  "fue fundada en Guayaquil, Ecuador en 2007 como un agente de ventas innovador en el sector Marítimo; hoy en día contamos con las herramientas actualizadas  para ofrecer un sistema rápido y adecuado que permite al usuario elegir en nuestra Página WEB la mejor opción para el  transporte de  su carga en menos de 2 minutos."

                            )?>                            

                        </p>



                        <p><b>F.C.L</b> 

                             <?php lang(

                                  "has strong NVOCC´s and Freight Forwarders companies, retrieving in the portal air and ocean rates in seconds. All services offered are backed by a highly skilled team of professionals.",

                                  "<b>mantiene una alimentación continua de tarifa e información en su Calculadora de Fletes en Línea</b>, para que nuestros clientes pueda en minutos elegir la tarifa más conveniente. Nuestra información  está respaldada por un equipo de profesionales altamente calificados en Transportacion Internacional. "

                            )?>                            

                        </p>



                        <p><b>F.C.L</b> 

                             <?php lang(

                                  "instant freight calculator is suitable for businesses or individuals requiring long-term & short-term or one-off importing, exporting transportation services.",

                                  " y su  calculadora de carga instantánea es adecuada para las empresas o personas que requieren a largo  y corto plazo  o por una  sola vez,  los servicios de transporte, aduana, logística para sus  Importaciones o Exportaciones."

                            )?>                            

                        </p>



                        <p>

                            <?php lang(

                                  "Our goal is to be the first international freight selling Company in Ecuador and South America, helping small to mid-sized shipper & consignee by giving them world-class service to support their growing commerce activities.",

                                  "Nuestro objetivo es ser la primera empresa de soluciones en Logistica y Transporte de carga dentro de Ecuador y en  América del Sur, ayudar a pequeños, medianos embarcadores y consignatarios, dándoles servicio de primera clase para apoyar el crecimiento de sus actividades en el área de Comercio Exterior. "

                            )?>                            

                        </p>

                        

                        <p>                          

                            <a href="register.php"><b><?php lang("Signing up with F.C.L is totally free.","Registrarse con F.C.L es totalmente gratuito .")?></b></a>

                        </p>



                </div>

                <div id="logos">
                    <div class="viewport">
                        <ul class="overview">
                            <?php $c=1; ?>
                            <?php for($i=1; $i<=count($lst_url_s); $i++){ ?>
                                    <?php if($i==1) echo "<li>\n" ?>
                                    <a href="<?php echo $lst_url_s[$i-1] ?>"><img src="../img/banner_small/<?php echo $lst_img_s[$i-1] ?>"></a>
                                    <?php if($i%3==0) echo "</li><li>\n" ?>
                                    <?php
                                        if($i==count($lst_url_s)){
                                        echo "</li>\n" ;
                                    }?>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                
                <script type="text/javascript">
                        $(document).ready(function(){
                            $("#logos").tinycarousel({
                                    bullets  : true, interval  : true
                            });
                        });
                </script>

                

            </div>

        </div>





<!-- =================== FOOTER  ====================== -->   



<?php

    require_once '../include/footer.php';

?>