<?php session_start();?>
<?php include_once("../admin/Consultas.php");?>
<?php include_once("../admin/funciones.php");?>
<?php
header("Content-Type: text/html;charset=utf-8");
if($_SESSION["idioma"]==""){
    $_SESSION["idioma"]="esp";
}
if(isset($_POST["idioma"])){
    $_SESSION["idioma"]=$_POST["idioma"];
}

$tipot=$_SESSION["tipo_tr"];
$tipof=$_SESSION["tipo_flete"];  // [im]portacion [ex]portacion
$id_ruta=$_GET["id_r"];
$id_pr=$_GET["id_p"];

$con= new Consultas();
//$lst_salidas= $con->get_salidas_proveedor_ruta($id_salida);

//var_dump($_SESSION);
?>




<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <link href="../css/jquery-ui.css" type="text/css" rel="stylesheet">
    <link href="../css/reset.css" type="text/css" rel="stylesheet">
    <link href="../css/layout.css" type="text/css" rel="stylesheet">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

    <!-- JSs -->

    <script src="../js/jquery-1.9.1.js"></script>
    <script src="../js/jquery.tinycarousel.js"></script>
    <script src="../js/jquery-ui.js"></script>

    <title>Freightlogistics</title>

    <link rel="shortcut icon" type="image/png" href="../img/favicon.png">
</head>

<body>


<script>
    $(document).ready(function() {

    });
</script>
<!-- =================== CONTENIDO  =================== -->         

    <div id="content" style="width: 700px">
    <section id="middle"></section>
    <?php if(!isset($_POST['next'])){?>

        <form id="cotizacion" method="post" action="">
            <!--************************* Contenedores *************************-->
            
            <section class="panel">
            
            <header> <?php lang("CHOOSE QUANTITY OF CONTAINERS","ELIJA LA CANTIDAD DE CONTENEDORES A EMBARCAR")?> </header>
            
            <table id="containers">
                
                <tr>
                    <th style="width: 25%">20 STD</th>
                    <th style="width: 25%">40 STD</th>
                    <th style="width: 25%">40 HC</th>
                    <th style="width: 25%">40 NOR</th>
                </tr>
                <tr>
                    <td style="text-align: center">
                        <select name="20_std">
                            <?php for($i=0; $i<=20; $i++){
                                echo "<option value='$i'>$i</option>";
                            } ?>
                        </select>
                    </td>
                    <td style="text-align: center">
                        <select name="40_std">
                            <?php for($i=0; $i<=20; $i++){
                                echo "<option value='$i'>$i</option>";
                            } ?>
                        </select>
                    </td>
                    <td style="text-align: center">
                        <select name="40_hc">
                            <?php for($i=0; $i<=20; $i++){
                                echo "<option value='$i'>$i</option>";
                            } ?>
                        </select>
                    </td>
                    <td style="text-align: center">
                        <select name="40_nor">
                            <?php for($i=0; $i<=20; $i++){
                                echo "<option value='$i'>$i</option>";
                            } ?>
                        </select>
                    </td>
                </tr>
    
            </table>
            <table>
                <tr>
                    <th style="text-align: left;" class="celda_desc">
                        <?php lang("FREIGHT PAYMENT","PAGO DE FLETE")?>
                    </th>
                    <td style="text-align: left">
                        <select name="freight_pay" class="">
                            <option value="<?php lang('Prepaid','Prepagado')?>"><?php lang('Prepaid','Prepagado')?></option>
                            <option value="<?php lang('Collet','Por cobrar')?>"><?php lang('Collet','Por cobrar')?></option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th style="text-align: left;" class="celda_desc">
                        <?php lang("SHIPPPING CONDITIONS","CONDICIONES DE EMBARQUE")?>
                    </th>
                    <td style="text-align: left">
                        <input type="text" name="incomterm" value="<?php lang("PORT/PORT","PUERTO/PUERTO")?>" readonly>
                    </td>
                </tr>
                <tr>
                    <th style="text-align: left;" class="celda_desc">
                        <?php lang("INCOTERM","INCOTERM")?>
                    </th>
                    <td style="text-align: left">
                        <select name="freight_pay" class="">
                            <option value="fob"><?php lang('FOB','FOB')?></option>
                            <option value="cif"><?php lang('CIF','CIF')?></option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th style="text-align: left;" class="celda_desc">

                            <?php lang("TRANSPORTATION INSURANCE","SEGURO DE TRANSPORTE")?>

                    </th>
                    <td style="text-align: left">
                        <select name="freight_pay" id="freight_pay">
                            <option value="yes"><?php lang('Yes','Si')?></option>
                            <option value="no" selected><?php lang('No','No')?></option>
                        </select>
                    </td>
                    <!--td style="text-align: left">
                        <input id="op1" type="radio" name="seg_t" value="yes"><label  style="margin-right:40px">Yes</label>
                        <input id="op2" type="radio" name="seg_t" value="no" checked><label>No</label>
                    </td-->
                </tr>
                <tr id="seguro" style="display:none">
                    <th style="text-align: left;" class="celda_desc">
                        <?php lang("DECLARED VALUE","VALOR DECLARADO")?>
                    </th>
                    <td style="text-align: left">
                        <input type="text" name="dec_value" style="width: 100px; float: left; margin-right: 10px">
                        <select name="moneda_seg" style="width: 60px; float: left">
                            <option value="DOL">DOL</option>
                            <option value="EUR">EUR</option>
                        </select>
                        <a href=""><label style="line-height: 23px; margin-left: 10px; cursor: pointer; color: #2a6496">
                                <?php lang("Insurance conditions","Condiciones del seguro")?></label>
                        </a>
                    </td>
                </tr>
                <tr>
                    <th style="text-align: left;" class="celda_desc">
                        <?php if($tipof=="im"){
                            lang("DELIVERY SERVICES","ENTREGA DE CARGA");
                        }else{
                            lang("PICK UP SERVICES","RECOGIDA");
                        } ?>
                    </th>
                    <td style="text-align: left" class="lastcell">
                        <?php if($tipof=="im"){?>
                        <select name="entrega" style=" float: left" id="entrega">
                            <option value="yes"><?php lang('Yes','Si')?></option> <!--***************** I WILL TAKE GOODS  MYSELF *****************-->
                            <option value="no"><?php lang('No','No')?></option> <!--***************** I WILL NEED INLAND SERVICES *****************-->
                        </select>
                        <?php }else{ ?>
                            <select name="recogida" style=" float: left" id="recogida">
                                <option value="yes"><?php lang('Yes','Si')?></option>
                                <option value="no"><?php lang('No','No')?></option>
                            </select>
                        <?php } ?>
                    </td>
                </tr>
            </table>
            </section>
            <!--************************* Origen *************************-->
            <section class="panel" id="panel_1" style="display:<?php echo ($tipof=="im")?"none":"block"?>">



                <header> <?php ($tipof=="im")?lang("ORIGIN CITY","CIUDAD DE ORIGEN"):lang("ENTREGA","DELIVERY")?></header>

                <table>

                    <tr>
                        <th style="text-align: left">
                            <?php lang("CITY","CIUDAD")?>
                        </th>
                        <td style="text-align: left">
                            <select name="city_r">
                                <?php
                                $lst_prv=$con->get_ciudades();
                                foreach($lst_prv as $p){ ?>
                                    <option value="<?php //echo $p['id'] ?>"><?php echo $p['ciudad']?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <th style="text-align: left;" class="celda_desc">
                            <?php lang("PICK UP ADDRESS","DIRECCION DE RECOGIDA")?>
                        </th>
                        <td style="text-align: left">
                            <input type="text" name="address_r" <?php //echo ($tipof!="im")?'required=""':''?>>
                        </td>
                    </tr>
                    <tr>
                        <th style="text-align: left;" class="celda_desc">
                            <?php lang("ZIP CODE","CODIGO DE AREA")?>
                        </th>
                        <td style="text-align: left">
                            <input type="text" name="zip_r" <?php //echo ($tipof!="im")?'required=""':''?>>
                        </td>
                    </tr>
                    <tr>
                        <th style="text-align: left;" class="celda_desc">
                            <?php lang("PERSON IN CHARGE","PERSONA RESPONSABLE DE LA ENTREGA")?>
                        </th>
                        <td style="text-align: left">
                            <input type="text" name="person_r" <?php //echo ($tipof!="im")?'required=""':''?>>
                        </td>
                    </tr>
                    <tr>
                        <th style="text-align: left;" class="celda_desc">
                            <?php lang("PHONE NUMBER","TELEFONO")?>
                        </th>
                        <td style="text-align: left">
                            <input type="text" name="telf_r"  <?php //echo ($tipof!="im")?'required=""':''?>>
                        </td>
                    </tr>
                    <!-- Pesos de contenedores -->

                    <tr>
                        <th style="text-align: left;" class="celda_desc">
                            <?php lang("Cont de 20 con menos de 15 TON","Cont de 20 con menos de 15 TON")?>
                        </th>
                        <td style="text-align: left">
                            <select name="cont20" class="cont_peso">
                                <?php
                                for($i=0; $i<=40; $i++){ ?>
                                    <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th style="text-align: left;" class="celda_desc">
                            <?php lang("Cont de 20 con mas de 15 TON","Cont de 20 con mas de 15 TON")?>
                        </th>
                        <td style="text-align: left">
                            <select name="cont20_15" class="cont_peso">
                                <?php
                                for($i=0; $i<=40; $i++){ ?>
                                    <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th style="text-align: left;" class="celda_desc">
                            <?php lang("Cont de 20 con mas de 25 TON","Cont de 20 con mas de 25 TON")?>
                        </th>
                        <td style="text-align: left">
                            <select name="cont20_25" class="cont_peso">
                                <?php
                                for($i=0; $i<=40; $i++){ ?>
                                    <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th style="text-align: left;" class="celda_desc">
                            <?php lang("Cont de 40 con menos de 15 TON","Cont de 40 con menos de 15 TON")?>
                        </th>
                        <td style="text-align: left">
                            <select name="cont40" class="cont_peso">
                                <?php
                                for($i=0; $i<=40; $i++){ ?>
                                    <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th style="text-align: left;" class="celda_desc">
                            <?php lang("Cont de 40 con mas de 15 TON","Cont de 40 con mas de 15 TON")?>
                        </th>
                        <td style="text-align: left">
                            <select name="cont40_15" style="width: 20%; min-width: 80px; float: left">
                                <?php
                                for($i=0; $i<=40; $i++){ ?>
                                    <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th style="text-align: left" class="celda_desc">
                            <?php lang("Cont de 40 con mas de 25 TON","Cont de 40 con mas de 25 TON")?>
                        </th>
                        <td style="text-align: left" class="lastcell">
                            <select name="cont40_25" class="cont_peso">
                                <?php
                                for($i=0; $i<=40; $i++){ ?>
                                    <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>

                    <!--tr>
                        <th style="text-align: left;" class="celda_desc">
                            <?php lang("TYPE OF CONTAINER","TIPO DE CONTENEDOR ")?>
                        </th>
                        <td style="text-align: left">
                            <select name="tipo_cnt" style="width: 60px; float: left">
                                <option value="20">20</option>
                                <option value="40">40</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th style="text-align: left;" class="celda_desc">
                            <?php lang("WEIGHT","PESO")?> (Kgs)
                        </th>
                        <td style="text-align: left">
                            <input type="text" name="peso_r"  <?php //echo ($tipof!="im")?'required=""':''?>>
                        </td>
                    </tr-->
                    <tr>
                        <th style="text-align: left;" class="celda_desc">
                            <?php lang("PICK UP","RECOGIDA ")?>
                        </th>
                        <td style="text-align: left" class="lastcell">
                            <?php lang("DATE","FECHA")?> <input type="text" name="fecha_r" style="width: 127px; margin: 0 10px;">
                            <?php lang("TIME","HORA")?> <input type="text" name="hora_r" style="width: 127px; margin-left: 10px">
                        </td>
                    </tr>
                </table>
            </section>
            <!--************************* Destino *************************-->
            <section class="panel" id="panel_2" style="display:<?php echo ($tipof=="ex")?"none":"block"?>">
               
               <header> <?php lang("DESTINATION CITY","CIUDAD DE DESTINO")?> </header>
    
                <section id="direcciones">
                    <table>
                        <!--tr>
                            <th style="text-align: left; background-color:#808080; color: #FFffFF; border-radius: 15px;height: 10px" class="celda_desc">
                                <?php lang("ADDRESS #1","DIRECCION #1")?>
                            </th>
                            <td style="text-align: left">
                            </td>
                        </tr-->

                        <tr>
                            <th style="text-align: left">
                                <?php lang("CITY","CIUDAD")?>
                            </th>
                            <td style="text-align: left">
                                <select name="city_d" style="width: 60%; min-width: 200px">
                                    <?php
                                    $lst_prv=$con->get_ciudades();
                                    foreach($lst_prv as $p){ ?>
                                        <option value="<?php echo $p['id'] ?>"><?php echo $p['ciudad']?></option>
                                    <?php } ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th style="text-align: left;" class="celda_desc">
                                <?php lang("DELIVERY ADDRESS","DIRECCION DE ENTREGA")?>
                            </th>
                            <td style="text-align: left">
                                <input type="text" name="address_d"  <?php //echo ($tipof=="im")?'required=""':''?>>
                            </td>
                        </tr>
                        <tr>
                            <th style="text-align: left;" class="celda_desc">
                                <?php lang("ZIP CODE","CODIGO DE AEREA")?>
                            </th>
                            <td style="text-align: left">
                                <input type="text" name="zip_d" <?php //echo ($tipof=="im")?'required=""':''?>>
                            </td>
                        </tr>
                        <tr>
                            <th style="text-align: left;" class="celda_desc">
                                <?php lang("PERSON IN CHARGE","PERSONA RESPONSABLE DE LA RECEPCION")?>
                            </th>
                            <td style="text-align: left">
                                <input type="text" name="person_d" <?php //echo ($tipof=="im")?'required=""':''?>>
                            </td>
                        </tr>
                        <tr>
                            <th style="text-align: left;" class="celda_desc">
                                <?php lang("PHONE NUMBER","TELEFONO")?>
                            </th>
                            <td style="text-align: left" class="lastcell">
                                <input type="text" name="telf_d" <?php //echo ($tipof=="im")?'required=""':''?>>
                            </td>
                        </tr>

                        <!-- Pesos de contenedores -->

                        <tr>
                            <th style="text-align: left;" class="celda_desc">
                                <?php lang("Cont de 20 con menos de 15 TON","Cont de 20 con menos de 15 TON")?>
                            </th>
                            <td style="text-align: left">
                                <select name="cont20" class="cont_peso">
                                    <?php
                                    for($i=0; $i<=40; $i++){ ?>
                                        <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                    <?php } ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th style="text-align: left;" class="celda_desc">
                                <?php lang("Cont de 20 con mas de 15 TON","Cont de 20 con mas de 15 TON")?>
                            </th>
                            <td style="text-align: left">
                                <select name="cont20_15" class="cont_peso">
                                    <?php
                                    for($i=0; $i<=40; $i++){ ?>
                                        <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                    <?php } ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th style="text-align: left;" class="celda_desc">
                                <?php lang("Cont de 20 con mas de 25 TON","Cont de 20 con mas de 25 TON")?>
                            </th>
                            <td style="text-align: left">
                                <select name="cont20_25" class="cont_peso">
                                    <?php
                                    for($i=0; $i<=40; $i++){ ?>
                                        <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                    <?php } ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th style="text-align: left;" class="celda_desc">
                                <?php lang("Cont de 40 con menos de 15 TON","Cont de 40 con menos de 15 TON")?>
                            </th>
                            <td style="text-align: left">
                                <select name="cont40" class="cont_peso">
                                    <?php
                                    for($i=0; $i<=40; $i++){ ?>
                                        <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                    <?php } ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th style="text-align: left;" class="celda_desc">
                                <?php lang("Cont de 40 con mas de 15 TON","Cont de 40 con mas de 15 TON")?>
                            </th>
                            <td style="text-align: left">
                                <select name="cont40_15" style="width: 20%; min-width: 80px; float: left">
                                    <?php
                                    for($i=0; $i<=40; $i++){ ?>
                                        <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                    <?php } ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th style="text-align: left" class="celda_desc">
                                <?php lang("Cont de 40 con mas de 25 TON","Cont de 40 con mas de 25 TON")?>
                            </th>
                            <td style="text-align: left" class="lastcell">
                                <select name="cont40_25" class="cont_peso">
                                    <?php
                                    for($i=0; $i<=40; $i++){ ?>
                                        <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                    <?php } ?>
                                </select>
                            </td>
                        </tr>


                    </table>
                </section>




                <!--section>
                    <table>
                    <tr>
                        <th style="text-align: left;" class="celda_desc">

                        </th>
                        <td style="text-align: left">
                            <label id="add_address" style="line-height: 23px; margin-left: 10px; cursor: pointer; color: #2a6496">
                                <?php lang("Add another address","Direccion adicional")?></label>

                        </td>
                    </tr>
                    </table>
                </section-->

            </section>

            <!--section class="panel" id="panel_2">
                <table>
                    <tr>
                        <th class="head_panel" style="text-align: center"><?php lang("SHIPPING DATE ","DIA DE ENTREGA")?></th>
                    </tr>
                </table>
                <section id="direcciones">
                    <table>
                        <tr>
                            <th style="text-align: left;" class="celda_desc">
                                <?php lang("SHIPPING DATE ","DIA DE ENTREGA")?>
                            </th>
                            <td style="text-align: left">

                            </td>
                        </tr>
                    </table>
                </section>
            </section-->


            <section class="panel" id="panel_2">
               <header><?php lang("ADDITIONAL SERVICES","SERVICIOS ADICIONALES")?></header>
             
                <section id="direcciones">
                    <table>
                        <!--tr>
                            <th style="text-align: left;" class="celda_desc">
                                <?php lang("CUSTOMS CLEARANCE","DESPACHO DE ADUANA")?>
                            </th>
                            <td style="text-align: left">
                                <input type="text" name="despacho" required="">
                            </td>
                        </tr-->
                        <tr>
                            <th style="text-align: left;" class="celda_desc">
                                <?php lang("CUSTOMS BROKERAGE","TRAMITES DE ADUANA")?>
                            </th>
                            <td style="text-align: left">
                                <select name="tramites_aduana" style=" float: left" id="tramites_aduana">
                                    <option value="yes"><?php lang('Yes','Si')?></option>
                                    <option value="no" selected><?php lang('No','No')?></option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th style="text-align: left;" class="celda_desc">
                                <?php lang("ORIGIN CERTIFICATE","CERTIFICADO DE ORIGEN")?>
                            </th>
                            <td style="text-align: left">
                                <select name="origin" style=" float: left" id="origin">
                                    <option value="yes"><?php lang('Yes','Si')?></option>
                                    <option value="no" selected><?php lang('No','No')?></option>
                                </select>
                            </td>
                        </tr>

                        <?php if($tipof=="im"){?>
                        <tr>
                            <th style="text-align: left;" class="celda_desc">
                                <?php lang("ISSUANCE OF BL IN DESTINATION","EMISION DE BL EN DESTINO")?>
                            </th>
                            <td style="text-align: left">
                                <select name="bl_destino" style=" float: left" id="bl_destino">
                                    <option value="yes"><?php lang('Yes','Si')?></option>
                                    <option value="no" selected><?php lang('No','No')?></option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th style="text-align: left;" class="celda_desc">
                                <?php lang("IMPORTER/EXPORTER REGISTRATION","REGISTRO DE IMPORTADOR - EXPORTADOR")?>
                            </th>
                            <td style="text-align: left">
                                <select name="importer" style=" float: left" id="importer">
                                    <option value="yes"><?php lang('Yes','Si')?></option>
                                    <option value="no" selected><?php lang('No','No')?></option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th style="text-align: left;" class="celda_desc">
                                <?php lang("INNEN CERTIFICATE","CERTIFICADO INEN")?>
                            </th>
                            <td style="text-align: left">
                                <select name="innen" style=" float: left" id="innen">
                                    <option value="yes"><?php lang('Yes','Si')?></option>
                                    <option value="no" selected><?php lang('No','No')?></option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th style="text-align: left;" class="celda_desc">
                                <?php lang("MIPRO REGISTRATION","REGISTRO MIPRO")?>
                            </th>
                            <td style="text-align: left">
                                <select name="mipro" style=" float: left" id="mipro">
                                    <option value="yes"><?php lang('Yes','Si')?></option>
                                    <option value="no" selected><?php lang('No','No')?></option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th style="text-align: left;" class="celda_desc">
                                <?php lang("PHITOSANITARY CERTIFICATE","CERTIFICADO FITOSANITARIO")?>
                            </th>
                            <td style="text-align: left" class="lastcell">
                                <select name="certificate" style=" float: left" id="certificate">
                                    <option value="yes"><?php lang('Yes','Si')?></option>
                                    <option value="no" selected><?php lang('No','No')?></option>
                                </select>
                            </td>
                        </tr>
                        <?php } ?>
                    </table>
                </section>

            </section>
            <div style="margin: auto; width: 250px"> 
                <a class="atras_form" href="cotiza_ruta.php?back=1"> <?php lang("Back","Atras" )?> </a>
                <!-- <button onclick="atras();" name="back" class="btnSig" style="text-align: center; margin-right: 2%;"><?php lang("Back","Atras" )?></button> -->
                <button onclick="" name="next" class="btnSig" style="text-align: center; margin-left: 2%;"><?php lang("Next","Siguiente" )?></button> 
            </div>

            <!---------------------------------------------->
        </form>



    <!------------------------------------------------------->
    <?php }else{ ?>
        <?php      

        
        /**************************************************************/
        include_once ("cotiza_datos_resumen.php");
        /**************************************************************/

    }?>
    <!------------------------------------------------------->



    <?php //echo ($seg_t=='yes')? lang("YES: ","SI: ").$freight_pay:"NO" ?>



    </div>


<!-- =================== FOOTER  ====================== -->


<script type="text/javascript">
    var n=2;
    $(document).ready(function(){
        $("#op1").click(function(){
            $("#seguro").css({"display":"table-row"});
        });
        $("#op2").click(function(){
            $("#seguro").css({"display":"none"});
        });

       /* $("#ec1").click(function(){
            $("#entrega").css({"display":"table-row"});
        });
        $("#ec2").click(function(){
            $("#entrega").css({"display":"none"});
        });*/
        $("#freight_pay").change(function(){
            if ($("#freight_pay").val()=="no"){
                $("#seguro").slideUp("slow");
                //$("#seguro").css({"display":"table-row"});
            }else{
                $("#seguro").slideDown("slow");
                //$("#seguro").css({"display":"none"});
            }
        });






        $("#add_address").click(function(){
            $("#direcciones")
                .append('<table>' +
                '<tr>'+
                '<th class="celda_n">'+
                '<?php lang("ADDRESS ","DIRECCION ")?>#'+ (n++) +
                '</th>'+
                '<td>&nbsp;</td>'+
                '</tr>'+
                '</table>').append($("#addr").html());
        });

        $("#entrega").change(function(){
            if ($("#entrega").val()=="no"){
                $("#panel_2").slideUp("slow");
                //$("#panel_2").css({"display":"none"})
            }else{
                $("#panel_2").slideDown("slow");
                //$("#panel_2").css({"display":"block"})
            }
        });

        $("#recogida").change(function(){
            if ($("#recogida").val()=="no"){
                $("#panel_1").slideUp("slow");
                //$("#panel_1").css({"display":"none"})
            }else{
                $("#panel_1").slideDown("slow");
                //$("#panel_1").css({"display":"block"})
            }
        });

    });
    
    function atras(){
        window.location = '/cotiza_ruta.php?back=1';
    }
</script>


</body>
</html>