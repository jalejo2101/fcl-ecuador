<?php
session_start();
error_reporting(0);


if($_SESSION["idioma"]==""){
    $_SESSION["idioma"]="esp";

}

if(isset($_POST["idioma"])){
    $_SESSION["idioma"]=$_POST["idioma"];

}


if(isset($_GET['logoff']) && $_GET['logoff']==1){
    unset($_SESSION['user']);
    unset($_SESSION['id']);
}
?>
<?php include_once("../admin/Consultas.php");?>
<?php include_once("../admin/funciones.php");?>
<?php
$con=new Consultas();



if(isset($_POST['q_mail'])){
    if($_POST['q_mail']!=null){
       agrega_quick_contact();

       Header("Location: index.php");
           


    }
}

if(isset($_POST['log_email'])){

    $usr_mail=$_POST['log_email'];
    $usr_pass=md5($_POST['password']);

    $usr_info=$con->find_user($usr_mail,$usr_pass);
    $usr_id=$usr_info[0]['id'];
    //echo ">>>>>>>>[".count($usr_info)."] [".$usr_info[0]['mail']."] ".$usr_mail."-".$_POST['log_email'];

    if(count($usr_info)==0){
        echo "<script>alert('El Usuario no existe!...')</script>";
    }else{
        //echo "<script>alert('El Usuario existe!...')</script>";
        $_SESSION['user']=$_POST['log_email'];
        $_SESSION['id']=$usr_id;
        //echo "<<<<<<<<<<<<<". $_SESSION['user'].">";
    }
}

$lg=($_SESSION["idioma"]=="esp")?"_esp":"";
//echo "<<<<<<<<<<<<<". $_SESSION['user'].">";

$lst_b=$con->get_lst_Banner_Big_activo($lg);
$c=0;
foreach($lst_b as $item){
    $lst_url[$c] = $item["url"];
    $lst_img[$c] = $item["imagen"];
    $c++;
}
?>

<!DOCTYPE html>
<html lang="es">
<style>
    #ui-datepicker-div{
        font-size: 10px;
    }
</style>

    <head>
        <meta charset="utf-8" />
        <link href="../css/jquery-ui.css" type="text/css" rel="stylesheet">
        <link href="../css/reset.css" type="text/css" rel="stylesheet">   
        <link href="../css/layout.css" type="text/css" rel="stylesheet">

        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

        <!-- JSs -->
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <script src="../js/jquery-1.9.1.js"></script>
        <script src="../js/jquery.tinycarousel.js"></script>
        <script src="../js/jquery-ui.js"></script>

        <title>Freightlogistics</title>
        
        <link rel="shortcut icon" type="image/png" href="../img/favicon.png">
    </head>

    <body>

    <script>



    $(document).ready(function() {
        $('nav#menu-top ul.main-sect ul li a').on("mouseenter",function(){ $('nav#menu-top ul.main-sect >li:nth-child(4) > a').css("background-color","#114F82"); });
        $('nav#menu-top ul.main-sect >li:nth-child(4) > a').on("mouseenter",function(){ $('nav#menu-top ul.main-sect >li:nth-child(4) > a').css("background-color","#114F82"); });
        $('nav#menu-top ul.main-sect ul li a').on("mouseleave",function(){ $('nav#menu-top ul.main-sect >li:nth-child(4) > a').css("background-color","#216BA8"); });
        $('nav#menu-top ul.main-sect >li:nth-child(4) > a').on("mouseleave",function(){ $('nav#menu-top ul.main-sect >li:nth-child(4) > a').css("background-color","#216BA8"); });
    });

    function show(mostrar){
        $('.darkbox-cont').fadeIn();
        $(mostrar).fadeIn();
    }

    function cierra(){
        $('.darkbox-cont').fadeOut();
        $('.login-box').fadeOut();
        $('.why-box').fadeOut();
        $('.quick-box').fadeOut();
    }

    </script>
        
    <!--div class="<?php lang("LiveChat-box","LiveChat-boxEsp")?>"></div-->

    <a href="cotiza_tipo.php">
    <div class="<?php lang("Calculator-Banner","Calculator-BannerEsp")?>">
      
    </div>
    </a>

    <div class="darkbox-cont">
        <section class="login-box">
            <form name="login" method="post" action="index.php">
                <h2><?php lang("LOGIN","INGRESAR")?></h2>

                <ul>
                    <li><label><?php lang("Email","Correo")?></label><input type="email" name="log_email" placeholder="<?php lang("Email","Correo")?>"></li>
                    <li><label><?php lang("Password","Contraseña")?></label><input type="password" name="password" placeholder="Password"></li>
                    <li><input type="checkbox" name="remember" value="remember"><?php lang("Remember me","Recuerdame")?><a href="forget.php" target="_blank"><?php lang("Forgot password","Olvidó su contraseña")?>?</a></li>
                </ul>

                <div>
                    <input type="submit" value="LOGIN">
                    <input type="button" value="CANCEL" onclick="cierra();">
                </div>
            </form>
        </section>

        <section class="why-box">
            <a href="#" onclick="cierra();"></a>
            <h2><?php lang("WHY FCL?","POR QUÉ FCL?")?></h2>

            <p>
                <?php lang(
                      "<b>F.C.L instant quotation</b> was created to simplify and improve the process of pricing and booking international freight movements. <b>There is no reason that freight quotes should take hours or even days</b>, no reason that pricing should not be transparent.",
                      "<b>La cotización instantánea de F.C.L</b> fue creada para simplificar y mejorar el proceso de requerimiento de tarifas y la reserva de espacio para los embarques internacionales de mercancías.<b> No hay ninguna razón para que las cotizaciones tomen horas o incluso días </b>, Ni razón para que las tarifas no sean transparentes."
                )?>
            </p>

            <p>
                <?php lang(
                    "We work with you to find the most cost-effective solutions to your freight and logistics requirements. We bring our vast knowledge, experience, professional skills, partner carriers, freight service facilities; hardworking and honest people to your organization.",
                    "Trabajamos con usted para encontrar las soluciones más rentables para su carga y para su logística. Traemos a su empresa nuestro amplio conocimiento, experiencia, habilidades profesionales, nuestros  transportistas asociados, las facilidades para los servicio de carga, las personas trabajadoras y honestas."
                )?>

          </p>

      </section>

      <section class="quick-box">
            <h2><?php lang("QUICK CONTACT","CONTACTO RÁPIDO")?> </h2>
            <form name="quick" method="post" action="">
                <ul>
                    <li><label><?php lang("Name","Nombre")?></label><input type="text" name="q_name" placeholder="<?php lang("Name","Nombre")?>" required></li>
                    <li><label><?php lang("Email","Correo")?></label><input type="email" name="q_mail" placeholder="<?php lang("Email","Correo")?>" required></li>
                    <li><label><?php lang("Phone","Telefono")?></label><input type="tel" name="q_phone" placeholder="<?php lang("Phone","Telefono")?>" required></li>
                    <li><label><?php lang("Message","Mensaje")?></label><textarea name="q_text" placeholder="<?php lang("  Message","  Mensaje")?>" required></textarea>
                </ul>

               <div>
                    <input type="submit" value="<?php lang("SUBMIT","ENVIAR")?>">                    
                    <input type="button" value="<?php lang("CANCEL","CANCELAR")?>" onclick="cierra();">
                </div>
            </form>
        </section>

    </div>

    <div id="wrapper">



<!-- =================== HEADER  ====================== -->  


        <header id="h-main">
            <section id="top">

                <div>

                    <ul class="sociales">
                        <li><a href="https://twitter.com/FCL_ECUADOR"><img src=../img/soc-twitter.png></a><div class="tooltip">Twitter</div></li>
                        <li><a href="#"><img src=../img/soc-face.png></a><div class="tooltip">Facebook</div></li>
                        <li><a href="skype:fcl-olguita?call"><img src=../img/soc-skype.png></a><div class="tooltip">Skype</div></li>
                        <li><a href="https://www.youtube.com/channel/UCX46h8hcZTT1mYzBWuXjjqQ"><img src=../img/soc-youtube.png></a><div class="tooltip">YouTube</div></li>
                    </ul>

                    <ul class="idiomas">
                        <li><a href="#" onclick="idioma('esp')"><?php if($_SESSION["idioma"]=="esp"){echo "<img src=../img/len-esp-on.png>";}else{echo "<img src=../img/len-esp-off.png>";}?></a><div class="tooltip">Español</div></li>
                        <li><a href="#" onclick="idioma('eng')"><?php if($_SESSION["idioma"]=="eng"){echo "<img src=../img/len-ing-on.png>";}else{echo "<img src=../img/len-ing-off.png>";}?></a><div class="tooltip">English</div></li>
                    </ul>
                </div>
            </section>

            <section id="middle">
                <div class="mini-menu">
                    <a class="why" onclick="show('.why-box');"><?php lang("Why FCL?","<span style='font-size: .7em;'>POR QUÉ</span> FCL?")?></a>
                    
                    <ul class="login">
                        <li><?php lang("Welcome to Freightlogistics","Bienvenido a Freightlogistics")?></li>
                        <?php if(!isset($_SESSION['user'])){ ?>
                            <li><a href="#" onclick="show('.login-box');"><?php lang("Login","Ingresar")?></a></li>
                            <li><a href="register.php"><?php lang("Register","Registro")?></a></li>
                        <?php } else {?>
                            <li><a href="index.php?logoff=1"><?php lang("Logoff","Salir")?></a></li>
                        <?php } ?>

                    </ul>
                    <ul class="account" <?php if(!isset($_SESSION['user'])) echo "style='display:none'" ?>>
                        <li><a href="myprofile.php"><?php lang("My Account","Mi Cuenta")?></a></li>
                        <!--li><a href="#">Add to Favorites</a></li-->
                    </ul>
                </div>
            </section>
            <nav id="menu-top">
                <ul class="main-sect">
                    <li></li>
                    <li><a href="index.php"><?php lang("Home","INICIO")?></a></li>
                    <li><a href="about.php"><?php lang("About us","NOSOTROS")?></a></li>
                    <li><a href="#"><?php lang("Services","SERVICIO")?> </a>
                        <ul>
                            <li><a href="serv-inter-trans.php"><?php lang("International Transportation","TRANSPORTE INTERNACIONAL")?></a></li>
                            <li><a href="serv-pickup.php"><?php lang("Pick up / Delivery","RECOGIDA Y  ENTREGA DE CARGA")?></a></li>
                            <li><a href="serv-custom.php"><?php lang("Customs Brokerage","ADUANA")?></a></li>
                            <li><a href="serv-cargo.php"><?php lang("Cargo Insurance","SEGURO DE CARGA")?></a></li>
                        </ul>
                    </li>
                    <li><a href="howtoget.php"><?php lang("How to get your rate","COMO OBTENER SU TARIFA")?></a></li>
                    <li><a href="contact.php"><?php lang("Contact us","CONTACTENOS")?></a></li>
                    <li></li>
                </ul>
            </nav>
            <section id="banner">
                    <div id="banner-main">
                            <div class="viewport">
                                <ul class="overview">
                                    <?php for($i=0; $i<count($lst_img); $i++){ ?>
                                        <li><img src="../img/banner_big/<?php echo $lst_img[$i] ?>"></li>
                                    <?php } ?>
                                </ul>
                            </div>
                            <ul class="bullets">
                                <?php for($i=0; $i<count($lst_url); $i++){ ?>
                                    <li><a href="<?php echo $lst_url[$i] ?>" class="bullet <?php echo ($i==0)?'active':''?>" data-slide="<?php echo $i ?>"></a></li>
                                <?php } ?>
                            </ul>


                        <script type="text/javascript">
                            $(document).ready(function()
                            {
                                $("#banner-main").tinycarousel({
                                    bullets  : true, interval  : true
                                });
                            });
                        </script>
                        <script type="text/javascript">
                            function idioma(lang){
                                document.lang.idioma.value=lang;
                                document.lang.submit();
                            }
                        </script>
                    </div>
            </section>
            <form name="lang" method="post" action="">
                <input type="hidden" name="idioma">
            </form>
        </header>