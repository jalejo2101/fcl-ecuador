<!DOCTYPE html>

<html lang="es">



    <head>

    

        <meta charset="utf-8" />

        

        <link href="../css/reset.css" type="text/css" rel="stylesheet">   

        <link href="../css/layout.css" type="text/css" rel="stylesheet">

        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

        

        <!-- JSs -->

        <script src="../js/jquery-1.9.1.js"></script>

        <script src="../js/jquery.tinycarousel.js"></script>

        

        <title>Freightlogistics</title>

    </head>

           

    <body>

    <script>

    $(document).ready(function() {
        $('nav#menu-top ul.main-sect ul li a').on("mouseenter",function(){ $('nav#menu-top ul.main-sect >li:nth-child(4) > a').css("background-color","#114F82"); });
        $('nav#menu-top ul.main-sect >li:nth-child(4) > a').on("mouseenter",function(){ $('nav#menu-top ul.main-sect >li:nth-child(4) > a').css("background-color","#114F82"); });
        $('nav#menu-top ul.main-sect ul li a').on("mouseleave",function(){ $('nav#menu-top ul.main-sect >li:nth-child(4) > a').css("background-color","#216BA8"); });
        $('nav#menu-top ul.main-sect >li:nth-child(4) > a').on("mouseleave",function(){ $('nav#menu-top ul.main-sect >li:nth-child(4) > a').css("background-color","#216BA8"); });
        //nav#menu-top ul.main-sect li > a:hover{ background-color:#114F82; }
    });

    

    function show(mostrar){

        $('.darkbox-cont').fadeIn();

        $(mostrar).fadeIn();

    }

        

    function cierra(){

        $('.darkbox-cont').fadeOut();

        $('.login-box').fadeOut();

        $('.why-box').fadeOut();

        $('.quick-box').fadeOut();

    }

    </script>

        

    <div class="darkbox-cont">

        <section class="login-box">

            <h2>LOGIN</h2>

            <ul>

                <li><label>Email</label><input type="email" name="email" placeholder="Email"></li>

                <li><label>Password</label><input type="password" name="password" placeholder="Password"></li>

                <li><input type="checkbox" name="remember" value="remember">Remember me</li>

            </ul>

            <div>

                <input type="submit" value="LOGIN">

                <input type="button" value="CANCEL" onclick="cierra();">

                

            </div>

        </section>

        

        <section class="why-box">

            <a href="#" onclick="cierra();"></a>

            <h2>WHY FCL</h2>

            <p>

                Some OTI or NVOCC are strong in certain port/areas and they are able to offer competitive rates but same OTI or NVOCC are not strong enough in other ports and their rates are higher, F.C.L. give you the best rate option from several international logistics firms to and from Ecuador.
            </p>

            <p>

                We provide shipping rates for commercial container shipping, L.C.L shipping, and Household goods shipping, via ocean freight carriers and air cargo carriers.
            </p>

        </section>

        

        <section class="quick-box">

            <h2>QUICK CONTACT</h2>

            <ul>

                <li><label>Name</label><input type="text" name="name" placeholder="Name"></li>

                <li><label>Email</label><input type="email" name="email" placeholder="Email"></li>

                <li><label>Phone</label><input type="tel" name="phone" placeholder="Phone"></li>

                <li><label>Message</label><textarea></textarea>

            </ul>

            <div>

                <input type="submit" value="SUBMIT">

                <input type="button" value="CANCEL" onclick="cierra();">

                

            </div>

        </section>

        

    </div>

        

    <div id="wrapper">

<!-- =================== HEADER  ====================== -->  

    

        

    

        <header id="h-main">

            <section id="top">

                <div>

                    <ul class="sociales">

                        <li><a href="#"><img src=../img/soc-twitter.png></a></li>

                        <li><a href="#"><img src=../img/soc-face.png></a></li>

                        <li><a href="#"><img src=../img/soc-skype.png></a></li>

                        <li><a href="#"><img src=../img/soc-gplus.png></a></li>

                    </ul>

                    <ul class="idiomas">

                        <li><a href="#"><img src=../img/len-esp-off.png></a></li>

                        <li><a href="#"><img src=../img/len-ing-on.png></a></li>

                    </ul>

                </div>

            </section>

            <section id="middle">

                <div class="mini-menu">

                    <a class="why" onclick="show('.why-box');">Why FCL</a>

                    <ul class="login">

                        <li>Welcome to Freightlogistics</li>

                        <li><a href="#" onclick="show('.login-box');">Login</a></li>

                        <li><a href="#">Register</a></li>

                    </ul>

                    <ul class="account">

                        <li><a href="#">My Account</a></li>

                        <li><a href="#">Add to Favorites</a></li>

                    </ul>

                </div>

            </section>

            <nav id="menu-top">

                <ul class="main-sect">
                    
                    <li></li>
                    
                    <li><a href="index.php">Home</a></li>

                    <li><a href="about.php">About us</a></li>

                    <li><a href="#">Services </a>

                        <ul>

                            <li><a href="serv-inter-trans.php">Internation Transportation</a></li>

                            <li><a href="serv-pickup.php">Pick up / Delivery</a></li>

                            <li><a href="serv-custom.php">Custom Brokerage</a></li>

                            <li><a href="serv-cargo.php">Cargo Insurance</a></li>

                        </ul>

                    </li>

                    <li><a href="#">How It Works</a></li>

                    <li><a href="contact.php">Contact us</a></li>

                    <li></li>

                </ul>

            </nav>

            <section id="banner">

                    <div id="banner-main">

                        <div class="viewport">

                            <ul class="overview">

                                <li><img src="../img/slider-01.png"></li>

                                <li><img src="../img/slider-02.png"></li>

                                <li><img src="../img/slider-03.png"></li>

                                <li><img src="../img/slider-04.png"></li>

                            </ul>

                        </div>

                        <ul class="bullets">

                            <li><a href="#" class="bullet active" data-slide="0"></a></li>

                            <li><a href="#" class="bullet" data-slide="1"></a></li>

                            <li><a href="#" class="bullet" data-slide="2"></a></li>

                            <li><a href="#" class="bullet" data-slide="3"></a></li>

                

                        </ul>



                        <script type="text/javascript">

                            $(document).ready(function()

                            {

                                $("#banner-main").tinycarousel({

                                    bullets  : true, interval  : true

                                });

                            });

                        </script>

                    </div>

            </section>

        </header>