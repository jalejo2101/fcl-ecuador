      <footer id="f-main">

            <nav id="menu-bottom">

                <section>

                    <h3>POPULAR LIKNS</h3>

                    <ul>

                        <li><a href="http://www.aduana.gob.ec/index.action" target="_blank">	Ecuadorian Customs	</a></li>

                        <li><a href="http://www.industrias.gob.ec/" target="_blank">	Ministry of Industry and Productivity	</a></li>

                        <li><a href="http://www.normalizacion.gob.ec/" target="_blank">	Ecuadorian Standards Institute - INEN	</a></li>

                        <li><a href="http://www.acreditacion.gob.ec/" target="_blank">	Ecuadorian Accreditation Organization	</a></li>

                        <li><a href="http://www.cgsa.com.ec/inicio.aspx" target="_blank">	Port Terminal Contecon	</a></li>

                        <li><a href="http://www.aretina.com/index.php?option=com_content&view=article&id=31&Itemid=52" target="_blank">	Port Terminal Banapuerto	</a></li>

                        <li><a href="http://www.tpg.com.ec/" target="_blank">	Port Terminal TPG	</a></li>

                    </ul>

                </section>

                <section>

                    <h3>TOOLS</h3>

                    <ul>

                        <li><a href="http://www.softtruck.com/?source=One_Phrase&gclid=CIWgqKmfjL4CFYqhOgodsRQACg" target="_blank">	Container Calculator	</a></li>

                        <li><a href="http://www.unitconverters.net/" target="_blank">	Measurement Converter	</a></li>

                        <li><a href="http://www.customs.gov.au/webdata/resources/files/Incoterms.pdf" target="_blank">	Incomterms	</a></li>

                        <li><a href="http://www.acuitivesolutions.com/Calculator/ChargeableWeight.aspx" target="_blank">	Air Cargo Calculator	</a></li>

                        <li><a href="http://www.dutycalculator.com/new-import-duty-and-tax-calculation/" target="_blank">	Duty Calculator	</a></li>

                        <li><a href="http://www.aduana.gob.ec/pro/household_goods.action" target="_blank">	Households to Ecuador 	</a></li>

                        <li><a href="http://www.dutycalculator.com/country-guides/Import-duty-taxes-when-importing-into-Ecuador/" target="_blank">	How to Import to Ecuador	</a></li>

                        <li><a href="http://www.aduana.gob.ec/pro/for_travelers.action" target="_blank">	For Traveler	</a></li>                    

                    </ul>

                </section>

                <section>

                    <h3>ABOUT US</h3>

                    <ul>

                        <li><a href="http://www.flc-ecuador.com/" target="_blank">	WHO WE ARE	</a></li>

                        <li><a href="#" target="_blank">PRIVATE POLICY</a></li>

                        <li><a href="#" target="_blank">TERMS & CONDITIONS</a></li>

                        <li><a href="#" target="_blank">CONTACT US</a></li>

                    </ul>

                </section>

                <section>

                    <h3>SUPPORT</h3>

                    <ul>

                        <li><a href=" " target="_blank">Frecuently Asked Question</a></li>

                        <li><a href=" " target="_blank">Moving</a></li>

                        <li><a href=" " target="_blank">Shipping Instructions</a></li>

                        <li><a href=" " target="_blank">Documentacion</a></li>

                        <li><a href=" " target="_blank">Reservation</a></li>

                        <li><a href=" " target="_blank">Council For Import</a></li>

                        <li><a href=" " target="_blank">Council For Export</a></li>                

                    </ul>

                </section>

                <!--

                <section>

                    <h3>SOCIAL</h3>

                    <ul>

                        <li><a href="https://www.facebook.com/pages/World-Freight-Rates/442892302431975?fref=ts" target="_blank">Facebook</a></li>

                        <li><a href="https://twitter.com/WFRates" target="_blank">Twitter</a></li>

                        <li><a href="#" target="_blank">SKYPE</a></li>

                        <li><a href="#" target="_blank">GOOGLE +</a></li>

                    </ul>

                </section>

            -->

            </nav>

            <section id="copyright">
            <ul>
                <li>Co Copyright © 2014 Freightlogistics C.A.</li>
                <!--  <li>Designed and developer by RF SOFTWARE SOLUTIONS</li>  -->
            </ul>

            </section>

        </footer>

    </body>

   </div> 

</html>