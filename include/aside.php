<?php include_once("../admin/Consultas.php");?>
<?php include_once("../admin/funciones.php");?>

<?php if(isset($_GET["track"])){
    $con=new Consultas();
    $number=$_GET["track"];
    $tr=$con->get_type_tracking($number);
    if($tr=="air"){
        $url='air_tracking.php?awb_number='.$number;
        echo "<script>window.open('$url','_self','')</script>";

    }else if($tr=="sea"){
        $url='sea_tracking.php?bl_number='.$number;
        echo "<script>window.open('$url','_self','')</script>";
        echo $url;
    }else{ ?>
        <script>
            alert('<?php lang("Tracking number doesn't exist","Numero de Tracking no existe")?>');
        </script>
    <?php }
}
?>





<?php
$lg=($_SESSION["idioma"]=="esp")?"_esp":"";
$lst=$con->get_lst_noticias_activo($lg);
if(isset($_POST['n_mail'])){
    if(agrega_newsletter()==true){ ?>
        <script>
            $('document').ready(function(){
                alert('<?php lang("Thank you for subscribe to our news letter.","Gracias por suscribirte a nuestros boletines")?>');
            });
        </script>
    <?php }else{ ?>
        <script>
            $('document').ready(function(){
                alert('<?php lang("This e-mail address is already registered.","Esta dirección de correo electrónico ya está registrada")?>');
            });
        </script>
    <?php }
} ?>



<script>
    function check(input) {
        if(input.value==""){
            input.setCustomValidity('<?php lang("Required email address","Se requiere dirección de correo")?>');
        }else if(input.validity.typeMismatch){
            input.setCustomValidity("<?php lang("Is not a Valid Email Address.","La dirección de correo no es valida")?>");
        }else {
            input.setCustomValidity("");
        }
    }
</script>

            <aside id="lateral">
                <section>
                   <h4><?php lang("Track Shipment","Rastree su envío")?></h4>
                   <form method="get" name="fr" action="">

                       <input type="text" name="track" id="track"  placeholder="<?php lang("AWB/CONTAINER NUMBER","AWB/NUMERO DE CONTAINER")?>" required="">

                       <input type="submit" class="fsearch" value="">

                    </form>

                </section>

                <section>

                   <h4><?php lang("E-mail Newsletter","Boletines")?></h4>

                    <form name="newsletter" method="post" action="">
                       <input type="email" name="n_mail" value="" id="track" placeholder="<?php lang("Email Address","Dirección de Correo")?>" required oninvalid="check(this)">
                       <input type="submit" value="" class="fmail">
                    </form>
                </section>

                <section>

                   <h4><?php lang("Latest News","Ultimas Noticias")?></h4>



                    <?php for($i=0; $i<=1; $i++){ ?>
                    <article class="news">
                        <aside>
                            <header><?php echo strtoupper(date('M',strtotime($lst[$i]["fecha"])));?></header>
                            <div><?php echo strtoupper(date('d',strtotime($lst[$i]["fecha"])));?></div>
                        </aside>
                        <h5><?php echo $lst[$i]["titulo"] ?></h5>
                        <p><?php echo substr($lst[$i]["texto"], 0, 70)."..."; ?></p>
                    </article>
                    <?php } ?>
                    <a class="allnews" href="news.php"><?php lang("All news","Todas las Noticias")?> <span> > </span> </a>
                </section>

                <section>
                   <h4 class="cursor-link"  onclick="show('.quick-box');"><?php lang("Quick Contact","Contacto Rápido")?></h4>
                </section>

            </aside>